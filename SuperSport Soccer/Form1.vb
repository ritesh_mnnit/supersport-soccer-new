Imports System.Text
Imports Rebex.Net

Public Class Form1

    Public Version As String = "1.0.0b"
    Public DatabaseConn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationSettings.AppSettings("dbConn"))
    Public SqlQuery As SqlCommand
    Public Proxy As Boolean = False
    Public myProxy As System.Net.WebProxy
    Public sysTimer As System.Timers.Timer
    Public minTimer As System.Timers.Timer
    Public Current As Hashtable
    Public _StartXY As Point
    Public _MouseDown As Boolean
    Public Paused As Boolean = False
    Public DbConnected As Boolean = False
    Public DbErrors As Integer = 0
    Public EmptyFiles As Hashtable
    Public FirstRun As Boolean = True
    Public mtnTournaments As ArrayList = New ArrayList
    Public paSeason As Integer = 6

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim Multiple As Boolean = False
        Dim Update As Boolean = False

        ContextMenuStrip1.ImageList = ImageList2
        ToolStripMenuItem1.ImageIndex = 3
        ViewToolStripMenuItem.ImageIndex = 7
        PauseToolStripMenuItem.ImageIndex = 4
        RunToolStripMenuItem.ImageIndex = 6

        Multiple = CheckForExistingInstance()
        Update = CheckForUpdate()
        CheckForProxy()

        If Multiple = True Then
            MessageBox.Show("Another Instance of this process is already running", "Multiple Instances Forbidden", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Application.Exit()
        End If

        If Multiple = False Then
            Xceed.Zip.Licenser.LicenseKey = "ZIN23-X0UZT-5AHMP-4A2A"
            If Update = False Then
                Call StartUp()
            End If
        Else

        End If

        mtnTournaments.Add(100)
        mtnTournaments.Add(794)

        'DatabaseConn = New SqlConnection
        'DatabaseConn.ConnectionString = "Server=sql.supersport.co.za;UID=supersms;PWD=5m5RrR4;DATABASE=SuperSportZone"
        'DatabaseConn.Open()

        'Dim tmpStream As MemoryStream = New MemoryStream
        'Dim tmpStreamWriter As StreamWriter = New StreamWriter(tmpStream)
        'SqlQuery = New SqlCommand("Select Distinct CellNumber As Number From SuperSportZone.dbo.supersms_viewSubscriptionDetails Where (Status = 1) And (ProductId = 100000 Or ProductId = 31)", DatabaseConn)
        'Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader()
        'While RsRec.Read()
        'tmpStreamWriter.WriteLine(RsRec("Number"))
        'End While
        'RsRec.Close()
        'tmpStreamWriter.Flush()
        'tmpStream.Seek(0, SeekOrigin.Begin)

        'Dim lBytes As Long = tmpStream.Length
        'Dim NumberData(lBytes) As Byte
        'tmpStream.Read(NumberData, 0, lBytes)
        'tmpStreamWriter.Close()

        'SqlQuery = New SqlCommand("Insert Into SSZGeneral.dbo.BulkSmsApp (Test) Values (@Picture)", DatabaseConn)
        'Dim pictureParameter As SqlParameter = New SqlParameter("@Picture", SqlDbType.Image)
        'pictureParameter.Value = NumberData
        'SqlQuery.Parameters.Add(pictureParameter)
        'SqlQuery.ExecuteNonQuery()

        'SqlQuery = New SqlCommand("Select * From SSZGeneral.dbo.BulkSmsApp", DatabaseConn)
        'Dim Msg() As Byte = SqlQuery.ExecuteScalar

        'Dim Fs As New FileStream("C:\Temp.txt", FileMode.OpenOrCreate, FileAccess.Write)
        'Dim Sw As New StreamWriter(Fs)
        'Sw.BaseStream.Seek(0, SeekOrigin.End)
        'Sw.Write(System.Text.Encoding.Default.GetString(Msg))
        'Sw.Close()

        'DatabaseConn.Close()

    End Sub

    Private Sub StartUp()

        sysTimer = New System.Timers.Timer(20000)
        AddHandler sysTimer.Elapsed, AddressOf sysOnTimedEvent
        sysTimer.AutoReset = True
        sysTimer.Enabled = True

        EmptyFiles = New Hashtable

    End Sub

    Private Sub Run()

        'DatabaseConn.ConnectionString = ""

        If Paused = False Then
            sysTimer.Enabled = False
        End If
        updateButton("", False)
        updateListbox("Starting run", True)

        GetFiles()
        'GetLiveFiles()

        If DateTime.Now.Minute = 10 Then
            GetEmptyFiles()
        End If

        Try
            DatabaseConn = New SqlConnection
            DatabaseConn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings("dbConn")
            DatabaseConn.Open()
            DbConnected = True
        Catch Ex As Exception
            Call ErrorHandler("", Ex.Message, "Error connecting to database")
            DbConnected = False
        End Try


        If DbConnected = True Then
            AddHandler DatabaseConn.StateChange, AddressOf databaseOnStateChanged

            If DbConnected = True Then
                Fixtures()
            End If
            If DbConnected = True Then
                Matches()
            End If
            If DbConnected = True Then
                Logs()
            End If
            If DbConnected = True Then
                Live()
            End If
            If DbConnected = True Then
                Profiles()
            End If

            If DbConnected = True Then
                'SmsFixtures(100)
                'SmsFixtures(794)
                If DateTime.Now.Minute Mod 5 = 0 Then
                    'mtnFixtures()
                End If
                'mtnMatchSms(794, "3154723", "half")
                'mtnMatchSms(794, "3105691", "full", "")
                'mtnDailySummary(794, "3105691")
                'mtnDailySummary(794, 3154723)
            End If

            If DbConnected = True Then
                'Competitions()
            End If

            If DateTime.Now.Hour = 0 And DateTime.Now.Minute < 5 Then
                ZipFiles()
            ElseIf FirstRun = True Then
                ZipFiles()
            End If

            RemoveHandler DatabaseConn.StateChange, AddressOf databaseOnStateChanged
        End If

        Try
            DatabaseConn.Close()
        Catch

        Finally
            DatabaseConn.Dispose()
            DbConnected = False
            DbErrors = 0
        End Try

        updateListbox("Run Complete (" & DateTime.Now.ToShortTimeString & ")", True)
        updateListbox("select", True)
        updateButton("", True)
        FirstRun = False
        If Paused = False Then
            sysTimer.Enabled = True
        End If

    End Sub

    Private Sub sysOnTimedEvent(ByVal source As Object, ByVal e As ElapsedEventArgs)

        If Me.WindowState = FormWindowState.Normal Then
            minTimer = New System.Timers.Timer(300000)
            AddHandler minTimer.Elapsed, AddressOf minOnTimedEvent
            minTimer.AutoReset = False
            minTimer.Enabled = True
        Else
            minTimer = New System.Timers.Timer(300000)
            AddHandler minTimer.Elapsed, AddressOf minOnTimedEvent
            minTimer.AutoReset = False
            minTimer.Enabled = True
        End If

        updateListbox("", False)
        Dim t As Thread = New Thread(AddressOf Run)
        t.IsBackground = True
        t.Start()

    End Sub

    Private Sub minOnTimedEvent(ByVal source As Object, ByVal e As ElapsedEventArgs)

        If Me.WindowState = FormWindowState.Normal Then
            'Me.Hide()
            'Me.WindowState = FormWindowState.Minimized
            'ViewToolStripMenuItem.Text = "View"
        End If

    End Sub

    Private Sub databaseOnStateChanged(ByVal source As Object, ByVal e As System.Data.StateChangeEventArgs)

        If DbConnected = True Then
            If e.CurrentState = ConnectionState.Broken Or e.CurrentState = ConnectionState.Closed Then
                Try
                    If DatabaseConn.State = ConnectionState.Broken Then
                        DatabaseConn.Close()
                        DatabaseConn.Dispose()
                        DatabaseConn = New SqlConnection
                        DatabaseConn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings("dbConn")
                        DatabaseConn.Open()
                    ElseIf DatabaseConn.State = ConnectionState.Closed Then
                        DatabaseConn.Dispose()
                        DatabaseConn = New SqlConnection
                        DatabaseConn.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings("dbConn")
                        DatabaseConn.Open()
                    End If
                Catch Ex As Exception
                    DbConnected = False
                    Call ErrorHandler("Database Error", Ex.Message, "Error connecting to database")
                End Try
            End If
        End If

    End Sub

#Region " Painting "

    Private Sub Form1_Paint(ByVal sender As Object, ByVal e As PaintEventArgs) Handles MyBase.Paint

        Dim myGraphics As Graphics = e.Graphics

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(206, 223, 246)
        Dim tmpPen As Pen = New Pen(tmpColor)

        myGraphics.DrawRectangle(tmpPen, New Rectangle(2, 2, 238, 212))

        myGraphics.DrawLine(tmpPen, 0, 3, 0, 213)
        myGraphics.DrawLine(tmpPen, 1, 1, 1, 214)

        myGraphics.DrawLine(tmpPen, 3, 0, 239, 0)
        myGraphics.DrawLine(tmpPen, 1, 1, 241, 1)

        myGraphics.DrawLine(tmpPen, 241, 1, 241, 214)
        myGraphics.DrawLine(tmpPen, 242, 3, 242, 213)

        myGraphics.DrawLine(tmpPen, 1, 215, 241, 215)
        myGraphics.DrawLine(tmpPen, 3, 216, 239, 216)

        myGraphics.DrawLine(tmpPen, 1, 1, 2, 2)
        myGraphics.DrawLine(tmpPen, 2, 1, 2, 2)

        myGraphics.DrawLine(tmpPen, 240, 2, 241, 1)
        myGraphics.DrawLine(tmpPen, 240, 1, 240, 2)

        myGraphics.DrawLine(tmpPen, 1, 215, 2, 214)
        myGraphics.DrawLine(tmpPen, 2, 214, 2, 215)

        myGraphics.DrawLine(tmpPen, 240, 215, 241, 215)
        myGraphics.DrawLine(tmpPen, 240, 214, 240, 215)

    End Sub

    Private Sub Panel1_Paint(ByVal sender As Object, ByVal e As PaintEventArgs) Handles Panel1.Paint

        Dim myGraphics As Graphics = e.Graphics

        Dim myRectangle As Rectangle = New Rectangle(0, 0, 237, 130)
        myGraphics.DrawRectangle(Pens.Transparent, myRectangle)
        Dim StartColor As System.Drawing.Color = System.Drawing.Color.FromArgb(68, 118, 185)
        Dim EndColor As System.Drawing.Color = System.Drawing.Color.FromArgb(206, 223, 246)
        Dim tmpPen As Pen = New System.Drawing.Pen(EndColor)
        Dim myBrush As System.Drawing.Drawing2D.LinearGradientBrush
        myBrush = New System.Drawing.Drawing2D.LinearGradientBrush(myRectangle, StartColor, EndColor, System.Drawing.Drawing2D.LinearGradientMode.Vertical)
        myGraphics.FillRectangle(myBrush, myRectangle)

    End Sub

    Private Sub Panel2_Paint(ByVal sender As Object, ByVal e As PaintEventArgs) Handles Panel2.Paint

        Dim myGraphics As Graphics = e.Graphics
        Dim Color1 As Color = System.Drawing.Color.FromArgb(18, 52, 113)

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(206, 223, 246)
        Dim tmpPen As Pen = New Pen(tmpColor)

        myGraphics.DrawLine(tmpPen, 0, 0, 1, 0)
        myGraphics.DrawLine(tmpPen, 0, 0, 0, 1)

        myGraphics.DrawLine(tmpPen, 235, 0, 236, 0)
        myGraphics.DrawLine(tmpPen, 236, 0, 236, 1)

        Dim tmpIcon As System.Drawing.Icon = New System.Drawing.Icon("C:\Program Files\SuperSport Zone\SuperSport Active\appxp.ico")
        myGraphics.DrawIcon(tmpIcon, 5, 5)

    End Sub

#End Region

#Region " Mouse Events "

    Private Sub pnl1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel2.MouseDown

        Me._StartXY = Me.MousePosition
        Me._MouseDown = True

    End Sub

    Private Sub pnl1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel2.MouseMove

        If Me._MouseDown Then
            Dim x As Integer, y As Integer
            Dim Xdiff As Integer, Ydiff As Integer
            Xdiff = Me.MousePosition.X - Me._StartXY.X
            Ydiff = Me.MousePosition.Y - Me._StartXY.Y
            Me.Left = Me.Left + Xdiff
            Me.Top = Me.Top + Ydiff
            Me._StartXY = Me.MousePosition
        End If

    End Sub

    Private Sub pnl1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Panel2.MouseUp

        MyBase.OnMouseUp(e)
        Me._MouseDown = False

    End Sub

    Private Sub Button1_MouseOver(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Button1.MouseMove

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(45, 115, 212)
        Button1.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button1_MouseOut(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.MouseLeave

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(206, 223, 246)
        Button1.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        ListBox1.Focus()
        ListBox1.Items.Clear()
        Dim t As Thread = New Thread(AddressOf Run)
        t.IsBackground = True
        t.Start()

    End Sub

    Private Sub Button2_MouseOver(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Button2.MouseMove

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(45, 115, 212)
        Button2.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button2_MouseOut(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.MouseLeave

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(206, 223, 246)
        Button2.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        ListBox1.Focus()
        If Button2.ImageIndex = 1 Then
            Button2.ImageIndex = 2
            sysTimer.Enabled = False
            Paused = True
            updateListbox("", False)
            updateListbox("Application paused", True)
            PauseToolStripMenuItem.Text = "Start"
            PauseToolStripMenuItem.ImageIndex = 5
        Else
            Button2.ImageIndex = 1
            sysTimer.Enabled = True
            Paused = False
            updateListbox("", False)
            updateListbox("Application started", True)
            PauseToolStripMenuItem.Text = "Pause"
            PauseToolStripMenuItem.ImageIndex = 4
        End If

    End Sub

    Private Sub Button3_MouseOver(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Button3.MouseMove

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(206, 223, 246)
        Button3.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button3_MouseOut(ByVal sender As Object, ByVal e As EventArgs) Handles Button3.MouseLeave

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(68, 118, 185)
        Button3.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        Me.Close()
        Me.Dispose()

    End Sub

    Private Sub Button4_MouseOver(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Button4.MouseMove

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(206, 223, 246)
        Button4.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button4_MouseOut(ByVal sender As Object, ByVal e As EventArgs) Handles Button4.MouseLeave

        Dim tmpColor As Color = System.Drawing.Color.FromArgb(68, 118, 185)
        Button4.FlatAppearance.BorderColor = tmpColor

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click

        ListBox1.Focus()
        ViewToolStripMenuItem.Text = "View"
        ViewToolStripMenuItem.ImageIndex = 8
        Me.Hide()
        Me.WindowState = FormWindowState.Minimized

    End Sub

    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem1.Click

        Me.Close()
        Me.Dispose()

    End Sub

    Private Sub ViewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewToolStripMenuItem.Click

        If ViewToolStripMenuItem.Text = "View" Then
            Me.WindowState = FormWindowState.Normal
            Me.Show()
            ViewToolStripMenuItem.Text = "Hide"
            ViewToolStripMenuItem.ImageIndex = 7
        Else
            Me.Hide()
            Me.WindowState = FormWindowState.Minimized
            ViewToolStripMenuItem.Text = "View"
            ViewToolStripMenuItem.ImageIndex = 8
        End If

    End Sub

    Private Sub RunToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RunToolStripMenuItem.Click

        ListBox1.Focus()
        ListBox1.Items.Clear()
        Dim t As Thread = New Thread(AddressOf Run)
        t.IsBackground = True
        t.Start()

    End Sub

    Private Sub PauseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PauseToolStripMenuItem.Click

        ListBox1.Focus()
        If Button2.ImageIndex = 1 Then
            Button2.ImageIndex = 2
            sysTimer.Enabled = False
            Paused = True
            updateListbox("", False)
            updateListbox("Application paused", True)
            PauseToolStripMenuItem.Text = "Start"
            PauseToolStripMenuItem.ImageIndex = 5
        Else
            Button2.ImageIndex = 1
            sysTimer.Enabled = True
            Paused = False
            updateListbox("", False)
            updateListbox("Application started", True)
            PauseToolStripMenuItem.Text = "Pause"
            PauseToolStripMenuItem.ImageIndex = 4
        End If

    End Sub

    Private Sub NotifyIcon1_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick

        If Me.WindowState = FormWindowState.Normal Then
            Me.Activate()
            ViewToolStripMenuItem.Text = "Hide"
        Else
            Me.WindowState = FormWindowState.Normal
            Me.Show()
            ViewToolStripMenuItem.Text = "Hide"
        End If

    End Sub


#End Region

#Region " GetFiles "

    Private Sub GetFiles()

        Try
            Dim File As String
            Dim Files() As String
            Dim Fi As FileInfo

            If Directory.Exists("C:\inetpub\ftproot\pauser") Then
                Files = Directory.GetFiles("C:\inetpub\ftproot\pauser")

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And Fi.Length > 0 Then
                        Dim Filename As String = String.Empty
                        If Fi.Name.StartsWith("fix_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("fixtures_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("matchlive_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.IndexOf("live") >= 0 Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("player_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("club_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("m") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Match\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("player_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("club_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("soc_lts") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Logs\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("soc_cmp") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Competition\Temporary\" & Fi.Name & ""
                            Call ErrorHandler("", "New competition file added", "New competition file added")
                        Else
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\" & Fi.Name & ""
                        End If

                        If Not String.IsNullOrEmpty(Filename) Then
                            Fi.CopyTo(Filename, True)
                            Fi.Delete()
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            Call ErrorHandler("", ex.Message, "Error getting files from internal PA ftp folder")
        End Try

        Try
            Dim File As String
            Dim Files() As String
            Dim Fi As FileInfo

            If Directory.Exists("C:\inetpub\ftproot\pauser\football") Then
                Files = Directory.GetFiles("C:\inetpub\ftproot\pauser\football")

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And Fi.Length > 0 Then
                        Dim Filename As String = String.Empty
                        If Fi.Name.StartsWith("fix_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("fixtures_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("matchlive_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.IndexOf("live") >= 0 Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("player_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("club_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("m") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Match\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("player_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("club_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("soc_lts") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Logs\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("soc_cmp") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Competition\Temporary\" & Fi.Name & ""
                            Call ErrorHandler("", "New competition file added", "New competition file added")
                        Else
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\" & Fi.Name & ""
                        End If

                        If Not String.IsNullOrEmpty(Filename) Then
                            Fi.CopyTo(Filename, True)
                            Fi.Delete()
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            Call ErrorHandler("", ex.Message, "Error getting files from internal PA ftp folder")
        End Try

        Try
            Dim File As String
            Dim Files() As String
            Dim Fi As FileInfo

            If Directory.Exists("C:\inetpub\ftproot\teamtalk\football") Then
                Files = Directory.GetFiles("C:\inetpub\ftproot\teamtalk\football")

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And Fi.Length > 0 Then
                        Dim Filename As String = String.Empty
                        If Fi.Name.StartsWith("fix_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("fixtures_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("matchlive_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.IndexOf("live") >= 0 Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("player_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("club_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("m") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Match\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("player_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("club_") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("soc_lts") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Logs\Temporary\" & Fi.Name & ""
                        ElseIf Fi.Name.StartsWith("soc_cmp") Then
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Competition\Temporary\" & Fi.Name & ""
                            Call ErrorHandler("", "New competition file added", "New competition file added")
                        Else
                            Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\" & Fi.Name & ""
                        End If

                        If Not String.IsNullOrEmpty(Filename) Then
                            Fi.CopyTo(Filename, True)
                            Fi.Delete()
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            Call ErrorHandler("", ex.Message, "Error getting files from internal Team Talk ftp folder")
        End Try

        Dim tmpFtp As Rebex.Net.Ftp = New Rebex.Net.Ftp
        'Dim Server As String = "partner.supersport.co.za"
        'Dim UserName As String = "supersport/pasport"
        'Dim Password As String = "Sup3rS0cc3r"
        Dim Server As String = "legacy.dstv.com"
        Dim UserName As String = "DSTVO/pasport"
        Dim Password As String = "Sup3rS0cc3r"
        Dim DisconnectCount As Integer = 0

        Try
            updateListbox("Checking for new data", True)
            tmpFtp.Connect(Server)
            tmpFtp.Login(UserName, Password)
            tmpFtp.SetTransferType(Rebex.Net.FtpTransferType.Binary)
            tmpFtp.KeepAlive()
            Dim List As Rebex.Net.FtpList
            List = tmpFtp.GetList()
            Dim I As Integer
            Dim Total As Integer = 0
            If List.Count > 0 Then
                For I = 0 To List.Count - 1
                    Dim Item As Rebex.Net.FtpItem = List(I)
                    If Item.Name.IndexOf(".xml") >= 0 Then
                        Total = Total + 1
                    End If
                Next
            End If
            If Total > 0 Then
                Dim FilesDownloaded As Integer = 0
                Dim FileErrors As Integer = 0
                updateListbox("Downloading files", True)
                updateListbox(Total & " files remaining", True)
                For I = 0 To List.Count - 1
                    Dim Download As Boolean = False
                    Dim Item As Rebex.Net.FtpItem = List(I)
                    Dim Filename As String = ""
                    If Item.Name.IndexOf(".xml") >= 0 Then
                        If Item.Size > 0 Then
                            Download = True
                            If Item.Name.StartsWith("fix_") Then
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\Temporary\" & Item.Name & ""
                            ElseIf Item.Name.StartsWith("fixtures_") Then
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\Temporary\" & Item.Name & ""
                            ElseIf Item.Name.StartsWith("matchlive_") Then
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Temporary\" & Item.Name & ""
                            ElseIf Item.Name.IndexOf("live") >= 0 Then
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Temporary\" & Item.Name & ""
                            ElseIf Item.Name.StartsWith("m") Then
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Match\Temporary\" & Item.Name & ""
                            ElseIf Item.Name.StartsWith("player_") Then
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Item.Name & ""
                            ElseIf Item.Name.StartsWith("club_") Then
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Item.Name & ""
                            ElseIf Item.Name.StartsWith("soc_lts") Then
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Logs\Temporary\" & Item.Name & ""
                            ElseIf Item.Name.StartsWith("soc_cmp") Then
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Competition\Temporary\" & Item.Name & ""
                                Call ErrorHandler("", "New competition file added", "New competition file added")
                            Else
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\" & Item.Name & ""
                            End If
                        Else
                            If EmptyFiles.ContainsKey(Filename) Then
                                If EmptyFiles(Filename) > 1 Then
                                    Download = True
                                    Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\empty_" & Item.Name & ""
                                    EmptyFiles.Remove(Filename)
                                Else
                                    EmptyFiles.Remove(Filename)
                                    EmptyFiles.Add(Filename, 2)
                                End If
                            Else
                                EmptyFiles.Add(Filename, 1)
                            End If
                        End If
                    End If

                    If Download = True Then
                        Try
                            tmpFtp.GetFile(Item.Name, Filename)
                            tmpFtp.KeepAlive()
                            tmpFtp.DeleteFile(Item.Name)
                            tmpFtp.KeepAlive()
                            FilesDownloaded = FilesDownloaded + 1
                        Catch ex1 As Exception
                            If File.Exists(Filename) Then
                                Try
                                    File.Delete(Filename)
                                Catch ex2 As Exception
                                    Call ErrorHandler(Item.Name, ex2.Message, "Error deleting file")
                                End Try
                            End If
                            FileErrors = FileErrors + 1
                            If tmpFtp.State = Rebex.Net.FtpState.Disconnected Then
                                DisconnectCount = DisconnectCount + 1
                                If DisconnectCount = 5 Then
                                    Call ErrorHandler(Item.Name, ex1.Message, "To many disconnects")
                                    Exit For
                                Else
                                    Try
                                        tmpFtp.Connect(Server)
                                        tmpFtp.Login(UserName, Password)
                                        tmpFtp.SetTransferType(Rebex.Net.FtpTransferType.Binary)
                                        tmpFtp.KeepAlive()
                                    Catch ex2 As Exception
                                        Call ErrorHandler(Item.Name, ex2.Message, "Unable to connect")
                                        Exit For
                                    End Try
                                End If
                            Else
                                Call ErrorHandler(Item.Name, ex1.Message, "Error downloading the file")
                            End If
                        End Try

                        updateListbox(Total - (FilesDownloaded + FileErrors) + 1 & " files remaining", False)
                        If FilesDownloaded + FileErrors = Total Then
                            updateListbox(FilesDownloaded & " files downloaded with " & FileErrors & " errors", True)
                        Else
                            updateListbox(Total - (FilesDownloaded + FileErrors) & " files remaining", True)
                        End If
                    End If
                Next
            Else
                updateListbox("No new files", True)
            End If
            tmpFtp.Disconnect()
        Catch ex As Exception
            Call ErrorHandler("", ex.Message, "Error with ftp")
        Finally
            tmpFtp.Dispose()
        End Try

    End Sub

    Private Sub GetLiveFiles()

        Dim tmpFtp As Rebex.Net.Ftp = New Rebex.Net.Ftp
        Dim Server As String = "152.111.116.3"
        Dim UserName As String = "PASport"
        Dim Password As String = "Supersp0rt"
        Dim DisconnectCount As Integer = 0

        Try
            updateListbox("Checking for new live data", True)
            tmpFtp.Connect(Server)
            tmpFtp.Login(UserName, Password)
            tmpFtp.SetTransferType(Rebex.Net.FtpTransferType.Binary)
            tmpFtp.KeepAlive()
            Dim List As Rebex.Net.FtpList
            List = tmpFtp.GetList()
            Dim I As Integer
            Dim Total As Integer = 0
            If List.Count > 0 Then
                For I = 0 To List.Count - 1
                    Dim Item As Rebex.Net.FtpItem = List(I)
                    If Item.Name.IndexOf(".xml") >= 0 Then
                        Total = Total + 1
                    End If
                Next
            End If
            If Total > 0 Then
                Dim FilesDownloaded As Integer = 0
                Dim FileErrors As Integer = 0
                updateListbox("Downloading files", True)
                updateListbox(Total & " files remaining", True)
                For I = 0 To List.Count - 1
                    Dim Download As Boolean = False
                    Dim Item As Rebex.Net.FtpItem = List(I)
                    Dim Filename As String = ""
                    If Item.Name.IndexOf(".xml") >= 0 Then
                        If Item.Size > 0 Then
                            Download = True
                            If Item.Name.StartsWith("matchlive_") Then
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Temporary\" & Item.Name & ""
                            ElseIf Item.Name.IndexOf("live") >= 0 Then
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Temporary\" & Item.Name & ""
                            ElseIf Item.Name.StartsWith("player_") Then
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Item.Name & ""
                            ElseIf Item.Name.StartsWith("club_") Then
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary\" & Item.Name & ""
                            Else
                                Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\" & Item.Name & ""
                            End If
                        Else
                            If EmptyFiles.ContainsKey(Filename) Then
                                If EmptyFiles(Filename) > 1 Then
                                    Download = True
                                    Filename = "C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\empty_" & Item.Name & ""
                                    EmptyFiles.Remove(Filename)
                                Else
                                    EmptyFiles.Remove(Filename)
                                    EmptyFiles.Add(Filename, 2)
                                End If
                            Else
                                EmptyFiles.Add(Filename, 1)
                            End If
                        End If
                    End If

                    If Download = True Then
                        Try
                            tmpFtp.GetFile(Item.Name, Filename)
                            tmpFtp.KeepAlive()
                            tmpFtp.DeleteFile(Item.Name)
                            tmpFtp.KeepAlive()
                            FilesDownloaded = FilesDownloaded + 1
                        Catch ex1 As Exception
                            If File.Exists(Filename) Then
                                Try
                                    File.Delete(Filename)
                                Catch ex2 As Exception
                                    Call ErrorHandler(Item.Name, ex2.Message, "Error deleting file")
                                End Try
                            End If
                            FileErrors = FileErrors + 1
                            If tmpFtp.State = Rebex.Net.FtpState.Disconnected Then
                                DisconnectCount = DisconnectCount + 1
                                If DisconnectCount = 5 Then
                                    Call ErrorHandler(Item.Name, ex1.Message, "To many disconnects")
                                    Exit For
                                Else
                                    Try
                                        tmpFtp.Connect(Server)
                                        tmpFtp.Login(UserName, Password)
                                        tmpFtp.SetTransferType(Rebex.Net.FtpTransferType.Binary)
                                        tmpFtp.KeepAlive()
                                    Catch ex2 As Exception
                                        Call ErrorHandler(Item.Name, ex2.Message, "Unable to connect")
                                        Exit For
                                    End Try
                                End If
                            Else
                                Call ErrorHandler(Item.Name, ex1.Message, "Error downloading the file")
                            End If
                        End Try

                        updateListbox(Total - (FilesDownloaded + FileErrors) + 1 & " files remaining", False)
                        If FilesDownloaded + FileErrors = Total Then
                            updateListbox(FilesDownloaded & " files downloaded with " & FileErrors & " errors", True)
                        Else
                            updateListbox(Total - (FilesDownloaded + FileErrors) & " files remaining", True)
                        End If
                    End If
                Next
            Else
                updateListbox("No new live files", True)
            End If
            tmpFtp.Disconnect()
        Catch ex As Exception
            Call ErrorHandler("", ex.Message, "Error with ftp")
        Finally
            tmpFtp.Dispose()
        End Try

    End Sub

    Private Sub GetEmptyFiles()

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files") Then
            Dim File As String
            Dim Files() As String
            Dim Fi As FileInfo
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files")
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf("empty") >= 0 Then
                    If Fi.Length = 0 Then
                        Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Empty/" & Fi.Name & "", True)
                        Fi.Delete()
                    Else
                        If Fi.Name.StartsWith("empty_m") Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Match\Temporary/" & Fi.Name.Replace("empty_", "") & "", True)
                            Fi.Delete()
                        ElseIf Fi.Name.StartsWith("empty_f") Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\Temporary/" & Fi.Name.Replace("empty_", "") & "", True)
                            Fi.Delete()
                        End If
                    End If
                End If
            Next
        End If

    End Sub

#End Region

#Region " Fixtures "

    Private Sub Fixtures()

        Dim File As String
        Dim Files() As String
        Dim Fi As FileInfo
        Dim MatchId As String = ""
        Dim DbCount As Integer = 0
        Dim DatabaseError As Boolean = False

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\Temporary") Then
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\Temporary")

            Dim Count As Integer = 0
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf(".xml") >= 0 Then
                    Count = Count + 1
                End If
            Next

            If Count > 0 Then
                updateListbox("Processing fixtures", True)

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And DbConnected = True Then
                        Dim MoveFile As Boolean = True
                        Try
                            Dim xmlDoc As New XmlDocument
                            xmlDoc.Load(File)

                            Dim xmlTeams As XmlNodeList = xmlDoc.SelectNodes("Fixtures/Teams/Team")
                            Dim I As Integer
                            Dim Teams As Hashtable = New Hashtable
                            Teams.Add("", "")
                            For I = 0 To xmlTeams.Count - 1
                                Dim tmpId As String = xmlTeams(I).Attributes.ItemOf("id").InnerText
                                Dim tmpName As String = xmlTeams(I).Attributes.ItemOf("name").InnerText
                                Teams.Add(tmpId, FixTeamName(tmpId, tmpName))
                            Next
                            xmlTeams = Nothing

                            Dim xmlMatches As XmlNodeList = xmlDoc.SelectNodes("Fixtures/Fixture")
                            For I = 0 To xmlMatches.Count - 1
                                Dim Id As String = xmlMatches(I).Attributes.ItemOf("id").InnerText
                                MatchId = Id
                                Dim Competition As String = xmlMatches(I).Attributes.ItemOf("competitionId").InnerText
                                If Competition = "794" Then
                                    'SqlQuery = New SqlCommand("Update Soccer.dbo.pa_Competitions Set Running = 0 Where (Competition_Id = 794) And (Competition_Season = 2)", DatabaseConn)
                                    'SqlQuery.ExecuteNonQuery()
                                    'SqlQuery = New SqlCommand("Update Soccer.dbo.pa_Competitions Set Running = 1 Where (Competition_Id = 794) And (Competition_Season = 3)", DatabaseConn)
                                    'SqlQuery.ExecuteNonQuery()
                                End If
                                Dim Stage As Integer = xmlMatches(I).Attributes.ItemOf("stageNumber").InnerText
                                SqlQuery = New SqlCommand("Select Top 1 Competition_Season From Soccer.dbo.pa_Competitions Where (Competition_Id = " & Competition & ") Order By Competition_Season Desc", DatabaseConn)
                                Dim Season As Integer = SqlQuery.ExecuteScalar

                                Dim Round As Integer = 0
                                If Not xmlMatches(I).Attributes.ItemOf("roundNumber") Is Nothing Then
                                    Round = xmlMatches(I).Attributes.ItemOf("roundNumber").InnerText
                                End If

                                Dim Leg As Integer = 0
                                If Not xmlMatches(I).Attributes.ItemOf("leg") Is Nothing Then
                                    Leg = xmlMatches(I).Attributes.ItemOf("leg").InnerText
                                End If

                                Dim Replay As Integer = 0
                                If Not xmlMatches(I).Attributes.ItemOf("replay") Is Nothing Then
                                    Replay = xmlMatches(I).Attributes.ItemOf("replay").InnerText
                                End If

                                Dim tmpDate As String = xmlMatches(I).Attributes.ItemOf("date").InnerText
                                Dim tmpYear As String = tmpDate.Substring(0, 4)
                                Dim tmpMonth As String = tmpDate.Substring(4, 2)
                                Dim tmpDay As String = tmpDate.Substring(6, 2)
                                If tmpMonth.StartsWith("0") Then
                                    tmpMonth = tmpMonth.Substring(1, 1)
                                End If
                                If tmpDay.StartsWith("0") Then
                                    tmpDay = tmpDay.Substring(1, 1)
                                End If
                                Dim MatchDate As DateTime = New DateTime(tmpYear, tmpMonth, tmpDay, 0, 0, 0)

                                If Not xmlMatches(I).Attributes.ItemOf("time") Is Nothing Then
                                    Dim tmpArray() As String = xmlMatches(I).Attributes.ItemOf("time").InnerText.Split("+")
                                    Dim Hour1 As String = tmpArray(0).Substring(0, 2)
                                    If Hour1.StartsWith("0") Then
                                        Hour1 = Hour1.Substring(1, 1)
                                    End If
                                    Dim Minute1 As String = tmpArray(0).Substring(2, 2)
                                    If Minute1.StartsWith("0") Then
                                        Minute1 = Minute1.Substring(1, 1)
                                    End If
                                    Dim Hour2 As String = tmpArray(1).Substring(0, 2)
                                    If Hour2.StartsWith("0") Then
                                        Hour2 = Hour2.Substring(1, 1)
                                    End If
                                    Dim Minute2 As String = tmpArray(1).Substring(2, 2)
                                    If Minute2.StartsWith("0") Then
                                        Minute2 = Minute2.Substring(1, 1)
                                    End If
                                    MatchDate = New DateTime(MatchDate.Year, MatchDate.Month, MatchDate.Day, Hour1, Minute1, 0)
                                    MatchDate = MatchDate.AddHours(0 - Hour2)
                                    MatchDate = MatchDate.AddMinutes(0 - Minute2)
                                    MatchDate = MatchDate.AddHours(2)
                                End If

                                Dim Venue As String = ""
                                If Not xmlMatches(I).Attributes.ItemOf("venue") Is Nothing Then
                                    Venue = xmlMatches(I).Attributes.ItemOf("venue").InnerText
                                End If

                                Dim Country As String = ""
                                If Not xmlMatches(I).Attributes.ItemOf("country") Is Nothing Then
                                    Country = xmlMatches(I).Attributes.ItemOf("country").InnerText
                                End If

                                Dim PoolsNo As String = ""
                                If Not xmlMatches(I).Attributes.ItemOf("poolsNo") Is Nothing Then
                                    PoolsNo = xmlMatches(I).Attributes.ItemOf("poolsNo").InnerText
                                End If

                                Dim Status As String = ""
                                Dim StatusId As Integer = 0
                                If Not xmlMatches(I).Attributes.ItemOf("status") Is Nothing Then
                                    Status = xmlMatches(I).Attributes.ItemOf("status").InnerText
                                    StatusId = retStatus(Status.ToLower)
                                End If

                                Dim xmlParticipants As XmlNodeList = xmlMatches(I).SelectNodes("Participant")
                                Dim N As Integer
                                Dim HomeTeamId As String = ""
                                Dim AwayTeamId As String = ""
                                For N = 0 To xmlParticipants.Count - 1
                                    If N = 0 Then
                                        HomeTeamId = xmlParticipants(N).Attributes.ItemOf("teamId").InnerText
                                    Else
                                        AwayTeamId = xmlParticipants(N).Attributes.ItemOf("teamId").InnerText
                                    End If
                                Next
                                Dim HomeTeamName As String = Teams(HomeTeamId)
                                Dim AwayTeamName As String = Teams(AwayTeamId)

                                SqlQuery = New SqlCommand("Execute Soccer.dbo.pa_SetTeamNames " & Competition & ", " & Season & ", " & HomeTeamId & ", '" & HomeTeamName.Replace("'", "''") & "'", DatabaseConn)
                                SqlQuery.ExecuteNonQuery()

                                SqlQuery = New SqlCommand("Execute Soccer.dbo.pa_SetTeamNames " & Competition & ", " & Season & ", " & AwayTeamId & ", '" & AwayTeamName.Replace("'", "''") & "'", DatabaseConn)
                                SqlQuery.ExecuteNonQuery()

                                Dim Present As Boolean = True
                                Dim tmpId As Integer

                                SqlQuery = New SqlCommand("Select StatusId From Soccer.dbo.pa_Matches Where (Id = " & Id & ")", DatabaseConn)
                                If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                                    Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
                                    If RsRec.HasRows() Then
                                        While RsRec.Read
                                            tmpId = RsRec("StatusId")
                                        End While
                                    Else
                                        Present = False
                                    End If
                                    RsRec.Close()
                                    If Present = True Then
                                        If StatusId >= tmpId Then
                                            SqlQuery = New SqlCommand("Update Soccer.dbo.pa_Matches Set Competition = '" & Competition.Replace("'", "''").Trim() & "', Season = " & Season & ", Stage = " & Stage & ", Round = " & Round & ", Leg = " & Leg & ", Replay = " & Replay & ", PoolsNo = '" & PoolsNo.Replace("'", "''").Trim() & "', MatchDateTime = '" & MatchDate & "', Venue = '" & Venue.Replace("'", "''").Trim() & "', Country = '" & Country.Replace("'", "''").Trim() & "', HomeTeamid = " & HomeTeamId & ", AwayTeamId = " & AwayTeamId & ", HomeTeamName = '" & HomeTeamName.Replace("'", "''").Trim() & "', AwayTeamName = '" & AwayTeamName.Replace("'", "''").Trim() & "', Status = '" & Status.Replace("'", "''").Trim() & "', StatusId = " & StatusId & ", Modified = '" & DateTime.Now & "' Where (Id = '" & Id & "')", DatabaseConn)
                                        Else
                                            SqlQuery = New SqlCommand("Update Soccer.dbo.pa_Matches Set Competition = '" & Competition.Replace("'", "''").Trim() & "', Season = " & Season & ", Stage = " & Stage & ", Round = " & Round & ", Leg = " & Leg & ", Replay = " & Replay & ", PoolsNo = '" & PoolsNo.Replace("'", "''").Trim() & "', MatchDateTime = '" & MatchDate & "', Venue = '" & Venue.Replace("'", "''").Trim() & "', Country = '" & Country.Replace("'", "''").Trim() & "', HomeTeamid = " & HomeTeamId & ", AwayTeamId = " & AwayTeamId & ", HomeTeamName = '" & HomeTeamName.Replace("'", "''").Trim() & "', AwayTeamName = '" & AwayTeamName.Replace("'", "''").Trim() & "', Modified = '" & DateTime.Now & "' Where (Id = '" & Id & "')", DatabaseConn)
                                        End If
                                    Else
                                        SqlQuery = New SqlCommand("Insert Into Soccer.dbo.pa_Matches (Id, Season, Competition, Stage, Round, Leg, Replay, PoolsNo, MatchDateTime, Venue, Country, HomeTeamId, AwayTeamId, HomeTeamName, AwayTeamName, Status, StatusId, Modified, Created) Values ('" & Id & "', " & Season & ", '" & Competition.Replace("'", "''").Trim() & "', " & Stage & ", " & Round & ", " & Leg & ", " & Replay & ", '" & PoolsNo.Replace("'", "''").Trim() & "', '" & MatchDate & "', '" & Venue.Replace("'", "''").Trim() & "', '" & Country.Replace("'", "''").Trim() & "', " & HomeTeamId & ", " & AwayTeamId & ", '" & HomeTeamName.Replace("'", "''").Trim() & "', '" & AwayTeamName.Replace("'", "''").Trim() & "', '" & Status.Replace("'", "''").Trim() & "', " & StatusId & ", '" & DateTime.Now & "', '" & DateTime.Now & "')", DatabaseConn)
                                    End If
                                    SqlQuery.ExecuteNonQuery()
                                Else
                                    MoveFile = False
                                    DatabaseError = True
                                End If
                            Next
                            xmlMatches = Nothing
                            xmlDoc = Nothing
                        Catch ex As Exception
                            MoveFile = False
                            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                                DatabaseError = True
                            Else
                                Call ErrorHandler(MatchId, ex.Source & "-" & ex.Message, "Error with the fixture")
                            End If
                        End Try
                        If MoveFile = True Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\Complete\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf DatabaseError = False Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        End If
                    End If
                Next

                updateListbox("Fixtures complete", True)
            End If
        End If

    End Sub

#End Region

#Region " Matches "

    Private Sub Matches()

        Dim File As String
        Dim Files() As String
        Dim Fi As FileInfo
        Dim MatchId As String = ""
        Dim Goals As Integer = 0
        Dim Matches As ArrayList = New ArrayList

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Match\Temporary") Then
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Match\Temporary")

            Dim Count As Integer = 0
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf(".xml") >= 0 Then
                    Count = Count + 1
                End If
            Next

            If Count > 0 Then
                updateListbox("Processing matches", True)

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And DbConnected = True Then
                        Dim MoveFile As Integer = 1
                        Try
                            Dim xmlDoc As New XmlDocument
                            xmlDoc.Load(File)

                            Dim xmlEvents As XmlNodeList = xmlDoc.SelectNodes("SoccerMatchPlus")
                            Dim I As Integer
                            For I = 0 To xmlEvents.Count - 1
                                MatchId = xmlEvents(I).Attributes.ItemOf("matchid").InnerText
                                Dim xmlTypes As XmlNodeList = xmlEvents(I).ChildNodes
                                Dim N As Integer
                                For N = 0 To xmlTypes.Count - 1
                                    Dim Type As String = xmlTypes(N).Name
                                    If DbConnected = True Then
                                        If Type = "Team" Then
                                            MoveFile = Teams(MatchId, xmlTypes(N))
                                        ElseIf Type = "Officials" Then
                                            MoveFile = Officials(MatchId, xmlTypes(N))
                                        ElseIf Type = "Booking" Then
                                            MoveFile = Booking(MatchId, xmlTypes(N))
                                        ElseIf Type = "Dismissal" Then
                                            MoveFile = Dismissal(MatchId, xmlTypes(N))
                                        ElseIf Type = "Substitution" Then
                                            MoveFile = Substitution(MatchId, xmlTypes(N))
                                        ElseIf Type = "Goal" Then
                                            MoveFile = Goal(MatchId, xmlTypes(N))
                                            If Not Matches.Contains(MatchId) Then
                                                Matches.Add(MatchId)
                                            End If
                                        ElseIf Type = "Penalty" Then
                                            MoveFile = Penalty(MatchId, xmlTypes(N))
                                            If Not Matches.Contains(MatchId) Then
                                                Matches.Add(MatchId)
                                            End If
                                        ElseIf Type = "ShootoutPenalty" Then
                                            MoveFile = PenaltyShootOut(MatchId, xmlTypes(N))
                                        ElseIf Type = "MatchStatus" Then
                                            MoveFile = Status(MatchId, xmlTypes(N))
                                        ElseIf Type = "Attendance" Then
                                            MoveFile = Attendance(MatchId, xmlTypes(N))
                                        ElseIf Type = "Managers" Then
                                            MoveFile = Managers(MatchId, xmlTypes(N))
                                        Else
                                            'updateListbox("1234", True)
                                            MoveFile = False
                                        End If
                                    Else
                                        MoveFile = 3
                                    End If
                                    If MoveFile > 1 Then
                                        Exit For
                                    End If
                                Next
                                xmlTypes = Nothing
                            Next
                            xmlEvents = Nothing
                            xmlDoc = Nothing
                        Catch ex As Exception
                            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                                MoveFile = 3
                            Else
                                MoveFile = 2
                                Call ErrorHandler(MatchId, ex.Message, "Error with the matches")
                            End If
                        End Try
                        If MoveFile = 1 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Match\Complete\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf MoveFile = 2 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Match\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        End If
                    End If
                Next

                updateListbox("Matches complete", True)
            End If
        End If

        If Matches.Count > 0 Then
            Call Scorers(Matches)
        End If

    End Sub

    Private Function Teams(ByVal MatchId As String, ByVal xmlTeam As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim TeamId As Integer
        Dim TeamName As String = ""
        Dim HomeTeam As Integer = 0
        Dim Correction As Boolean = False
        Dim QueryText As String = ""

        Try
            Dim Formation As String = ""
            If Not xmlTeam.SelectSingleNode("Formation") Is Nothing Then
                Formation = xmlTeam.SelectSingleNode("Formation").InnerText
            End If

            Dim tmpNodes As XmlAttributeCollection = xmlTeam.Attributes
            Dim M As Integer
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        TeamId = tmpNodes(M).InnerText
                    Case "name"
                        TeamName = tmpNodes(M).InnerText
                    Case "homeOrAway"
                        If tmpNodes(M).InnerText = "Home" Then
                            HomeTeam = 1
                        End If
                    Case "correction"
                        If tmpNodes(M).InnerText = "Yes" Then
                            Correction = True
                        End If
                    Case Else

                End Select
            Next

            If TeamName <> "" Then
                TeamName = FixTeamName(TeamId.ToString(), TeamName)
            End If

            Dim xmlPlayers As XmlNodeList = xmlTeam.SelectNodes("Players/Player")
            Dim I As Integer
            For I = 0 To xmlPlayers.Count - 1
                Dim Id As Integer = xmlPlayers(I).Attributes.ItemOf("id").InnerText
                Dim ShirtNumber As String = ""
                Dim Position As String = ""
                Dim Substitute As Integer = 0
                Dim Captain As Integer = 0
                tmpNodes = xmlPlayers(I).Attributes
                For M = 0 To tmpNodes.Count - 1
                    Dim tmpName As String = tmpNodes(M).Name
                    Select Case tmpName
                        Case "id"
                            Id = tmpNodes(M).InnerText
                        Case "shirtNumber"
                            ShirtNumber = tmpNodes(M).InnerText
                        Case "position"
                            Position = tmpNodes(M).InnerText
                        Case "substitute"
                            If tmpNodes(M).InnerText = "Yes" Then
                                Substitute = 1
                            End If
                        Case "captain"
                            If tmpNodes(M).InnerText = "Yes" Then
                                Captain = 1
                            End If
                        Case Else

                    End Select
                Next

                Dim FirstName As String = ""
                Dim Initials As String = ""
                Dim MiddleNames As String = ""
                Dim LastName As String = ""
                Dim Abbreviation As String = ""
                Dim FullName As String = xmlPlayers(I).SelectSingleNode("Name").InnerText
                tmpNodes = xmlPlayers(I).SelectSingleNode("Name").Attributes
                For M = 0 To tmpNodes.Count - 1
                    Dim tmpName As String = tmpNodes(M).Name
                    Select Case tmpName
                        Case "firstName"
                            FirstName = tmpNodes(M).InnerText
                        Case "initials"
                            Initials = tmpNodes(M).InnerText
                        Case "middleNames"
                            MiddleNames = tmpNodes(M).InnerText
                        Case "lastName"
                            LastName = tmpNodes(M).InnerText
                        Case "abbreviation"
                            Abbreviation = tmpNodes(M).InnerText
                        Case Else

                    End Select
                Next

                If I = 0 Then
                    QueryText = "Insert Into Soccer.dbo.pa_Teams (MatchId, TeamId, TeamName, HomeTeam, PlayerId, PlayerFirstName, PlayerLastname, PlayerInitials, PlayerFullName, Position, Substitute, Captain, ShirtNumber, Sorting, Formation, Modified, Created) Values ('" & MatchId & "', " & TeamId & ", '" & TeamName.Replace("'", "''") & "', " & HomeTeam & ", " & Id & ", '" & FirstName.Replace("'", "''") & "', '" & LastName.Replace("'", "''") & "', '" & Initials.Replace("'", "''") & "', '" & FullName.Replace("'", "''") & "', '" & Position.Replace("'", "''") & "', " & Substitute & ", " & Captain & ", '" & ShirtNumber.Replace("'", "''") & "', " & I & ", '" & Formation & "', '" & DateTime.Now() & "', '" & DateTime.Now() & "')"
                Else
                    QueryText &= ";" & Environment.NewLine & "Insert Into Soccer.dbo.pa_Teams (MatchId, TeamId, TeamName, HomeTeam, PlayerId, PlayerFirstName, PlayerLastname, PlayerInitials, PlayerFullName, Position, Substitute, Captain, ShirtNumber, Sorting, Formation, Modified, Created) Values ('" & MatchId & "', " & TeamId & ", '" & TeamName.Replace("'", "''") & "', " & HomeTeam & ", " & Id & ", '" & FirstName.Replace("'", "''") & "', '" & LastName.Replace("'", "''") & "', '" & Initials.Replace("'", "''") & "', '" & FullName.Replace("'", "''") & "', '" & Position.Replace("'", "''") & "', " & Substitute & ", " & Captain & ", '" & ShirtNumber.Replace("'", "''") & "', " & I & ", '" & Formation & "', '" & DateTime.Now() & "', '" & DateTime.Now() & "')"
                End If
            Next

            If QueryText <> "" Then
                If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                    If Correction = True Then
                        SqlQuery = New SqlCommand("Delete From Soccer.dbo.pa_Teams Where (MatchId = '" & MatchId & "') And (TeamId = " & TeamId & ")", DatabaseConn)
                        SqlQuery.ExecuteNonQuery()
                        SqlQuery = New SqlCommand(QueryText, DatabaseConn)
                        SqlQuery.ExecuteNonQuery()
                    Else
                        SqlQuery = New SqlCommand("Select Count(MatchId) From Soccer.dbo.pa_Teams Where (MatchId = '" & MatchId & "') And (TeamId = " & TeamId & ")", DatabaseConn)
                        Dim Present As Integer = SqlQuery.ExecuteScalar
                        If Present > 0 Then

                        Else
                            SqlQuery = New SqlCommand(QueryText, DatabaseConn)
                            SqlQuery.ExecuteNonQuery()
                        End If
                    End If
                Else
                    Completed = 3
                End If
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with the team")
            End If
        End Try

        Return Completed

    End Function

    Private Function Officials(ByVal MatchId As String, ByVal xmlOfficials As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim QueryText As String = ""

        Try
            Dim tmpNodeList As XmlNodeList = xmlOfficials.SelectNodes("Official")
            Dim I As Integer
            For I = 0 To tmpNodeList.Count - 1
                Dim Id As Integer = 0
                Dim Type As String = ""
                Dim POB As String = ""

                Dim tmpNodes As XmlAttributeCollection = tmpNodeList(I).Attributes
                Dim M As Integer
                For M = 0 To tmpNodes.Count - 1
                    Dim tmpName As String = tmpNodes(M).Name
                    Select Case tmpName
                        Case "id"
                            Id = tmpNodes(M).InnerText
                        Case "type"
                            Type = tmpNodes(M).InnerText
                        Case "pofb"
                            POB = tmpNodes(M).InnerText
                        Case Else

                    End Select
                Next

                Dim Initials As String = ""
                Dim LastName As String = ""
                Dim FullName As String = tmpNodeList(I).SelectSingleNode("Name").InnerText
                tmpNodes = tmpNodeList(I).SelectSingleNode("Name").Attributes
                For M = 0 To tmpNodes.Count - 1
                    Dim tmpName As String = tmpNodes(M).Name
                    Select Case tmpName
                        Case "initials"
                            Initials = tmpNodes(M).InnerText
                        Case "lastName"
                            LastName = tmpNodes(M).InnerText
                        Case Else

                    End Select
                Next

                If I = 0 Then
                    QueryText = "Insert Into Soccer.dbo.pa_Officials (MatchId, Id, Type, Initials, LastName, FullName, POB, Modified, Created) Values ('" & MatchId & "', " & Id & ", '" & Type.Replace("'", "''") & "', '" & Initials & "', '" & LastName.Replace("'", "''") & "', '" & FullName.Replace("'", "''") & "', '" & POB.Replace("'", "''") & "', '" & DateTime.Now() & "', '" & DateTime.Now() & "')"
                Else
                    QueryText &= ";" & Environment.NewLine & "Insert Into Soccer.dbo.pa_Officials (MatchId, Id, Type, Initials, LastName, FullName, POB, Modified, Created) Values ('" & MatchId & "', " & Id & ", '" & Type.Replace("'", "''") & "', '" & Initials & "', '" & LastName.Replace("'", "''") & "', '" & FullName.Replace("'", "''") & "', '" & POB.Replace("'", "''") & "', '" & DateTime.Now() & "', '" & DateTime.Now() & "')"
                End If
            Next

            If QueryText <> "" Then
                If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                    SqlQuery = New SqlCommand("Delete From Soccer.dbo.pa_Officials Where (MatchId = '" & MatchId & "')", DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                    SqlQuery = New SqlCommand(QueryText, DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                Else
                    Completed = 3
                End If
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with the officials")
            End If
        End Try

        Return Completed

    End Function

    Private Function Booking(ByVal MatchId As String, ByVal xmlBooking As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim Id As String = ""
        Dim Time As String = ""
        Dim Reason As String = ""
        Dim Deletion As Boolean = False
        Dim Correction As Boolean = False
        Dim Rescinded As Integer = 0
        Dim TeamId As Integer = 0
        Dim TeamName As String = ""
        Dim HomeTeam As Integer = 0
        Dim PlayerId As Integer = 0
        Dim PlayerFirstname As String = ""
        Dim PlayerInitials As String = ""
        Dim PlayerLastName As String = ""
        Dim PlayerFullName As String = ""

        Try
            Dim tmpNodes As XmlAttributeCollection = xmlBooking.Attributes
            Dim M As Integer
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        Id = tmpNodes(M).InnerText
                    Case "deletion"
                        If tmpNodes(M).InnerText = "Yes" Then
                            Deletion = True
                        End If
                    Case "correction"
                        If tmpNodes(M).InnerText = "Yes" Then
                            Correction = True
                        End If
                    Case "time"
                        Time = tmpNodes(M).InnerText
                    Case "reason"
                        Reason = tmpNodes(M).InnerText
                    Case "rescinded"
                        If tmpNodes(M).InnerText = "Yes" Then
                            Rescinded = 1
                        End If
                    Case Else

                End Select
            Next

            Dim tmpMinute As Integer = 0
            Dim tmpSecond As Integer = 0
            Dim tmpExtraMinute As Integer = 0
            Dim tmpExtraSecond As Integer = 0
            If Time <> "" Then
                If Time.IndexOf("(") >= 0 Then
                    Dim tmpArray() As String = Time.Split("(")
                    If tmpArray(0).IndexOf(":") >= 0 Then
                        tmpMinute = Minute(tmpArray(0))
                        tmpSecond = Second(tmpArray(0))
                    Else
                        tmpMinute = Minute(tmpArray(0))
                    End If
                    If tmpArray(1).IndexOf(":") >= 0 Then
                        tmpExtraMinute = Minute(tmpArray(1).Replace(")", ""))
                        tmpExtraSecond = Second(tmpArray(1).Replace(")", ""))
                    Else
                        tmpExtraMinute = Minute(tmpArray(1).Replace(")", ""))
                    End If
                Else
                    If Time.IndexOf(":") >= 0 Then
                        tmpMinute = Minute(Time)
                        tmpSecond = Second(Time)
                    Else
                        tmpMinute = Minute(Time)
                    End If
                End If
            End If

            tmpNodes = xmlBooking.SelectSingleNode("Player").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        PlayerId = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            PlayerFullName = xmlBooking.SelectSingleNode("Player/Name").InnerText
            tmpNodes = xmlBooking.SelectSingleNode("Player/Name").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "firstName"
                        PlayerFirstname = tmpNodes(M).InnerText
                    Case "initials"
                        PlayerInitials = tmpNodes(M).InnerText
                    Case "lastName"
                        PlayerLastName = tmpNodes(M).InnerText
                    Case "abbreviation"
                        PlayerFullName = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            tmpNodes = xmlBooking.SelectSingleNode("Team").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        TeamId = tmpNodes(M).InnerText
                    Case "name"
                        TeamName = tmpNodes(M).InnerText
                    Case "homeOrAway"
                        If tmpNodes(M).InnerText = "Home" Then
                            HomeTeam = 1
                        End If
                    Case Else

                End Select
            Next

            If TeamName <> "" Then
                TeamName = FixTeamName(TeamId.ToString(), TeamName)
            End If

            If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                SqlQuery = New SqlCommand("Delete From Soccer.dbo.pa_Events Where (MatchId = '" & MatchId & "') And (Id = " & Id & ") And (EventId = 8)", DatabaseConn)
                SqlQuery.ExecuteNonQuery()
                If Deletion = False Then
                    SqlQuery = New SqlCommand("Insert Into Soccer.dbo.pa_Events (MatchId, Id, TeamId, TeamName, HomeTeam, Player1Id, Player1FirstName, Player1LastName, Player1Initials, Player1Fullname, EventId, Minutes, Seconds, ExtraMinutes, ExtraSeconds, txtTime, Comments, HasScore, Modified, Created) Values ('" & MatchId & "', " & Id & ", " & TeamId & ", '" & TeamName.Replace("'", "''") & "', " & HomeTeam & ", " & PlayerId & ", '" & PlayerFirstname.Replace("'", "''") & "', '" & PlayerLastName.Replace("'", "''") & "', '" & PlayerInitials.Replace("'", "''") & "', '" & PlayerFullName.Replace("'", "''") & "', 8, " & tmpMinute & ", " & tmpSecond & ", " & tmpExtraMinute & ", " & tmpExtraSecond & ", '" & Time.Replace("'", "''") & "', '" & Reason.Replace("'", "''") & "', 0, '" & DateTime.Now & "', '" & DateTime.Now & "')", DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                End If
            Else
                Completed = 3
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with a booking")
            End If
        End Try

        Return Completed

    End Function

    Private Function Dismissal(ByVal MatchId As String, ByVal xmlDismissal As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim Id As String = ""
        Dim Time As String = ""
        Dim Reason As String = ""
        Dim Deletion As Boolean = False
        Dim Correction As Boolean = False
        Dim Rescinded As Integer = 0
        Dim TeamId As Integer = 0
        Dim TeamName As String = ""
        Dim HomeTeam As Integer = 0
        Dim PlayerId As Integer = 0
        Dim PlayerFirstname As String = ""
        Dim PlayerInitials As String = ""
        Dim PlayerLastName As String = ""
        Dim PlayerFullName As String = ""

        Try
            Dim tmpNodes As XmlAttributeCollection = xmlDismissal.Attributes
            Dim M As Integer
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        Id = tmpNodes(M).InnerText
                    Case "deletion"
                        If tmpNodes(M).InnerText = "Yes" Then
                            Deletion = True
                        End If
                    Case "correction"
                        If tmpNodes(M).InnerText = "Yes" Then
                            Correction = True
                        End If
                    Case "time"
                        Time = tmpNodes(M).InnerText
                    Case "reason"
                        Reason = tmpNodes(M).InnerText
                    Case "rescinded"
                        If tmpNodes(M).InnerText = "Yes" Then
                            Rescinded = 1
                        End If
                    Case Else

                End Select
            Next

            Dim tmpMinute As Integer = 0
            Dim tmpSecond As Integer = 0
            Dim tmpExtraMinute As Integer = 0
            Dim tmpExtraSecond As Integer = 0
            If Time <> "" Then
                If Time.IndexOf("(") >= 0 Then
                    Dim tmpArray() As String = Time.Split("(")
                    If tmpArray(0).IndexOf(":") >= 0 Then
                        tmpMinute = Minute(tmpArray(0))
                        tmpSecond = Second(tmpArray(0))
                    Else
                        tmpMinute = Minute(tmpArray(0))
                    End If
                    If tmpArray(1).IndexOf(":") >= 0 Then
                        tmpExtraMinute = Minute(tmpArray(1).Replace(")", ""))
                        tmpExtraSecond = Second(tmpArray(1).Replace(")", ""))
                    Else
                        tmpExtraMinute = Minute(tmpArray(1).Replace(")", ""))
                    End If
                Else
                    If Time.IndexOf(":") >= 0 Then
                        tmpMinute = Minute(Time)
                        tmpSecond = Second(Time)
                    Else
                        tmpMinute = Minute(Time)
                    End If
                End If
            End If

            tmpNodes = xmlDismissal.SelectSingleNode("Player").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        PlayerId = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            PlayerFullName = xmlDismissal.SelectSingleNode("Player/Name").InnerText
            tmpNodes = xmlDismissal.SelectSingleNode("Player/Name").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "firstName"
                        PlayerFirstname = tmpNodes(M).InnerText
                    Case "initials"
                        PlayerInitials = tmpNodes(M).InnerText
                    Case "lastName"
                        PlayerLastName = tmpNodes(M).InnerText
                    Case "abbreviation"
                        PlayerFullName = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            tmpNodes = xmlDismissal.SelectSingleNode("Team").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        TeamId = tmpNodes(M).InnerText
                    Case "name"
                        TeamName = tmpNodes(M).InnerText
                    Case "homeOrAway"
                        If tmpNodes(M).InnerText = "Home" Then
                            HomeTeam = 1
                        End If
                    Case Else

                End Select
            Next

            If TeamName <> "" Then
                TeamName = FixTeamName(TeamId.ToString(), TeamName)
            End If

            If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                SqlQuery = New SqlCommand("Delete From Soccer.dbo.pa_Events Where (MatchId = '" & MatchId & "') And (Id = " & Id & ") And (EventId = 9)", DatabaseConn)
                SqlQuery.ExecuteNonQuery()
                If Deletion = False Then
                    SqlQuery = New SqlCommand("Insert Into Soccer.dbo.pa_Events (MatchId, Id, TeamId, TeamName, HomeTeam, Player1Id, Player1FirstName, Player1LastName, Player1Initials, Player1Fullname, EventId, Minutes, Seconds, ExtraMinutes, ExtraSeconds, txtTime, Comments, HasScore, Modified, Created) Values ('" & MatchId & "', " & Id & ", " & TeamId & ", '" & TeamName.Replace("'", "''") & "', " & HomeTeam & ", " & PlayerId & ", '" & PlayerFirstname.Replace("'", "''") & "', '" & PlayerLastName.Replace("'", "''") & "', '" & PlayerInitials.Replace("'", "''") & "', '" & PlayerFullName.Replace("'", "''") & "', 9, " & tmpMinute & ", " & tmpSecond & ", " & tmpExtraMinute & ", " & tmpExtraSecond & ", '" & Time.Replace("'", "''") & "', '" & Reason.Replace("'", "''") & "', 0, '" & DateTime.Now & "', '" & DateTime.Now & "')", DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                End If
            Else
                Completed = 3
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with a dismissal")
            End If
        End Try

        Return Completed

    End Function

    Private Function Substitution(ByVal MatchId As String, ByVal xmlSubstitution As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim Id As String = ""
        Dim Time As String = ""
        Dim Reason As String = ""
        Dim Deletion As Boolean = False
        Dim Correction As Boolean = False
        Dim TeamId As Integer = 0
        Dim TeamName As String = ""
        Dim HomeTeam As Integer = 0
        Dim Player1Id As Integer = 0
        Dim Player1Firstname As String = ""
        Dim Player1Initials As String = ""
        Dim Player1LastName As String = ""
        Dim Player1FullName As String = ""
        Dim Player2Id As Integer = 0
        Dim Player2Firstname As String = ""
        Dim Player2Initials As String = ""
        Dim Player2LastName As String = ""
        Dim Player2FullName As String = ""

        Try
            Dim tmpNodes As XmlAttributeCollection = xmlSubstitution.Attributes
            Dim M As Integer
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        Id = tmpNodes(M).InnerText
                    Case "deletion"
                        If tmpNodes(M).InnerText = "Yes" Then
                            Deletion = True
                        End If
                    Case "correction"
                        If tmpNodes(M).InnerText = "Yes" Then
                            Correction = True
                        End If
                    Case "time"
                        Time = tmpNodes(M).InnerText
                    Case "reason"
                        Reason = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            Dim tmpMinute As Integer = 0
            Dim tmpSecond As Integer = 0
            Dim tmpExtraMinute As Integer = 0
            Dim tmpExtraSecond As Integer = 0
            If Time <> "" Then
                If Time.IndexOf("(") >= 0 Then
                    Dim tmpArray() As String = Time.Split("(")
                    If tmpArray(0).IndexOf(":") >= 0 Then
                        tmpMinute = Minute(tmpArray(0))
                        tmpSecond = Second(tmpArray(0))
                    Else
                        tmpMinute = Minute(tmpArray(0))
                    End If
                    If tmpArray(1).IndexOf(":") >= 0 Then
                        tmpExtraMinute = Minute(tmpArray(1).Replace(")", ""))
                        tmpExtraSecond = Second(tmpArray(1).Replace(")", ""))
                    Else
                        tmpExtraMinute = Minute(tmpArray(1).Replace(")", ""))
                    End If
                Else
                    If Time.IndexOf(":") >= 0 Then
                        tmpMinute = Minute(Time)
                        tmpSecond = Second(Time)
                    Else
                        tmpMinute = Minute(Time)
                    End If
                End If
            End If

            tmpNodes = xmlSubstitution.SelectSingleNode("PlayerOff/Player").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        Player1Id = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            Player1FullName = xmlSubstitution.SelectSingleNode("PlayerOff/Player/Name").InnerText
            tmpNodes = xmlSubstitution.SelectSingleNode("PlayerOff/Player/Name").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "firstName"
                        Player1Firstname = tmpNodes(M).InnerText
                    Case "initials"
                        Player1Initials = tmpNodes(M).InnerText
                    Case "lastName"
                        Player1LastName = tmpNodes(M).InnerText
                    Case "abbreviation"
                        Player1FullName = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            tmpNodes = xmlSubstitution.SelectSingleNode("PlayerOn/Player").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        Player2Id = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            Player2FullName = xmlSubstitution.SelectSingleNode("PlayerOn/Player/Name").InnerText
            tmpNodes = xmlSubstitution.SelectSingleNode("PlayerOn/Player/Name").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "firstName"
                        Player2Firstname = tmpNodes(M).InnerText
                    Case "initials"
                        Player2Initials = tmpNodes(M).InnerText
                    Case "lastName"
                        Player2LastName = tmpNodes(M).InnerText
                    Case "abbreviation"
                        Player2FullName = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            tmpNodes = xmlSubstitution.SelectSingleNode("Team").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        TeamId = tmpNodes(M).InnerText
                    Case "name"
                        TeamName = tmpNodes(M).InnerText
                    Case "homeOrAway"
                        If tmpNodes(M).InnerText = "Home" Then
                            HomeTeam = 1
                        End If
                    Case Else

                End Select
            Next

            If TeamName <> "" Then
                TeamName = FixTeamName(TeamId.ToString(), TeamName)
            End If

            If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                SqlQuery = New SqlCommand("Delete From Soccer.dbo.pa_Events Where (MatchId = '" & MatchId & "') And (Id = " & Id & ") And (EventId = 10)", DatabaseConn)
                SqlQuery.ExecuteNonQuery()
                If Deletion = False Then
                    SqlQuery = New SqlCommand("Insert Into Soccer.dbo.pa_Events (MatchId, Id, TeamId, TeamName, HomeTeam, Player1Id, Player1FirstName, Player1LastName, Player1Initials, Player1Fullname, Player2Id, Player2FirstName, Player2LastName, Player2Initials, Player2Fullname, EventId, Minutes, Seconds, ExtraMinutes, ExtraSeconds, txtTime, Comments, HasScore, Modified, Created) Values ('" & MatchId & "', " & Id & ", " & TeamId & ", '" & TeamName.Replace("'", "''") & "', " & HomeTeam & ", " & Player1Id & ", '" & Player1Firstname.Replace("'", "''") & "', '" & Player1LastName.Replace("'", "''") & "', '" & Player1Initials.Replace("'", "''") & "', '" & Player1FullName.Replace("'", "''") & "', " & Player2Id & ", '" & Player2Firstname.Replace("'", "''") & "', '" & Player2LastName.Replace("'", "''") & "', '" & Player2Initials.Replace("'", "''") & "', '" & Player2FullName.Replace("'", "''") & "', 10, " & tmpMinute & ", " & tmpSecond & ", " & tmpExtraMinute & ", " & tmpExtraSecond & ", '" & Time.Replace("'", "''") & "', '" & Reason.Replace("'", "''") & "', 0, '" & DateTime.Now & "', '" & DateTime.Now & "')", DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                End If
            Else
                Completed = 3
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with a substitution")
            End If
        End Try

        Return Completed

    End Function

    Private Function Goal(ByVal MatchId As String, ByVal xmlGoal As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim Id As String = ""
        Dim Time As String = ""
        Dim Deletion As Boolean = False
        Dim Correction As Boolean = False
        Dim OwnGoal As Integer = 0
        Dim GoldenGoal As Integer = 0
        Dim TeamId As Integer = 0
        Dim TeamName As String = ""
        Dim HomeTeam As Integer = 0
        Dim PlayerId As Integer = 0
        Dim PlayerFirstname As String = ""
        Dim PlayerInitials As String = ""
        Dim PlayerLastName As String = ""
        Dim PlayerFullName As String = ""
        Dim EventId As Integer = 2
        Dim HasScore As Integer = 1
        Dim HasAggregate As Integer = 0
        Dim HomeTeamScore As Integer = 0
        Dim AwayTeamScore As Integer = 0
        Dim HomeTeamAggregateScore As Integer = 0
        Dim AwayTeamAggregateScore As Integer = 0

        Try
            Dim tmpNodes As XmlAttributeCollection = xmlGoal.Attributes
            Dim M As Integer
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        Id = tmpNodes(M).InnerText
                    Case "deletion"
                        If tmpNodes(M).InnerText = "Yes" Then
                            Deletion = True
                        End If
                    Case "correction"
                        If tmpNodes(M).InnerText = "Yes" Then
                            Correction = True
                        End If
                    Case "time"
                        Time = tmpNodes(M).InnerText
                    Case "ownGoal"
                        If tmpNodes(M).InnerText = "Yes" Then
                            OwnGoal = 1
                        End If
                    Case "goldenGoal"
                        If tmpNodes(M).InnerText = "Yes" Then
                            GoldenGoal = 1
                        End If
                    Case Else

                End Select
            Next

            If OwnGoal = 1 And GoldenGoal = 1 Then
                EventId = 11
            ElseIf OwnGoal = 1 Then
                EventId = 3
            ElseIf GoldenGoal = 1 Then
                EventId = 4
            End If

            Dim tmpMinute As Integer = 0
            Dim tmpSecond As Integer = 0
            Dim tmpExtraMinute As Integer = 0
            Dim tmpExtraSecond As Integer = 0
            If Time <> "" Then
                If Time.IndexOf("(") >= 0 Then
                    Dim tmpArray() As String = Time.Split("(")
                    If tmpArray(0).IndexOf(":") >= 0 Then
                        tmpMinute = Minute(tmpArray(0))
                        tmpSecond = Second(tmpArray(0))
                    Else
                        tmpMinute = Minute(tmpArray(0))
                    End If
                    If tmpArray(1).IndexOf(":") >= 0 Then
                        tmpExtraMinute = Minute(tmpArray(1).Replace(")", ""))
                        tmpExtraSecond = Second(tmpArray(1).Replace(")", ""))
                    Else
                        tmpExtraMinute = Minute(tmpArray(1).Replace(")", ""))
                    End If
                Else
                    If Time.IndexOf(":") >= 0 Then
                        tmpMinute = Minute(Time)
                        tmpSecond = Second(Time)
                    Else
                        tmpMinute = Minute(Time)
                    End If
                End If
            End If

            tmpNodes = xmlGoal.SelectSingleNode("Player").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        PlayerId = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            PlayerFullName = xmlGoal.SelectSingleNode("Player/Name").InnerText
            tmpNodes = xmlGoal.SelectSingleNode("Player/Name").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "firstName"
                        PlayerFirstname = tmpNodes(M).InnerText
                    Case "initials"
                        PlayerInitials = tmpNodes(M).InnerText
                    Case "lastName"
                        PlayerLastName = tmpNodes(M).InnerText
                    Case "abbreviation"
                        PlayerFullName = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            tmpNodes = xmlGoal.SelectSingleNode("Team").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        TeamId = tmpNodes(M).InnerText
                    Case "name"
                        TeamName = tmpNodes(M).InnerText
                    Case "homeOrAway"
                        If tmpNodes(M).InnerText = "Home" Then
                            HomeTeam = 1
                        End If
                    Case Else

                End Select
            Next

            If TeamName <> "" Then
                TeamName = FixTeamName(TeamId.ToString(), TeamName)
            End If

            tmpNodes = xmlGoal.SelectSingleNode("Score").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "home"
                        HomeTeamScore = tmpNodes(M).InnerText
                    Case "away"
                        AwayTeamScore = tmpNodes(M).InnerText
                    Case "aggregateHome"
                        HasAggregate = 1
                        HomeTeamAggregateScore = tmpNodes(M).InnerText
                    Case "aggregateAway"
                        HasAggregate = 1
                        AwayTeamAggregateScore = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                SqlQuery = New SqlCommand("Delete From Soccer.dbo.pa_Events Where (MatchId = '" & MatchId & "') And (Id = " & Id & ") And (EventId = 2 Or EventId = 3 Or EventId = 4 Or EventId = 11)", DatabaseConn)
                SqlQuery.ExecuteNonQuery()
                If Deletion = False Then
                    SqlQuery = New SqlCommand("Insert Into Soccer.dbo.pa_Events (MatchId, Id, TeamId, TeamName, HomeTeam, Player1Id, Player1FirstName, Player1LastName, Player1Initials, Player1Fullname, EventId, Minutes, Seconds, ExtraMinutes, ExtraSeconds, txtTime, HasScore, HasAggregate, HomeTeamScore, AwayTeamScore, HomeTeamAggregate, AwayTeamAggregate, Modified, Created) Values ('" & MatchId & "', " & Id & ", " & TeamId & ", '" & TeamName.Replace("'", "''") & "', " & HomeTeam & ", " & PlayerId & ", '" & PlayerFirstname.Replace("'", "''") & "', '" & PlayerLastName.Replace("'", "''") & "', '" & PlayerInitials.Replace("'", "''") & "', '" & PlayerFullName.Replace("'", "''") & "', " & EventId & ", " & tmpMinute & ", " & tmpSecond & ", " & tmpExtraMinute & ", " & tmpExtraSecond & ", '" & Time.Replace("'", "''") & "', " & HasScore & ", " & HasAggregate & ", " & HomeTeamScore & ", " & AwayTeamScore & ", " & HomeTeamAggregateScore & ", " & AwayTeamAggregateScore & ", '" & DateTime.Now & "', '" & DateTime.Now & "')", DatabaseConn)
                    SqlQuery.ExecuteNonQuery()

                    If HomeTeamScore > 0 Or AwayTeamScore > 0 Then
                        SqlQuery = New SqlCommand("Select HomeTeamScore + AwayTeamScore As Total From Soccer.dbo.pa_Matches Where (Id = '" & MatchId & "')", DatabaseConn)
                        Dim Total As Integer = SqlQuery.ExecuteScalar
                        If HomeTeamScore + AwayTeamScore > Total Then
                            SqlQuery = New SqlCommand("Update Soccer.dbo.pa_Matches Set HomeTeamScore = " & HomeTeamScore & ", AwayTeamScore = " & AwayTeamScore & " Where (Id = '" & MatchId & "')", DatabaseConn)
                            SqlQuery.ExecuteNonQuery()
                        End If
                    End If
                Else
                    SqlQuery = New SqlCommand("Update Soccer.dbo.pa_Matches Set HomeTeamScore = " & HomeTeamScore & ", AwayTeamScore = " & AwayTeamScore & " Where (Id = '" & MatchId & "')", DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                End If

                If Correction = False Then
                    SqlQuery = New SqlCommand("Select Competition From Soccer.dbo.pa_Matches Where (Id = '" & MatchId & "')", DatabaseConn)
                    Dim tmpCompetition As Integer = SqlQuery.ExecuteScalar
                    If mtnTournaments.Contains(tmpCompetition) Then
                        'mtnMatchSms(tmpCompetition, MatchId, "goal", Id)
                    End If
                End If
            Else
                Completed = 3
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with a goal")
            End If
        End Try

        Return Completed

    End Function

    Private Function Penalty(ByVal MatchId As String, ByVal xmlPenalty As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim Id As String = ""
        Dim Time As String = ""
        Dim Deletion As Boolean = False
        Dim Correction As Boolean = False
        Dim Comments As String = ""
        Dim TeamId As Integer = 0
        Dim TeamName As String = ""
        Dim HomeTeam As Integer = 0
        Dim PlayerId As Integer = 0
        Dim PlayerFirstname As String = ""
        Dim PlayerInitials As String = ""
        Dim PlayerLastName As String = ""
        Dim PlayerFullName As String = ""
        Dim EventId As Integer = 5
        Dim HasScore As Integer = 1
        Dim HasAggregate As Integer = 0
        Dim HomeTeamScore As Integer = 0
        Dim AwayTeamScore As Integer = 0
        Dim HomeTeamAggregateScore As Integer = 0
        Dim AwayTeamAggregateScore As Integer = 0

        Try
            Dim tmpNodes As XmlAttributeCollection = xmlPenalty.Attributes
            Dim M As Integer
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        Id = tmpNodes(M).InnerText
                    Case "deletion"
                        If tmpNodes(M).InnerText = "Yes" Then
                            Deletion = True
                        End If
                    Case "correction"
                        If tmpNodes(M).InnerText = "Yes" Then
                            Correction = True
                        End If
                    Case "time"
                        Time = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            Dim tmpMinute As Integer = 0
            Dim tmpSecond As Integer = 0
            Dim tmpExtraMinute As Integer = 0
            Dim tmpExtraSecond As Integer = 0
            If Time <> "" Then
                If Time.IndexOf("(") >= 0 Then
                    Dim tmpArray() As String = Time.Split("(")
                    If tmpArray(0).IndexOf(":") >= 0 Then
                        tmpMinute = Minute(tmpArray(0))
                        tmpSecond = Second(tmpArray(0))
                    Else
                        tmpMinute = Minute(tmpArray(0))
                    End If
                    If tmpArray(1).IndexOf(":") >= 0 Then
                        tmpExtraMinute = Minute(tmpArray(1).Replace(")", ""))
                        tmpExtraSecond = Second(tmpArray(1).Replace(")", ""))
                    Else
                        tmpExtraMinute = Minute(tmpArray(1).Replace(")", ""))
                    End If
                Else
                    If Time.IndexOf(":") >= 0 Then
                        tmpMinute = Minute(Time)
                        tmpSecond = Second(Time)
                    Else
                        tmpMinute = Minute(Time)
                    End If
                End If
            End If

            tmpNodes = xmlPenalty.SelectSingleNode("TakenBy/Player").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        PlayerId = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            PlayerFullName = xmlPenalty.SelectSingleNode("TakenBy/Player/Name").InnerText
            tmpNodes = xmlPenalty.SelectSingleNode("TakenBy/Player/Name").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "firstName"
                        PlayerFirstname = tmpNodes(M).InnerText
                    Case "initials"
                        PlayerInitials = tmpNodes(M).InnerText
                    Case "lastName"
                        PlayerLastName = tmpNodes(M).InnerText
                    Case "abbreviation"
                        PlayerFullName = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            tmpNodes = xmlPenalty.SelectSingleNode("Team").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        TeamId = tmpNodes(M).InnerText
                    Case "name"
                        TeamName = tmpNodes(M).InnerText
                    Case "homeOrAway"
                        If tmpNodes(M).InnerText = "Home" Then
                            HomeTeam = 1
                        End If
                    Case Else

                End Select
            Next

            If TeamName <> "" Then
                TeamName = FixTeamName(TeamId.ToString(), TeamName)
            End If

            tmpNodes = xmlPenalty.SelectSingleNode("Score").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "home"
                        HomeTeamScore = tmpNodes(M).InnerText
                    Case "away"
                        AwayTeamScore = tmpNodes(M).InnerText
                    Case "aggregateHome"
                        HasAggregate = 1
                        HomeTeamAggregateScore = tmpNodes(M).InnerText
                    Case "aggregateAway"
                        HasAggregate = 1
                        AwayTeamAggregateScore = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            tmpNodes = xmlPenalty.SelectSingleNode("PenaltyOutcome").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "penaltyOutcome"
                        If tmpNodes(M).InnerText = "Scored" Then
                            EventId = 5
                        ElseIf tmpNodes(M).InnerText = "Missed" Then
                            EventId = 6
                        ElseIf tmpNodes(M).InnerText = "Saved" Then
                            EventId = 7
                        End If
                    Case "comments"
                        Comments = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                SqlQuery = New SqlCommand("Delete From Soccer.dbo.pa_Events Where (MatchId = '" & MatchId & "') And (Id = " & Id & ") And (EventId = " & EventId & ")", DatabaseConn)
                SqlQuery.ExecuteNonQuery()
                If Deletion = False Then
                    SqlQuery = New SqlCommand("Insert Into Soccer.dbo.pa_Events (MatchId, Id, TeamId, TeamName, HomeTeam, Player1Id, Player1FirstName, Player1LastName, Player1Initials, Player1Fullname, EventId, Minutes, Seconds, ExtraMinutes, ExtraSeconds, txtTime, Comments, HasScore, HasAggregate, HomeTeamScore, AwayTeamScore, HomeTeamAggregate, AwayTeamAggregate, Modified, Created) Values ('" & MatchId & "', " & Id & ", " & TeamId & ", '" & TeamName.Replace("'", "''") & "', " & HomeTeam & ", " & PlayerId & ", '" & PlayerFirstname.Replace("'", "''") & "', '" & PlayerLastName.Replace("'", "''") & "', '" & PlayerInitials.Replace("'", "''") & "', '" & PlayerFullName.Replace("'", "''") & "', " & EventId & ", " & tmpMinute & ", " & tmpSecond & ", " & tmpExtraMinute & ", " & tmpExtraSecond & ", '" & Time.Replace("'", "''") & "', '" & Comments.Replace("'", "''") & "', " & HasScore & ", " & HasAggregate & ", " & HomeTeamScore & ", " & AwayTeamScore & ", " & HomeTeamAggregateScore & ", " & AwayTeamAggregateScore & ", '" & DateTime.Now & "', '" & DateTime.Now & "')", DatabaseConn)
                    SqlQuery.ExecuteNonQuery()

                    If HomeTeamScore > 0 Or AwayTeamScore > 0 Then
                        SqlQuery = New SqlCommand("Select HomeTeamScore + AwayTeamScore As Total From Soccer.dbo.pa_Matches Where (Id = '" & MatchId & "')", DatabaseConn)
                        Dim Total As Integer = SqlQuery.ExecuteScalar
                        If HomeTeamScore + AwayTeamScore > Total Then
                            SqlQuery = New SqlCommand("Update Soccer.dbo.pa_Matches Set HomeTeamScore = " & HomeTeamScore & ", AwayTeamScore = " & AwayTeamScore & " Where (Id = '" & MatchId & "')", DatabaseConn)
                            SqlQuery.ExecuteNonQuery()
                        End If
                    End If
                Else
                    SqlQuery = New SqlCommand("Update Soccer.dbo.pa_Matches Set HomeTeamScore = " & HomeTeamScore & ", AwayTeamScore = " & AwayTeamScore & " Where (Id = '" & MatchId & "')", DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                End If

                If Correction = False And EventId = 5 Then
                    SqlQuery = New SqlCommand("Select Competition From Soccer.dbo.pa_Matches Where (Id = '" & MatchId & "')", DatabaseConn)
                    Dim tmpCompetition As Integer = SqlQuery.ExecuteScalar
                    If mtnTournaments.Contains(tmpCompetition) Then
                        'mtnMatchSms(tmpCompetition, MatchId, "goal", Id)
                    End If
                End If
            Else
                Completed = 3
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with a penalty")
            End If
        End Try

        Return Completed

    End Function

    Private Function PenaltyShootOut(ByVal MatchId As String, ByVal xmlPenalty As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim Id As String = ""
        Dim Time As String = ""
        Dim Deletion As Boolean = False
        Dim Correction As Boolean = False
        Dim Comments As String = ""
        Dim TeamId As Integer = 0
        Dim TeamName As String = ""
        Dim HomeTeam As Integer = 0
        Dim PlayerId As Integer = 0
        Dim PlayerFirstname As String = ""
        Dim PlayerInitials As String = ""
        Dim PlayerLastName As String = ""
        Dim PlayerFullName As String = ""
        Dim EventId As Integer = 5
        Dim HasScore As Integer = 1
        Dim HasAggregate As Integer = 0
        Dim HomeTeamScore As Integer = 0
        Dim AwayTeamScore As Integer = 0
        Dim HomeTeamAggregateScore As Integer = 0
        Dim AwayTeamAggregateScore As Integer = 0

        Try
            Dim tmpNodes As XmlAttributeCollection = xmlPenalty.Attributes
            Dim M As Integer
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        Id = tmpNodes(M).InnerText
                    Case "deletion"
                        If tmpNodes(M).InnerText = "Yes" Then
                            Deletion = True
                        End If
                    Case "correction"
                        If tmpNodes(M).InnerText = "Yes" Then
                            Correction = True
                        End If
                    Case "time"
                        Time = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            Dim tmpMinute As Integer = 0
            Dim tmpSecond As Integer = 0
            Dim tmpExtraMinute As Integer = 0
            Dim tmpExtraSecond As Integer = 0
            If Time <> "" Then
                If Time.IndexOf("(") >= 0 Then
                    Dim tmpArray() As String = Time.Split("(")
                    If tmpArray(0).IndexOf(":") >= 0 Then
                        tmpMinute = Minute(tmpArray(0))
                        tmpSecond = Second(tmpArray(0))
                    Else
                        tmpMinute = Minute(tmpArray(0))
                    End If
                    If tmpArray(1).IndexOf(":") >= 0 Then
                        tmpExtraMinute = Minute(tmpArray(1).Replace(")", ""))
                        tmpExtraSecond = Second(tmpArray(1).Replace(")", ""))
                    Else
                        tmpExtraMinute = Minute(tmpArray(1).Replace(")", ""))
                    End If
                Else
                    If Time.IndexOf(":") >= 0 Then
                        tmpMinute = Minute(Time)
                        tmpSecond = Second(Time)
                    Else
                        tmpMinute = Minute(Time)
                    End If
                End If
            End If

            tmpNodes = xmlPenalty.SelectSingleNode("TakenBy/Player").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        PlayerId = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            PlayerFullName = xmlPenalty.SelectSingleNode("TakenBy/Player/Name").InnerText
            tmpNodes = xmlPenalty.SelectSingleNode("TakenBy/Player/Name").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "firstName"
                        PlayerFirstname = tmpNodes(M).InnerText
                    Case "initials"
                        PlayerInitials = tmpNodes(M).InnerText
                    Case "lastName"
                        PlayerLastName = tmpNodes(M).InnerText
                    Case "abbreviation"
                        PlayerFullName = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            tmpNodes = xmlPenalty.SelectSingleNode("Team").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "id"
                        TeamId = tmpNodes(M).InnerText
                    Case "name"
                        TeamName = tmpNodes(M).InnerText
                    Case "homeOrAway"
                        If tmpNodes(M).InnerText = "Home" Then
                            HomeTeam = 1
                        End If
                    Case Else

                End Select
            Next

            If TeamName <> "" Then
                TeamName = FixTeamName(TeamId.ToString(), TeamName)
            End If

            tmpNodes = xmlPenalty.SelectSingleNode("PenaltyScore").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "home"
                        HomeTeamScore = tmpNodes(M).InnerText
                    Case "away"
                        AwayTeamScore = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            tmpNodes = xmlPenalty.SelectSingleNode("PenaltyOutcome").Attributes
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "penaltyOutcome"
                        If tmpNodes(M).InnerText = "Scored" Then
                            EventId = 13
                        ElseIf tmpNodes(M).InnerText = "Missed" Then
                            EventId = 14
                        ElseIf tmpNodes(M).InnerText = "Saved" Then
                            EventId = 15
                        End If
                    Case "comments"
                        Comments = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                SqlQuery = New SqlCommand("Delete From Soccer.dbo.pa_Events Where (MatchId = '" & MatchId & "') And (Id = " & Id & ") And (EventId = " & EventId & ")", DatabaseConn)
                SqlQuery.ExecuteNonQuery()
                If Deletion = False Then
                    SqlQuery = New SqlCommand("Insert Into Soccer.dbo.pa_Events (MatchId, Id, TeamId, TeamName, HomeTeam, Player1Id, Player1FirstName, Player1LastName, Player1Initials, Player1Fullname, EventId, PenaltyScoreHome, PenaltyScoreAway, Modified, Created) Values ('" & MatchId & "', " & Id & ", " & TeamId & ", '" & TeamName.Replace("'", "''") & "', " & HomeTeam & ", " & PlayerId & ", '" & PlayerFirstname.Replace("'", "''") & "', '" & PlayerLastName.Replace("'", "''") & "', '" & PlayerInitials.Replace("'", "''") & "', '" & PlayerFullName.Replace("'", "''") & "', " & EventId & ", " & HomeTeamScore & ", " & AwayTeamScore & ", '" & DateTime.Now & "', '" & DateTime.Now & "')", DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                End If
            Else
                Completed = 3
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with a penalty")
            End If
        End Try

        Return Completed

    End Function

    Private Function Status(ByVal MatchId As String, ByVal xmlStatus As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim tmpStatus As String = ""
        Dim Comments As String = ""
        Dim Attendance As String = ""
        Dim HasScore As Integer = 1
        Dim HasAggregate As Integer = 0
        Dim HomeTeamScore As Integer = 0
        Dim AwayTeamScore As Integer = 0
        Dim HomeTeamAggregateScore As Integer = 0
        Dim AwayTeamAggregateScore As Integer = 0
        Dim HomeTeamPenalties As Integer = 0
        Dim AwayTeamPenalties As Integer = 0
        Dim Result As Integer = 0

        Try
            Dim tmpNodes As XmlAttributeCollection = xmlStatus.Attributes
            Dim M As Integer
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "status"
                        tmpStatus = tmpNodes(M).InnerText
                    Case "comment"
                        Comments = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            If Not xmlStatus.SelectSingleNode("Attendance") Is Nothing Then
                tmpNodes = xmlStatus.SelectSingleNode("Attendance").Attributes
                For M = 0 To tmpNodes.Count - 1
                    Dim tmpName As String = tmpNodes(M).Name
                    Select Case tmpName
                        Case "attendance"
                            Attendance = tmpNodes(M).InnerText
                        Case Else

                    End Select
                Next
            End If

            If Not xmlStatus.SelectSingleNode("Score") Is Nothing Then
                tmpNodes = xmlStatus.SelectSingleNode("Score").Attributes
                For M = 0 To tmpNodes.Count - 1
                    Dim tmpName As String = tmpNodes(M).Name
                    Select Case tmpName
                        Case "home"
                            HomeTeamScore = tmpNodes(M).InnerText
                        Case "away"
                            AwayTeamScore = tmpNodes(M).InnerText
                        Case "aggregateHome"
                            HasAggregate = 1
                            HomeTeamAggregateScore = tmpNodes(M).InnerText
                        Case "aggregateAway"
                            HasAggregate = 1
                            AwayTeamAggregateScore = tmpNodes(M).InnerText
                        Case Else

                    End Select
                Next
            Else
                HasScore = 0
            End If

            If tmpStatus = "Result" Then
                Result = 1
            End If

            Dim RsRec As SqlDataReader

            If Comments.IndexOf("win") >= 0 And Comments.IndexOf("on penalties") >= 0 Then
                SqlQuery = New SqlCommand("Select HomeTeamName From Soccer.dbo.pa_Matches Where (Id = " & MatchId & ")", DatabaseConn)
                RsRec = SqlQuery.ExecuteReader
                Dim tmpHomeTeam As String = ""
                While RsRec.Read
                    tmpHomeTeam = RsRec("HomeTeamName")
                End While
                RsRec.Close()

                Dim tmpArray() As String = Comments.Split("-")
                Dim tmpArray1() As String = tmpArray(0).Trim.Split(" ")
                Dim tmpArray2() As String = tmpArray(1).Trim.Split(" ")
                If Comments.IndexOf(tmpHomeTeam) >= 0 Then
                    HomeTeamPenalties = tmpArray1(tmpArray1.GetUpperBound(0))
                    AwayTeamPenalties = tmpArray2(0)
                Else
                    AwayTeamPenalties = tmpArray1(tmpArray1.GetUpperBound(0))
                    HomeTeamPenalties = tmpArray2(0)
                End If
            End If

            Dim StatusId As Integer = retStatus(tmpStatus.ToLower)

            If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                Dim tmpStatusId As Integer = 0
                Dim tmpType As String = "Knockout"
                Dim tmpId As Integer = 0
                Dim tmpSeason As Integer = 0
                Dim tmpCompetition As Integer = 0
                SqlQuery = New SqlCommand("Select a.Season, a.StatusId, b.Stage_Type, b.LeagueId, b.Competition_Id From Soccer.dbo.pa_Matches a INNER JOIN Soccer.dbo.pa_Competitions b ON b.Competition_Id = a.Competition Where (a.Id = '" & MatchId & "' And b.Stage_Number = a.Stage And b.Round_Number = a.Round) And (b.Running = 1)", DatabaseConn)
                RsRec = SqlQuery.ExecuteReader
                While RsRec.Read()
                    tmpStatusId = RsRec("StatusId")
                    tmpType = RsRec("Stage_Type")
                    tmpId = RsRec("LeagueId")
                    tmpSeason = RsRec("Season")
                    tmpCompetition = RsRec("Competition_Id")
                End While
                RsRec.Close()

                If (StatusId >= tmpStatusId) Or (tmpStatusId = 16 And StatusId <> 16) Then
                    If StatusId = 4 Then
                        Result = 1
                    End If
                    If StatusId = 2 Then
                        SqlQuery = New SqlCommand("Update Soccer.dbo.pa_Matches Set Modified = '" & DateTime.Now & "', Status = '" & tmpStatus.Replace("'", "''") & "', StatusId = " & StatusId & ", Comment = '" & Comments.Replace("'", "''") & "', Result = " & Result & ", HomeTeamScore = " & HomeTeamScore & ", AwayTeamScore = " & AwayTeamScore & ", HomeTeamAggregate = " & HomeTeamAggregateScore & ", AwayTeamAggregate = " & AwayTeamAggregateScore & ", HomeTeamPenalties = " & HomeTeamPenalties & ", AwayTeamPenalties = " & AwayTeamPenalties & ", HasAggregate = '" & HasAggregate & "', HalfTimeHomeTeamScore = " & HomeTeamScore & ", HalfTimeAwayTeamScore = " & AwayTeamScore & ", HalfTimeHomeTeamAggregate = " & HomeTeamAggregateScore & ", HalfTimeAwayTeamAggregate = " & AwayTeamAggregateScore & " Where (Id = '" & MatchId & "')", DatabaseConn)
                        SqlQuery.ExecuteNonQuery()
                    Else
                        If HasScore = 0 And Attendance <> "" Then
                            SqlQuery = New SqlCommand("Update Soccer.dbo.pa_Matches Set Modified = '" & DateTime.Now & "', Status = '" & tmpStatus.Replace("'", "''") & "', StatusId = " & StatusId & ", Comment = '" & Comments.Replace("'", "''") & "', Attendance = '" & Attendance & "' Where (Id = '" & MatchId & "')", DatabaseConn)
                            SqlQuery.ExecuteNonQuery()
                        ElseIf HasScore = 0 Then
                            SqlQuery = New SqlCommand("Update Soccer.dbo.pa_Matches Set Modified = '" & DateTime.Now & "', Status = '" & tmpStatus.Replace("'", "''") & "', StatusId = " & StatusId & ", Comment = '" & Comments.Replace("'", "''") & "' Where (Id = '" & MatchId & "')", DatabaseConn)
                            SqlQuery.ExecuteNonQuery()
                        ElseIf Attendance <> "" Then
                            SqlQuery = New SqlCommand("Update Soccer.dbo.pa_Matches Set Modified = '" & DateTime.Now & "', Status = '" & tmpStatus.Replace("'", "''") & "', StatusId = " & StatusId & ", Comment = '" & Comments.Replace("'", "''") & "', Attendance = '" & Attendance & "', Result = " & Result & ", HomeTeamScore = " & HomeTeamScore & ", AwayTeamScore = " & AwayTeamScore & ", HomeTeamAggregate = " & HomeTeamAggregateScore & ", AwayTeamAggregate = " & AwayTeamAggregateScore & ", HomeTeamPenalties = " & HomeTeamPenalties & ", AwayTeamPenalties = " & AwayTeamPenalties & ", HasAggregate = '" & HasAggregate & "' Where (Id = '" & MatchId & "')", DatabaseConn)
                            SqlQuery.ExecuteNonQuery()
                        Else
                            SqlQuery = New SqlCommand("Update Soccer.dbo.pa_Matches Set Modified = '" & DateTime.Now & "', Status = '" & tmpStatus.Replace("'", "''") & "', StatusId = " & StatusId & ", Comment = '" & Comments.Replace("'", "''") & "', Result = " & Result & ", HomeTeamScore = " & HomeTeamScore & ", AwayTeamScore = " & AwayTeamScore & ", HomeTeamAggregate = " & HomeTeamAggregateScore & ", AwayTeamAggregate = " & AwayTeamAggregateScore & ", HomeTeamPenalties = " & HomeTeamPenalties & ", AwayTeamPenalties = " & AwayTeamPenalties & ", HasAggregate = '" & HasAggregate & "' Where (Id = '" & MatchId & "')", DatabaseConn)
                            SqlQuery.ExecuteNonQuery()
                        End If
                    End If
                    If Result = 1 And tmpType = "League" Then
                        Dim logsIds As String = System.Configuration.ConfigurationSettings.AppSettings("paLogs").ToString()
                        If logsIds.IndexOf(tmpCompetition.ToString()) < 0 Then
                            SqlQuery = New SqlCommand("Execute Soccer.dbo.pa_createlog " & tmpCompetition & ", " & tmpSeason & "", DatabaseConn)
                            SqlQuery.ExecuteNonQuery()
                            'FixLog(tmpCompetition, tmpSeason)
                        End If
                    End If
                    If tmpCompetition = 100 And Result = 1 Then
                        'SmsResult(tmpCompetition, MatchId)
                        'SmsLog(tmpCompetition)
                    End If
                    If tmpCompetition = 794 And Result = 1 And StatusId <> 4 Then
                        'SmsResult(tmpCompetition, MatchId)
                        'SmsLog(tmpCompetition)
                    End If
                End If

                Try
                    If mtnTournaments.Contains(tmpCompetition) Then
                        If StatusId = 2 Then
                            'mtnMatchSms(tmpCompetition, MatchId, "half")
                        ElseIf StatusId = 4 Or StatusId = 13 Then
                            'mtnMatchSms(tmpCompetition, MatchId, "full")
                            'mtnDailySummary(tmpCompetition, MatchId)
                        End If
                    End If
                Catch ex As Exception

                End Try
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with the status")
            End If
        End Try

        Return Completed

    End Function

    Private Function Attendance(ByVal MatchId As String, ByVal xmlAttendance As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim QueryText As String = ""

        Try
            Dim tmpAttendance As String = ""
            Dim tmpNodes As XmlAttributeCollection = xmlAttendance.Attributes
            Dim M As Integer
            For M = 0 To tmpNodes.Count - 1
                Dim tmpName As String = tmpNodes(M).Name
                Select Case tmpName
                    Case "attendance"
                        tmpAttendance = tmpNodes(M).InnerText
                    Case Else

                End Select
            Next

            If tmpAttendance <> "" Then
                QueryText = "Update Soccer.dbo.pa_Matches Set Attendance = '" & tmpAttendance & "' Where (Id = '" & MatchId & "')"
            End If

            If QueryText <> "" Then
                If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                    SqlQuery = New SqlCommand(QueryText, DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                Else
                    Completed = 3
                End If
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler(MatchId, ex.Message, "Error with the attendance")
            End If
        End Try

        Return Completed

    End Function

    Private Function Managers(ByVal MatchId As String, ByVal xmlAttendance As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1

        Return Completed

    End Function

    Private Function Minute(ByVal Time As String) As Integer

        Dim retMinute As Integer = 0

        If Time.IndexOf(":") >= 0 Then
            Dim tmpArray() As String = Time.Split(":")
            If tmpArray(0).StartsWith("0") And tmpArray(0).Length > 1 Then
                retMinute = Convert.ToInt32(tmpArray(0).Substring(1, 1))
            Else
                retMinute = Convert.ToInt32(tmpArray(0))
            End If
        Else
            If Time.StartsWith("0") And Time.Length > 1 Then
                retMinute = Convert.ToInt32(Time.Substring(1, 1))
            Else
                retMinute = Convert.ToInt32(Time)
            End If
        End If

        Return retMinute

    End Function

    Private Function Second(ByVal Time As String) As Integer

        Dim retSecond As Integer = 0

        If Time.IndexOf(":") >= 0 Then
            Dim tmpArray() As String = Time.Split(":")
            If tmpArray(1).StartsWith("0") And tmpArray(1).Length > 1 Then
                retSecond = Convert.ToInt32(tmpArray(1).Substring(1, 1))
            Else
                retSecond = Convert.ToInt32(tmpArray(1))
            End If
        End If

        Return retSecond

    End Function

    Private Function retStatus(ByVal Status As String) As Integer

        Dim StatusId As Integer = 0

        Select Case Status
            Case "kickoff"
                StatusId = 1
            Case "halftime"
                StatusId = 2
            Case "secondhalfstarted"
                StatusId = 3
            Case "fulltime"
                StatusId = 4
            Case "fulltimeextratime"
                StatusId = 5
            Case "extratimestarted"
                StatusId = 6
            Case "extratimehalftime"
                StatusId = 7
            Case "extratimesecondhalfstarted"
                StatusId = 8
            Case "extratimefulltime"
                StatusId = 9
            Case "extratimefulltimepenaltiesbeingplayed"
                StatusId = 10
            Case "penalties"
                StatusId = 11
            Case "penaltiescomplete"
                StatusId = 12
            Case "result"
                StatusId = 13
            Case "postponed"
                StatusId = 14
            Case "suspended"
                StatusId = 15
            Case "resumed"
                StatusId = 16
            Case "cancelled"
                StatusId = 17
            Case "abandoned"
                StatusId = 18
            Case "deletion"
                StatusId = 19
            Case Else
                StatusId = 0
        End Select

        Return StatusId

    End Function

    Private Sub Scorers(ByVal Matches As ArrayList)

        Dim QueryText As String = ""
        Dim MatchString As String = ""
        Dim Competitions As ArrayList = New ArrayList

        Dim IEnum As IEnumerator = Matches.GetEnumerator
        While IEnum.MoveNext
            If MatchString <> "" Then
                MatchString &= ","
            End If
            MatchString &= IEnum.Current
        End While

        Try
            SqlQuery = New SqlCommand("Select Distinct(Competition) From Soccer.dbo.pa_Matches Where (Id In (" & MatchString & "))", DatabaseConn)
            Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
            While RsRec.Read
                Competitions.Add(RsRec("Competition"))
            End While
            RsRec.Close()

            If Competitions.Count > 0 Then

                updateListbox("Processing scorers", True)

                IEnum = Competitions.GetEnumerator
                While IEnum.MoveNext
                    Dim Competition As Integer = IEnum.Current
                    Dim Season As Integer = 1
                    Dim ds As DataSet = New DataSet
                    Dim da As SqlDataAdapter = New SqlDataAdapter

                    SqlQuery = New SqlCommand("Select Top 1 Competition_Season From Soccer.dbo.pa_Competitions Where (Competition_Id = " & Competition & ") And (Running = 1) Order By Competition_Season Desc", DatabaseConn)
                    Season = SqlQuery.ExecuteScalar

                    QueryText = "Delete From Soccer.dbo.pa_Scorers Where (Competition = " & Competition & ") And (Season = " & Season & ");"

                    SqlQuery = New SqlCommand("Select Count(a.Id) As Goals, a.Player1Id From soccer.dbo.pa_Events a INNER JOIN soccer.dbo.pa_Matches b ON b.Id = a.MatchId INNER JOIN soccer.dbo.pa_Competitions c ON c.Competition_Id = b.Competition where (b.Competition = " & Competition & ") AND (a.EventId = 2 Or a.EventId = 4 Or a.EventId = 5) AND (c.Running = 1) AND (b.Season = c.Competition_Season) AND (c.Round_Number = b.Round And c.Stage_Number = b.Stage) Group By c.Competition_Name, a.Player1Id Order By Goals Desc", DatabaseConn)
                    da.SelectCommand = SqlQuery
                    da.Fill(ds, "table1")

                    SqlQuery = New SqlCommand("Select Count(a.Id) As Goals, a.Player1Id, a.TeamId, a.TeamName From soccer.dbo.pa_Events a INNER JOIN soccer.dbo.pa_Matches b ON b.Id = a.MatchId INNER JOIN soccer.dbo.pa_Competitions c ON c.Competition_Id = b.Competition where (b.Competition = " & Competition & ") AND (a.EventId = 2 Or a.EventId = 4 Or a.EventId = 5) AND (c.Running = 1) AND (b.Season = c.Competition_Season) AND (c.Round_Number = b.Round And c.Stage_Number = b.Stage) Group By c.Competition_Name, a.Player1Id, a.TeamId, a.TeamName Order By Goals Desc", DatabaseConn)
                    da.SelectCommand = SqlQuery
                    da.Fill(ds, "table2")

                    SqlQuery = New SqlCommand("Select a.Player1Id, a.Player1FirstName, a.Player1LastName, a.Player1Initials, a.Player1FullName, a.TeamId, a.TeamName, a.Created From Soccer.dbo.pa_Events a INNER JOIN Soccer.dbo.pa_Matches b ON b.Id = a.MatchId INNER JOIN Soccer.dbo.pa_Competitions c ON c.Competition_Id = b.Competition Where (c.Stage_Number = b.Stage And c.Round_Number = b.Round And c.Competition_Season = b.Season And c.Running = 1 And b.Competition = " & Competition & ") And (a.EventId = 2 Or a.EventId = 4 Or a.EventId = 5) Order By a.Created Desc", DatabaseConn)
                    da.SelectCommand = SqlQuery
                    da.Fill(ds, "table3")

                    Dim tmpRows() As DataRow = ds.Tables("table1").Select("", "Goals Desc")
                    Dim I As Integer
                    Dim Position As Integer = 0
                    Dim tmpGoals As Integer = -1
                    For I = 0 To tmpRows.GetUpperBound(0)
                        Dim Tied As Integer = 0
                        Dim TeamId As Integer = 0
                        Dim TeamName As String = ""
                        Dim PlayerId As Integer = 0
                        Dim PlayerFirstname As String = ""
                        Dim PlayerLastname As String = ""
                        Dim PlayerInitials As String = ""
                        Dim PlayerFullname As String = ""
                        Dim Goals As Integer = tmpRows(I).Item("Goals")
                        Dim ExtraInfo As String = ""

                        If tmpGoals <> Goals Then
                            Position = Position + 1
                        End If

                        Dim tmpRows1() As DataRow = ds.Tables("table3").Select("Player1Id = " & tmpRows(I).Item("Player1Id"), "Created Desc")
                        Dim N As Integer
                        For N = 0 To tmpRows1.GetUpperBound(0)
                            TeamId = tmpRows1(N).Item("TeamId")
                            TeamName = tmpRows1(N).Item("TeamName")
                            PlayerId = tmpRows1(N).Item("Player1Id")
                            PlayerFirstname = tmpRows1(N).Item("Player1Firstname")
                            PlayerLastname = tmpRows1(N).Item("Player1Lastname")
                            PlayerInitials = tmpRows1(N).Item("Player1Initials")
                            PlayerFullname = tmpRows1(N).Item("Player1Fullname")
                            Exit For
                        Next

                        If TeamName <> "" Then
                            TeamName = FixTeamName(TeamId.ToString(), TeamName)
                        End If

                        tmpRows1 = ds.Tables("table2").Select("Player1Id = " & PlayerId & "", "")
                        If tmpRows1.Length > 0 Then
                            For N = 0 To tmpRows1.GetUpperBound(0)
                                If ExtraInfo <> "" Then
                                    ExtraInfo &= ", "
                                End If
                                ExtraInfo &= tmpRows1(N).Item("TeamName") & " " & tmpRows1(N).Item("Goals")
                            Next
                        End If

                        If ExtraInfo.IndexOf(",") < 0 Then
                            ExtraInfo = ""
                        End If

                        tmpRows1 = ds.Tables("table1").Select("Goals = " & Goals & "", "")
                        If tmpRows1.Length > 1 Then
                            Tied = 1
                        End If

                        QueryText &= Environment.NewLine & "Insert Into Soccer.dbo.pa_Scorers (Position, Tied, Competition, Season, TeamId, TeamName, PlayerId, PlayerFirstname, PlayerLastname, PlayerInitials, PlayerFullname, Goals, ExtraInfo, Sorting, Created) Values (" & Position & ", " & Tied & ", " & Competition & ", " & Season & ", " & TeamId & ", '" & TeamName.Replace("'", "''") & "', " & PlayerId & ", '" & PlayerFirstname.Replace("'", "''") & "', '" & PlayerLastname.Replace("'", "''") & "', '" & PlayerInitials.Replace("'", "''") & "', '" & PlayerFullname.Replace("'", "''") & "', " & Goals & ", '" & ExtraInfo.Replace("'", "''") & "', " & I & ", '" & DateTime.Now & "');"

                        tmpGoals = Goals
                    Next

                    If QueryText <> "" Then
                        SqlQuery = New SqlCommand(QueryText, DatabaseConn)
                        SqlQuery.ExecuteNonQuery()
                    End If

                    ds.Dispose()
                End While

                updateListbox("Scorers complete", True)
            End If
        Catch ex As Exception
            Call ErrorHandler("", ex.Message, "Error with the scorers")
        End Try

    End Sub

    Private Sub fixLog(ByVal Competition As Integer, ByVal Season As Integer)

        Dim duplicates As ArrayList = New ArrayList

        SqlQuery = New SqlCommand("Select * From Soccer.dbo.pa_logs Where Competition = " + Competition.ToString() + " And Season = " + Season.ToString(), DatabaseConn)
        Dim ds As DataSet = New DataSet
        Dim da As SqlDataAdapter = New SqlDataAdapter
        da.SelectCommand = SqlQuery
        da.Fill(ds, "log")

        Dim dataRow As DataRow
        Dim groupName As String
        Dim points As Integer
        For Each dataRow In ds.Tables("log").Select
            If dataRow("GroupName").ToString() = groupName And Convert.ToInt32(dataRow("LogPoints")) = points And Convert.ToInt32(dataRow("LogPoints")) > 0 Then
                duplicates.Add(dataRow("GroupName").ToString() + "," + dataRow("LogPoints").ToString())
                points = 100000
            Else
                points = Convert.ToInt32(dataRow("LogPoints"))
            End If
            groupName = dataRow("GroupName").ToString()
        Next

        If duplicates.Count > 0 Then
            ds.Tables("log").Columns.Add("dupPoints", System.Type.GetType("System.Int32"))
            ds.Tables("log").Columns.Add("dupGoalDifference", System.Type.GetType("System.Int32"))
            ds.Tables("log").Columns.Add("dupGoals", System.Type.GetType("System.Int32"))
            ds.Tables("log").Columns.Add("dupGoalsAway", System.Type.GetType("System.Int32"))

            Dim listedDuplicate As String
            For Each listedDuplicate In duplicates
                groupName = listedDuplicate.Split(",")(0).ToString()
                points = Convert.ToInt32(listedDuplicate.Split(",")(1))
                Dim teams As String

                For Each dataRow In ds.Tables("log").Select("GroupName = '" + groupName + "' And LogPoints = '" + points.ToString() + "'")
                    If teams <> "" Then
                        teams += ","
                    End If
                    teams += dataRow("teamId").ToString
                Next

                SqlQuery = New SqlCommand("Select a.* From Soccer.dbo.pa_Matches a INNER JOIN Soccer.dbo.pa_Competitions b ON b.Competition_Id = a.Competition Where (a.Competition = " + Competition.ToString() + ") And (a.Season = " + Season.ToString() + ") And (a.HomeTeamId In (" + teams.ToString() + ")) And (a.AwayTeamId In (" + teams.ToString() + ")) And (b.Competition_Season = a.Season And b.Round_Number = a.Round And b.Stage_Number = a.Stage) And (a.result = 1) And (b.Stage_Type = 'League')", DatabaseConn)
                da = New SqlDataAdapter
                da.SelectCommand = SqlQuery
                da.Fill(ds, "matches")

                For Each dataRow In ds.Tables("log").Select("GroupName = '" + groupName + "' And LogPoints = '" + points.ToString() + "'")
                    Dim teamId As Integer = Convert.ToInt32(dataRow("teamId"))
                    Dim teamPoints As Integer = 0
                    Dim teamGoalDifference As Integer = 0
                    Dim teamGoal As Integer = 0
                    Dim teamGoalAway As Integer = 0

                    Dim matchRow As DataRow
                    For Each matchRow In ds.Tables("matches").Select("(HomeTeamId = " + teamId.ToString() + " Or AwayTeamId = " + teamId.ToString() + ")")
                        Dim HomeTeamScore As Integer = Convert.ToInt32(matchRow("homeTeamScore"))
                        Dim AwayTeamScore As Integer = Convert.ToInt32(matchRow("awayTeamScore"))
                        If Convert.ToInt32(matchRow("AwayTeamId") = teamId) Then
                            If AwayTeamScore > HomeTeamScore Then
                                teamPoints += 3
                            ElseIf AwayTeamScore = HomeTeamScore Then
                                teamPoints += 1
                            End If
                            teamGoal += AwayTeamScore
                            teamGoalAway += AwayTeamScore
                            teamGoalDifference += AwayTeamScore - HomeTeamScore
                        Else
                            If HomeTeamScore > AwayTeamScore Then
                                teamPoints += 3
                            ElseIf HomeTeamScore = AwayTeamScore Then
                                teamPoints += 1
                            End If
                            teamGoal += HomeTeamScore
                            teamGoalAway += 0
                            teamGoalDifference += HomeTeamScore - AwayTeamScore
                        End If
                    Next

                    ds.Tables("log").Select("teamId = " + teamId.ToString())(0)("dupPoints") = teamPoints
                    ds.Tables("log").Select("teamId = " + teamId.ToString())(0)("dupGoalDifference") = teamGoalDifference
                    ds.Tables("log").Select("teamId = " + teamId.ToString())(0)("dupGoals") = teamGoal
                    ds.Tables("log").Select("teamId = " + teamId.ToString())(0)("dupGoalsAway") = teamGoalAway
                Next

                Dim sorting As Integer = 1

                Dim queryString As String = ""
                For Each dataRow In ds.Tables("log").Select("GroupName = '" + groupName + "' And LogPoints = '" + points.ToString() + "'", "dupPoints, dupGoalDifference, dupGoals, dupGoalsAway, goalDifference")
                    queryString += "Update Soccer.dbo.pa_Logs Set Ordering = " + sorting.ToString() + " Where TeamId = " + dataRow("teamId").ToString() + " And Competition = " + Competition.ToString() + " And GroupName = '" + groupName.ToString() + "' And Season = " + Season.ToString() + ";"
                    sorting += 1
                Next

                If queryString <> "" Then
                    SqlQuery = New SqlCommand(queryString, DatabaseConn)
                    SqlQuery.ExecuteNonQuery()
                End If
            Next
        End If

    End Sub

#End Region

#Region " Logs "

    Private Sub Logs()

        Dim File As String
        Dim Files() As String
        Dim Fi As FileInfo
        Dim DbCount As Integer = 0
        Dim DatabaseError As Boolean = False
        Dim CompetitionId As Integer = 0
        Dim QueryText As String = ""

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Logs\Temporary") Then
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Logs\Temporary")

            Dim Count As Integer = 0
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf(".xml") >= 0 Then
                    Count = Count + 1
                End If
            Next

            If Count > 0 Then
                updateListbox("Processing logs", True)

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And DbConnected = True Then
                        Dim MoveFile As Boolean = True
                        Try
                            Dim xmlDoc As New XmlDocument
                            xmlDoc.Load(File)

                            Dim xmlTeams As XmlNodeList = xmlDoc.SelectNodes("SoccerTables/Teams/Team")
                            Dim I As Integer
                            Dim Teams As Hashtable = New Hashtable
                            Teams.Add("", "")
                            For I = 0 To xmlTeams.Count - 1
                                Dim tmpId As String = xmlTeams(I).Attributes.ItemOf("id").InnerText
                                Dim tmpName As String = xmlTeams(I).Attributes.ItemOf("name").InnerText
                                Teams.Add(tmpId, FixTeamName(tmpId, tmpName))
                            Next
                            xmlTeams = Nothing

                            Dim xmlTables As XmlNodeList = xmlDoc.SelectNodes("SoccerTables/Table")
                            For I = 0 To xmlTables.Count - 1
                                Dim Competition As Integer = 0
                                Dim StageNumber As Integer = 0
                                Dim RoundNumber As Integer = 0
                                Dim RoundName As String = ""
                                Dim Season As Integer = 0
                                QueryText = ""

                                Dim xmlDetails As XmlAttributeCollection = xmlTables(I).Attributes
                                Dim N As Integer
                                For N = 0 To xmlDetails.Count - 1
                                    Dim tmpName As String = xmlDetails(N).Name
                                    Select Case tmpName
                                        Case "competitionId"
                                            Competition = xmlDetails(N).InnerText
                                        Case "stageNumber"
                                            StageNumber = xmlDetails(N).InnerText
                                        Case "roundNumber"
                                            RoundNumber = xmlDetails(N).InnerText
                                        Case Else

                                    End Select
                                Next

                                SqlQuery = New SqlCommand("Select Top 1 Competition_Season, Round_Name From Soccer.dbo.pa_Competitions Where (Competition_Id = " & Competition.ToString() & ") And (Round_Number = " & RoundNumber.ToString() & ") And (Running = 1) AND (Stage_Type = 'League') Order By Competition_Season Desc", DatabaseConn)
                                Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
                                While RsRec.Read
                                    Season = Convert.ToInt32(RsRec("Competition_Season"))
                                    RoundName = RsRec("Round_Name").ToString()
                                End While
                                RsRec.Close()

                                Dim xmlTeamPositions As XmlNodeList = xmlTables(I).SelectNodes("TeamPosition")
                                For N = 0 To xmlTeamPositions.Count - 1
                                    Dim Rank As Integer = 0
                                    Dim TeamId As Integer = 0
                                    Dim TeamName As String = ""
                                    Dim Played As Integer = 0
                                    Dim Won As Integer = 0
                                    Dim Drawn As Integer = 0
                                    Dim Lost As Integer = 0
                                    Dim GoalsFor As Integer = 0
                                    Dim GoalsAgainst As Integer = 0
                                    Dim GoalDifference As Integer = 0
                                    Dim Points As Integer = 0

                                    Dim xmlStats As XmlAttributeCollection = xmlTeamPositions(N).Attributes
                                    Dim M As Integer
                                    For M = 0 To xmlStats.Count - 1
                                        Dim tmpName As String = xmlStats(M).Name
                                        Select Case tmpName
                                            Case "rank"
                                                Rank = xmlStats(M).InnerText
                                            Case "teamId"
                                                TeamId = xmlStats(M).InnerText
                                                If Teams.ContainsKey(TeamId.ToString) Then
                                                    TeamName = Teams(TeamId.ToString)
                                                Else
                                                    TeamName = TeamId
                                                End If
                                            Case "played"
                                                Played = xmlStats(M).InnerText
                                            Case "won"
                                                Won = xmlStats(M).InnerText
                                            Case "drawn"
                                                Drawn = xmlStats(M).InnerText
                                            Case "lost"
                                                Lost = xmlStats(M).InnerText
                                            Case "for"
                                                GoalsFor = xmlStats(M).InnerText
                                            Case "against"
                                                GoalsAgainst = xmlStats(M).InnerText
                                            Case "goalDifference"
                                                GoalDifference = xmlStats(M).InnerText
                                            Case "points"
                                                Points = xmlStats(M).InnerText
                                            Case Else

                                        End Select
                                    Next

                                    If QueryText <> "" Then
                                        QueryText &= Environment.NewLine
                                    End If
                                    QueryText &= "Insert Into Soccer.dbo.pa_Logs (GroupId, GroupName, Competition, Season, Ordering, TeamId, Played, Won, Drew, Lost, GoalsFor, GoalsAgainst, GoalDifference, logPoints, Created) Values (" + RoundNumber.ToString() + ", '" + RoundName.ToString() + "', " & Competition.ToString() & ", " & Season.ToString() & ", " & Rank.ToString() & ", " & TeamId.ToString() & ", " & Played.ToString() & ", " & Won.ToString() & ", " & Drawn.ToString() & ", " & Lost.ToString() & ", " & GoalsFor.ToString() & ", " & GoalsAgainst.ToString() & ", " & GoalDifference.ToString() & ", " & Points.ToString() & ", '" & DateTime.Now.ToString() & "');"
                                Next

                                If QueryText <> "" Then
                                    If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                                        SqlQuery = New SqlCommand("Delete From Soccer.dbo.pa_Logs Where (Competition = '" & Competition & "') And (GroupId = " & RoundNumber & ")", DatabaseConn)
                                        SqlQuery.ExecuteNonQuery()
                                        SqlQuery = New SqlCommand(QueryText, DatabaseConn)
                                        SqlQuery.ExecuteNonQuery()
                                        'Dim Fs As New FileStream("C:\Program Files\SuperSport Zone\SuperSport Soccer\log.txt", FileMode.OpenOrCreate, FileAccess.Write)
                                        'Dim Sw As New StreamWriter(Fs)
                                        'Sw.BaseStream.Seek(0, SeekOrigin.End)
                                        'Sw.WriteLine(QueryText)
                                        'Sw.Close()
                                    End If
                                End If
                            Next

                            xmlDoc = Nothing
                        Catch ex As Exception
                            MoveFile = False
                            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                                DatabaseError = True
                            Else
                                Call ErrorHandler(CompetitionId, ex.Source & "-" & ex.Message, "Error with the logs")
                            End If
                        End Try
                        If MoveFile = True Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Logs\Complete\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf DatabaseError = False Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Logs\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        End If
                    End If
                Next

                'Set the season for both the Absa PSL and the NFD to be valid for the 3rd party sites to pull data.
                SqlQuery = New SqlCommand("Update Soccer.dbo.pa_logs Set [season] = 11 Where Competition in (794, 879) and season = 0" , DatabaseConn)
                SqlQuery.ExecuteNonQuery()

                updateListbox("Logs complete", True)
            End If
        End If

    End Sub

#End Region

#Region " Live "

    Private Sub Live()

        Dim File As String
        Dim Files() As String
        Dim Fi As FileInfo
        Dim MatchId As String = ""

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Temporary") Then
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Temporary")

            Dim Count As Integer = 0
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf(".xml") >= 0 Then
                    Count = Count + 1
                End If
            Next
            If Count > 0 Then
                updateListbox("Processing live events", True)

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And DbConnected = True Then
                        Dim MoveFile As Integer = 1
                        Try
                            Dim xmlDoc As New XmlDocument
                            xmlDoc.Load(File)

                            Dim xmlNode As XmlNode = xmlDoc.FirstChild
                            Dim xmlNodes As XmlNodeList = xmlDoc.ChildNodes
                            Dim I As Integer
                            For I = 0 To xmlNodes.Count - 1
                                If xmlNodes(I).Name = "MatchLive" Then
                                    xmlNode = xmlNodes(I)
                                End If
                            Next
                            Dim xmlEvents As XmlNodeList = xmlNode.ChildNodes
                            MatchId = xmlNode.Attributes.ItemOf("id").InnerText

                            For I = 0 To xmlEvents.Count - 1
                                Dim Type As String = xmlEvents(I).Name
                                If DbConnected = True Then
                                    If Type = "Event" Then
                                        MoveFile = LiveEvents(MatchId, xmlEvents(I))
                                    ElseIf Type = "TextFeed" Then
                                        MoveFile = TextFeed(MatchId, xmlEvents(I))
                                    ElseIf Type = "MatchStats" Then
                                        MoveFile = MatchStats(MatchId, xmlEvents(I))
                                    ElseIf Type = "Team" Then

                                    ElseIf Type = "Staff" Then

                                    ElseIf Type = "Officials" Then

                                    ElseIf Type = "Ratings" Then

                                    Else
                                        MoveFile = -1
                                    End If
                                Else
                                    MoveFile = 3
                                End If
                                If MoveFile > 1 Then
                                    Exit For
                                End If
                            Next
                            xmlEvents = Nothing
                            xmlDoc = Nothing
                        Catch ex As Exception
                            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                                MoveFile = 3
                            Else
                                MoveFile = 2
                                Call ErrorHandler(MatchId, ex.Message, "Error with the matches")
                            End If
                        End Try
                        If MoveFile = 1 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Complete\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf MoveFile = 2 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf MoveFile = 0 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf MoveFile = -1 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        End If
                    End If
                Next

                updateListbox("Live events complete", True)
            End If
        End If

    End Sub

    Private Function LiveEvents(ByVal MatchId As String, ByVal xmlEvents As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim EventCorrection As Boolean = False
        Dim EventId As String = ""
        Dim EventType As Integer = 0
        Dim QueryText As String = ""

        Dim xmlAttributes As XmlAttributeCollection = xmlEvents.Attributes
        Dim M As Integer
        For M = 0 To xmlAttributes.Count - 1
            Dim tmpName As String = xmlAttributes(M).Name
            Select Case tmpName
                Case "id"
                    EventId = xmlAttributes(M).InnerText
                Case "Correction"
                    If xmlAttributes(M).InnerText = "Yes" Then
                        EventCorrection = True
                    End If
                Case Else

            End Select
        Next

        Dim xmlTypes As XmlNodeList = xmlEvents.ChildNodes
        Dim I As Integer
        For I = 0 To xmlTypes.Count - 1
            Completed = 1
            Dim Id As String = ""
            Dim TeamId As Integer = 1
            Dim TeamName As String = ""
            Dim Player1Id As Integer = 1
            Dim Player1Firstname As String = ""
            Dim Player1Surname As String = ""
            Dim Player2Id As Integer = 1
            Dim Player2Firstname As String = ""
            Dim Player2Surname As String = ""
            Dim NormalMinutes As Integer = 0
            Dim NormalSeconds As Integer = 0
            Dim NormalTime As String = ""
            Dim AddedMinutes As Integer = 0
            Dim AddedSeconds As Integer = 0
            Dim AddedTime As String = ""
            Dim Type As String = ""
            Dim Reason As String = ""
            Dim OnTarget As Boolean = False
            Dim Outcome As String = ""
            Dim OwnGoal As Boolean = False
            Dim GoldenGoal As Boolean = False
            Dim How As String = ""
            Dim WhereFrom As String = ""
            Dim WhereTo As String = ""
            Dim Distance As String = ""
            Dim Rating As Integer = -1
            Dim Comment As String = ""
            Dim HomeTeamScore As Integer = 0
            Dim AwayTeamScore As Integer = 0
            Dim HomeTeamPenalties As Integer = 0
            Dim AwayTeamPenalties As Integer = 0
            Dim KeeperCorrect As Boolean = False
            Dim Rescinded As Boolean = False
            Dim Name As String = xmlTypes(I).Name
            Dim Status As String = ""
            Dim Correction As Boolean = False

            Select Case Name
                Case "Goal"
                    EventType = 1
                Case "Penalty"
                    EventType = 2
                Case "PenShootOutGoal"
                    EventType = 3
                Case "Cross"
                    EventType = 4
                Case "Offside"
                    EventType = 5
                Case "Throw"
                    EventType = 6
                Case "GoalKick"
                    EventType = 7
                Case "Booking"
                    EventType = 8
                Case "Dismissal"
                    EventType = 9
                Case "DisallowedGoal"
                    EventType = 10
                Case "Shot"
                    EventType = 11
                Case "Corner"
                    EventType = 12
                Case "FreeKick"
                    EventType = 13
                Case "Save"
                    EventType = 14
                Case "Clearance"
                    EventType = 15
                Case "TenYard"
                    EventType = 16
                Case "Block"
                    EventType = 17
                Case "Substitution"
                    EventType = 18
                Case "Foul"
                    EventType = 19
                Case "Assist"
                    EventType = 20
                Case "Pass"
                    EventType = 21
                Case "BackPass"
                    EventType = 22
                Case "HandBall"
                    EventType = 23
                Case "ShootoutPenalty"
                    EventType = 25
                Case "ShootoutGoal"
                    EventType = 26
                Case "ShootoutSave"
                    EventType = 27
                Case "MatchStatus"
                    EventType = 0
                Case "DeletedAction"
                    EventType = 50
                Case Else
                    Completed = -1
            End Select

            If Completed = 1 Then
                xmlAttributes = xmlTypes(I).Attributes
                For M = 0 To xmlAttributes.Count - 1
                    Dim tmpName As String = xmlAttributes(M).Name
                    Select Case tmpName
                        Case "id"
                            Id = xmlAttributes(M).InnerText
                        Case "correction"
                            If xmlAttributes(M).InnerText = "Yes" Then
                                Correction = True
                            End If
                        Case "normalTime"
                            NormalTime = xmlAttributes(M).InnerText
                        Case "addedTime"
                            AddedTime = xmlAttributes(M).InnerText
                        Case "ownGoal"
                            If xmlAttributes(M).InnerText = "Yes" Then
                                OwnGoal = True
                            End If
                        Case "goldenGoal"
                            If xmlAttributes(M).InnerText = "Yes" Then
                                GoldenGoal = True
                            End If
                        Case "how"
                            How = xmlAttributes(M).InnerText
                        Case "whereFrom"
                            WhereFrom = xmlAttributes(M).InnerText
                        Case "whereTo"
                            WhereTo = xmlAttributes(M).InnerText
                        Case "reason"
                            Reason = xmlAttributes(M).InnerText
                        Case "distance"
                            Distance = xmlAttributes(M).InnerText
                        Case "rating"
                            Rating = xmlAttributes(M).InnerText
                        Case "type"
                            Type = xmlAttributes(M).InnerText
                        Case "outcome"
                            Outcome = xmlAttributes(M).InnerText
                        Case "penaltyOutcome"
                            Outcome = xmlAttributes(M).InnerText
                        Case "onTarget"
                            If xmlAttributes(M).InnerText = "Yes" Then
                                OnTarget = True
                            End If
                        Case "keeperCorrect"
                            If xmlAttributes(M).InnerText = "Yes" Then
                                KeeperCorrect = True
                            End If
                        Case "comment"
                            Comment = xmlAttributes(M).InnerText
                        Case "rescinded"
                            If xmlAttributes(M).InnerText = "Yes" Then
                                Rescinded = True
                            End If
                        Case "status"
                            Status = xmlAttributes(M).InnerText
                        Case Else
                            'Completed = -1
                    End Select
                Next
            End If

            If Completed = 1 And EventType > 0 Then
                Dim xmlElements As XmlNodeList = xmlTypes(I).ChildNodes
                For M = 0 To xmlElements.Count - 1
                    Dim tmpName As String = xmlElements(M).Name
                    Select Case tmpName
                        Case "Player"
                            Dim tmpAttributes As XmlAttributeCollection = xmlElements(M).Attributes
                            Dim N As Integer
                            For N = 0 To tmpAttributes.Count - 1
                                Dim tmpName1 As String = tmpAttributes(N).Name
                                Select Case tmpName1
                                    Case "id"
                                        Player1Id = tmpAttributes(N).InnerText
                                    Case Else

                                End Select
                            Next

                            Dim tmpElements As XmlNodeList = xmlElements(M).ChildNodes
                            For N = 0 To tmpElements.Count - 1
                                Dim tmpName1 As String = tmpElements(N).Name
                                Select Case tmpName1
                                    Case "Name"
                                        tmpAttributes = tmpElements(N).Attributes
                                        Dim O As Integer
                                        For O = 0 To tmpAttributes.Count - 1
                                            Dim tmpName2 As String = tmpAttributes(O).Name
                                            Select Case tmpName2
                                                Case "firstName"
                                                    Player1Firstname = tmpAttributes(O).InnerText
                                                Case "lastName"
                                                    Player1Surname = tmpAttributes(O).InnerText
                                                Case Else

                                            End Select
                                        Next
                                    Case Else

                                End Select
                            Next
                        Case "Team"
                            Dim tmpAttributes As XmlAttributeCollection = xmlElements(M).Attributes
                            Dim N As Integer
                            For N = 0 To tmpAttributes.Count - 1
                                Dim tmpName1 As String = tmpAttributes(N).Name
                                Select Case tmpName1
                                    Case "id"
                                        TeamId = tmpAttributes(N).InnerText
                                    Case "name"
                                        TeamName = tmpAttributes(N).InnerText
                                    Case Else

                                End Select
                            Next
                            If TeamName = "" Then
                                TeamName = xmlElements(M).InnerText
                            End If
                            If TeamName <> "" Then
                                TeamName = FixTeamName(TeamId.ToString(), TeamName)
                            End If
                        Case "PlayerOn"
                            Dim tmpElements As XmlNodeList = xmlElements(M).ChildNodes
                            Dim N As Integer
                            For N = 0 To tmpElements.Count - 1
                                Dim tmpName1 As String = tmpElements(N).Name
                                Select Case tmpName1
                                    Case "Player"
                                        Dim tmpAttributes As XmlAttributeCollection = tmpElements(N).Attributes
                                        Dim O As Integer
                                        For O = 0 To tmpAttributes.Count - 1
                                            Dim tmpName2 As String = tmpAttributes(O).Name
                                            Select Case tmpName2
                                                Case "id"
                                                    Player1Id = tmpAttributes(O).InnerText
                                                Case Else

                                            End Select
                                        Next

                                        Dim tmpElements1 As XmlNodeList = tmpElements(N).ChildNodes
                                        For O = 0 To tmpElements1.Count - 1
                                            Dim tmpName2 As String = tmpElements1(O).Name
                                            Select Case tmpName2
                                                Case "Name"
                                                    tmpAttributes = tmpElements1(O).Attributes
                                                    Dim P As Integer
                                                    For P = 0 To tmpAttributes.Count - 1
                                                        Dim tmpName3 As String = tmpAttributes(P).Name
                                                        Select Case tmpName3
                                                            Case "firstName"
                                                                Player1Firstname = tmpAttributes(P).InnerText
                                                            Case "lastName"
                                                                Player1Surname = tmpAttributes(P).InnerText
                                                            Case Else

                                                        End Select
                                                    Next
                                            End Select
                                        Next
                                    Case Else

                                End Select
                            Next
                        Case "PlayerOff"
                            Dim tmpElements As XmlNodeList = xmlElements(M).ChildNodes
                            Dim N As Integer
                            For N = 0 To tmpElements.Count - 1
                                Dim tmpName1 As String = tmpElements(N).Name
                                Select Case tmpName1
                                    Case "Player"
                                        Dim tmpAttributes As XmlAttributeCollection = tmpElements(N).Attributes
                                        Dim O As Integer
                                        For O = 0 To tmpAttributes.Count - 1
                                            Dim tmpName2 As String = tmpAttributes(O).Name
                                            Select Case tmpName2
                                                Case "id"
                                                    Player2Id = tmpAttributes(O).InnerText
                                                Case Else

                                            End Select
                                        Next

                                        Dim tmpElements1 As XmlNodeList = tmpElements(N).ChildNodes
                                        For O = 0 To tmpElements1.Count - 1
                                            Dim tmpName2 As String = tmpElements1(O).Name
                                            Select Case tmpName2
                                                Case "Name"
                                                    tmpAttributes = tmpElements1(O).Attributes
                                                    Dim P As Integer
                                                    For P = 0 To tmpAttributes.Count - 1
                                                        Dim tmpName3 As String = tmpAttributes(P).Name
                                                        Select Case tmpName3
                                                            Case "firstName"
                                                                Player2Firstname = tmpAttributes(P).InnerText
                                                            Case "lastName"
                                                                Player2Surname = tmpAttributes(P).InnerText
                                                            Case Else

                                                        End Select
                                                    Next
                                                Case Else

                                            End Select
                                        Next
                                    Case Else

                                End Select
                            Next
                        Case "FoulBy"
                            Dim tmpElements As XmlNodeList = xmlElements(M).ChildNodes
                            Dim N As Integer
                            For N = 0 To tmpElements.Count - 1
                                Dim tmpName1 As String = tmpElements(N).Name
                                Select Case tmpName1
                                    Case "Player"
                                        Dim tmpAttributes As XmlAttributeCollection = tmpElements(N).Attributes
                                        Dim O As Integer
                                        For O = 0 To tmpAttributes.Count - 1
                                            Dim tmpName2 As String = tmpAttributes(O).Name
                                            Select Case tmpName2
                                                Case "id"
                                                    Player1Id = tmpAttributes(O).InnerText
                                                Case Else

                                            End Select
                                        Next

                                        Dim tmpElements1 As XmlNodeList = tmpElements(N).ChildNodes
                                        For O = 0 To tmpElements1.Count - 1
                                            Dim tmpName2 As String = tmpElements1(O).Name
                                            Select Case tmpName2
                                                Case "Name"
                                                    tmpAttributes = tmpElements1(O).Attributes
                                                    Dim P As Integer
                                                    For P = 0 To tmpAttributes.Count - 1
                                                        Dim tmpName3 As String = tmpAttributes(P).Name
                                                        Select Case tmpName3
                                                            Case "firstName"
                                                                Player1Firstname = tmpAttributes(P).InnerText
                                                            Case "lastName"
                                                                Player1Surname = tmpAttributes(P).InnerText
                                                            Case Else

                                                        End Select
                                                    Next
                                            End Select
                                        Next
                                    Case "Team"
                                        Dim tmpAttributes As XmlAttributeCollection = tmpElements(N).Attributes
                                        Dim O As Integer
                                        For O = 0 To tmpAttributes.Count - 1
                                            Dim tmpName2 As String = tmpAttributes(O).Name
                                            Select Case tmpName2
                                                Case "id"
                                                    TeamId = tmpAttributes(O).InnerText
                                                Case "name"
                                                    TeamName = tmpAttributes(O).InnerText
                                                Case Else

                                            End Select
                                        Next
                                        If TeamName = "" Then
                                            TeamName = xmlElements(M).InnerText
                                        End If
                                        If TeamName <> "" Then
                                            TeamName = FixTeamName(TeamId.ToString(), TeamName)
                                        End If
                                    Case Else

                                End Select
                            Next
                        Case "FoulOn"

                        Case "Score"
                            Dim tmpAttributes As XmlAttributeCollection = xmlElements(M).Attributes
                            Dim N As Integer
                            For N = 0 To tmpAttributes.Count - 1
                                Dim tmpName1 As String = tmpAttributes(N).Name
                                Select Case tmpName1
                                    Case "home"
                                        HomeTeamScore = tmpAttributes(N).InnerText
                                    Case "away"
                                        AwayTeamScore = tmpAttributes(N).InnerText
                                    Case Else

                                End Select
                            Next
                        Case "PenaltyScore"
                            Dim tmpAttributes As XmlAttributeCollection = xmlElements(M).Attributes
                            Dim N As Integer
                            For N = 0 To tmpAttributes.Count - 1
                                Dim tmpName1 As String = tmpAttributes(N).Name
                                Select Case tmpName1
                                    Case "home"
                                        HomeTeamPenalties = tmpAttributes(N).InnerText
                                    Case "away"
                                        AwayTeamPenalties = tmpAttributes(N).InnerText
                                    Case Else

                                End Select
                            Next
                        Case Else
                            'Completed = -1
                    End Select
                Next
            End If

            If EventType > 0 And EventType < 50 Then
                If NormalTime <> "" Then
                    If NormalTime.IndexOf(":") >= 0 Then
                        NormalMinutes = Minute(NormalTime)
                        NormalSeconds = Second(NormalTime)
                    Else
                        NormalMinutes = Minute(NormalTime)
                    End If
                End If

                If AddedTime <> "" Then
                    If AddedTime.IndexOf(":") >= 0 Then
                        AddedMinutes = Minute(AddedTime)
                        AddedSeconds = Second(AddedTime)
                    Else
                        AddedMinutes = Minute(AddedTime)
                    End If
                End If

                Dim tmpText As String = ""
                If QueryText <> "" Then
                    tmpText = ";" & Environment.NewLine
                End If
                QueryText &= tmpText & "Delete From Soccer.dbo.pa_live_stats Where (Event = " & EventType & ") And (Id = '" & Id & "') And (MatchId = '" & MatchId & "')"
                QueryText &= ";" & Environment.NewLine & "Insert Into Soccer.dbo.pa_live_stats(MatchId, Id, Event, TeamId, TeamName, Player1Id, Player1Firstname, Player1Surname, Player2Id, Player2Firstname, Player2Surname, NormalMinutes, NormalSeconds, NormalTime, AddedMinutes, AddedSeconds, AddedTime, Type, Reason, OnTarget, Outcome, OwnGoal, GoldenGoal, How, WhereFrom ,WhereTo, Distance, KeeperCorrect, Rating, Comment, Rescinded, HomeTeamScore, AwayTeamScore, HomeTeamPenalties, AwayTeamPenalties, Modified) Values ('" & MatchId & "', '" & Id & "', " & EventType & ", " & TeamId & ", '" & TeamName & "', " & Player1Id & ", '" & Player1Firstname & "', '" & Player1Surname & "', " & Player2Id & ", '" & Player2Firstname & "', '" & Player2Surname & "', " & NormalMinutes & ", " & NormalSeconds & ", '" & NormalTime & "', " & AddedMinutes & ", " & AddedSeconds & ", '" & AddedTime & "', '" & Type & "', '" & Reason & "', " & OnTarget & ", '" & Outcome & "', " & OwnGoal & ", " & GoldenGoal & ", '" & How & "', '" & WhereFrom & "' , '" & WhereTo & "', '" & Distance & "', " & KeeperCorrect & ", " & Rating & ", '" & Comment & "', " & Rescinded & ", " & HomeTeamScore & ", " & AwayTeamScore & ", " & HomeTeamPenalties & ", " & AwayTeamPenalties & ", '" & DateTime.Now & "')"
            ElseIf EventType = 50 Then
                Dim tmpText As String = ""
                If QueryText <> "" Then
                    tmpText = ";" & Environment.NewLine
                End If
                QueryText &= tmpText & "Delete From Soccer.dbo.pa_live_stats Where (Id = '" & Id & "') And (MatchId = '" & MatchId & "')"
            ElseIf EventType = 0 Then
                Select Case Status
                    Case "KO"
                        NormalTime = "00:00"
                    Case "HT"
                        NormalTime = "45:01"
                    Case "SHS"
                        NormalTime = "45:02"
                    Case "FT"
                        NormalTime = "90:01"
                    Case "FTET"
                        NormalTime = "90:01"
                    Case "ETS"
                        NormalTime = "90:02"
                    Case "ETHT"
                        NormalTime = "105:01"
                    Case "ETSHS"
                        NormalTime = "105:02"
                    Case "ETFT"
                        NormalTime = "120:01"
                    Case "ETFTPT"
                        NormalTime = "120:01"
                    Case "PT"
                        NormalTime = "120:02"
                    Case "PTFT"
                        NormalTime = "120:03"
                    Case Else
                        NormalTime = ""
                End Select

                If NormalTime <> "" Then
                    If NormalTime.IndexOf(":") >= 0 Then
                        NormalMinutes = Minute(NormalTime)
                        NormalSeconds = Second(NormalTime)
                    Else
                        NormalMinutes = Minute(NormalTime)
                    End If
                End If

                If NormalTime <> "" Then
                    Dim tmpText As String = ""
                    If QueryText <> "" Then
                        tmpText = ";" & Environment.NewLine
                    End If
                    QueryText &= tmpText & "Delete From Soccer.dbo.pa_live_stats Where (Id = '" & EventId & "') And (Event = " & EventType & ") And (MatchId = '" & MatchId & "');" & Environment.NewLine & "Delete From Soccer.dbo.pa_live_text Where (Id = '" & EventId & "') And (MatchId = '" & MatchId & "');" & Environment.NewLine & "Insert Into Soccer.dbo.Pa_live_stats (MatchId, Id, Event, Status, NormalMinutes, NormalSeconds, NormalTime, Modified) Values ('" & MatchId & "', '" & EventId & "', " & EventType & ", '" & Status & "', " & NormalMinutes & ", " & NormalSeconds & ", '" & NormalTime & "', '" & DateTime.Now & "');" & Environment.NewLine & "Insert Into Soccer.dbo.pa_live_text (MatchId, Id, NormalMinutes, NormalSeconds, Status) Values ('" & MatchId & "', '" & EventId & "', '" & NormalMinutes & "', '" & NormalSeconds & "', '" & Status & "')"
                End If
            End If
        Next

        If QueryText <> "" Then
            QueryText = QueryText.Replace("= '", "= ||")
            QueryText = QueryText.Replace("='", "=||")

            QueryText = QueryText.Replace(", '", ", ||")
            QueryText = QueryText.Replace(",'", ",||")
            QueryText = QueryText.Replace("('", "(||")
            QueryText = QueryText.Replace("( '", "( ||")

            QueryText = QueryText.Replace("',", "||,")
            QueryText = QueryText.Replace("' ,", "|| ,")
            QueryText = QueryText.Replace("')", "||)")
            QueryText = QueryText.Replace("' )", "|| )")

            QueryText = QueryText.Replace("'", "''")
            QueryText = QueryText.Replace("True", "1")
            QueryText = QueryText.Replace("False", "0")
            QueryText = QueryText.Replace("||", "'")

            SqlQuery = New SqlCommand(QueryText, DatabaseConn)
            SqlQuery.ExecuteNonQuery()
        End If

        Return Completed

    End Function

    Private Function MatchStats(ByVal MatchId As String, ByVal xmlStats As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim QueryText As String = ""
        Dim tmpTime As String = ""
        Dim StatsTime As Integer = 0
        Dim Dominator As Integer = 0
        Dim Possession As Integer = 0
        Dim Keyplayer As Integer = 0
        Dim HotSpot As String = ""
        Dim FirstHalfIndicated As String = ""
        Dim FirstHalfActual As String = ""
        Dim SecondHalfIndicated As String = ""
        Dim SecondHalfActual As String = ""

        Dim xmlAttributes As XmlAttributeCollection = xmlStats.Attributes
        Dim M As Integer
        For M = 0 To xmlAttributes.Count - 1
            Dim tmpName As String = xmlAttributes(M).Name
            Select Case tmpName
                Case "statTime"
                    tmpTime = xmlAttributes(M).InnerText
                Case Else

            End Select
        Next

        Dim xmlTypes As XmlNodeList = xmlStats.ChildNodes
        Dim I As Integer
        For I = 0 To xmlTypes.Count - 1
            xmlAttributes = xmlTypes(I).Attributes
            For M = 0 To xmlAttributes.Count - 1
                Dim tmpName As String = xmlAttributes(M).Name
                Select Case tmpName
                    Case "dominance"
                        Dominator = xmlAttributes(M).InnerText
                    Case "possession"
                        Possession = xmlAttributes(M).InnerText
                    Case "playerId"
                        Keyplayer = xmlAttributes(M).InnerText
                    Case "hotSpot"
                        HotSpot = xmlAttributes(M).InnerText
                    Case "firstHalfIndicated"
                        FirstHalfIndicated = xmlAttributes(M).InnerText
                    Case "firstHalfActual"
                        FirstHalfActual = xmlAttributes(M).InnerText
                    Case "secondHalfIndicated"
                        SecondHalfIndicated = xmlAttributes(M).InnerText
                    Case "secondHalfActual"
                        SecondHalfActual = xmlAttributes(M).InnerText
                    Case Else

                End Select
            Next
        Next

        SqlQuery = New SqlCommand("Select Count(*) As Total From Soccer.dbo.pa_live_matchstats Where (MatchId = '" & MatchId & "')", DatabaseConn)
        Dim Count As Integer = SqlQuery.ExecuteScalar

        If Count < 1 Then
            QueryText = "Insert Into Soccer.dbo.pa_live_matchstats (MatchId) Values ('" & MatchId & "')"
        End If

        Dim tmpArrayList As ArrayList = New ArrayList
        If FirstHalfIndicated <> "" Then
            tmpArrayList.Add("firstHalfIndicated = '" & FirstHalfIndicated.Replace("'", "''") & "'")
        End If
        If FirstHalfActual <> "" Then
            tmpArrayList.Add("firstHalfActual = '" & FirstHalfActual.Replace("'", "''") & "'")
        End If
        If SecondHalfIndicated <> "" Then
            tmpArrayList.Add("secondHalfIndicated = '" & SecondHalfIndicated.Replace("'", "''") & "'")
        End If
        If SecondHalfActual <> "" Then
            tmpArrayList.Add("secondHalfActual = '" & SecondHalfActual.Replace("'", "''") & "'")
        End If
        If Dominator <> 0 Then
            tmpArrayList.Add("Dominator = " & Dominator & "")
        End If
        If Possession > 0 Then
            tmpArrayList.Add("Possession = " & Possession & "")
        End If
        If Keyplayer > 0 Then
            tmpArrayList.Add("KeyPlayer = " & Keyplayer & "")
        End If
        If HotSpot <> "" Then
            tmpArrayList.Add("hotSpot = '" & HotSpot.Replace("'", "''") & "'")
        End If
        If tmpTime <> "" Then
            tmpArrayList.Add("StatsTime = '" & tmpTime.Replace("'", "''") & "'")
        End If
        tmpArrayList.TrimToSize()

        Dim tmpQuery As String = ""
        For I = 0 To tmpArrayList.Count - 1
            If I = 0 Then
                tmpQuery = " " & tmpArrayList(I)
            Else
                tmpQuery &= ", " & tmpArrayList(I)
            End If
        Next

        If tmpQuery <> "" Then
            tmpQuery = "Update Soccer.dbo.pa_live_matchstats Set" & tmpQuery & " Where (MatchId = '" & MatchId & "')"
            If QueryText <> "" Then
                QueryText &= ";" & Environment.NewLine & tmpQuery
            Else
                QueryText = tmpQuery
            End If
        End If
        If QueryText <> "" Then
            Dim SqlQuery As SqlCommand = New SqlCommand(QueryText, DatabaseConn)
            SqlQuery.ExecuteNonQuery()
        End If

        Return Completed

    End Function

    Private Function TextFeed(ByVal MatchId As String, ByVal xmlText As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim Id As Integer = 0
        Dim ShortText As String = ""
        Dim FullText As String = ""
        Dim NormalMinutes As Integer = 0
        Dim NormalSeconds As Integer = 0
        Dim NormalTime As String = 0
        Dim AddedMinutes As Integer = 0
        Dim AddedSeconds As Integer = 0
        Dim AddedTime As String = 0

        Dim xmlAttributes As XmlAttributeCollection = xmlText.Attributes
        Dim M As Integer
        For M = 0 To xmlAttributes.Count - 1
            Dim tmpName As String = xmlAttributes(M).Name
            Select Case tmpName
                Case "id"
                    Id = xmlAttributes(M).InnerText
                Case "normalTime"
                    NormalTime = xmlAttributes(M).InnerText
                Case "addedTime"
                    AddedTime = xmlAttributes(M).InnerText
                Case Else

            End Select
        Next

        If NormalTime <> "" Then
            If NormalTime.IndexOf(":") >= 0 Then
                NormalMinutes = Minute(NormalTime)
                NormalSeconds = Second(NormalTime)
            Else
                NormalMinutes = Minute(NormalTime)
            End If
        End If

        If AddedTime <> "" Then
            If AddedTime.IndexOf(":") >= 0 Then
                AddedMinutes = Minute(AddedTime)
                AddedSeconds = Second(AddedTime)
            Else
                AddedMinutes = Minute(AddedTime)
            End If
        End If

        FullText = xmlText.InnerXml
        ShortText = FullText
        Dim Repeat As Boolean = True
        Dim Position1 As Integer
        Dim Position2 As Integer
        While Repeat = True
            Position1 = ShortText.IndexOf("<")
            Position2 = ShortText.IndexOf(">", Position1 + 1)
            If Position1 >= 0 And Position2 > 0 Then
                Position2 = Position2 + 1
                ShortText = ShortText.Remove(Position1, Position2 - Position1)
            Else
                Repeat = False
            End If
        End While

        ShortText = ShortText.Replace("Holland", "Netherlands")
        ShortText = ShortText.Replace("Rep of Ireland", "Ireland")

        If FullText.Length > 3 Then
            SqlQuery = New SqlCommand("Delete From Soccer.dbo.pa_live_text Where (MatchId = '" & MatchId & "') And (Id = '" & Id & "');" & Environment.NewLine & "Insert Into Soccer.dbo.pa_live_text (MatchId, Id, ShortText, FullText, NormalMinutes, NormalSeconds, NormalTime, AddedMinutes, AddedSeconds, AddedTime) Values ('" & MatchId & "', " & Id & ", '" & ShortText.Replace("'", "''") & "', '" & FullText.Replace("'", "''") & "', " & NormalMinutes & ", " & NormalSeconds & ", '" & NormalTime & "', " & AddedMinutes & ", " & AddedSeconds & ", '" & AddedTime & "')", DatabaseConn)
            SqlQuery.ExecuteNonQuery()
        End If

        Return Completed

    End Function

#End Region

#Region " Profiles "

    Private Sub Profiles()

        Dim File As String
        Dim Files() As String
        Dim Fi As FileInfo
        Dim MatchId As String = ""

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary") Then
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Temporary")

            Dim Count As Integer = 0
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf(".xml") >= 0 Then
                    Count = Count + 1
                End If
            Next
            If Count > 0 Then
                updateListbox("Processing profiles", True)

                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And Fi.Name.StartsWith("player_") And DbConnected = True Then
                        Dim MoveFile As Integer = 1
                        Try
                            Dim xmlDoc As New XmlDocument
                            xmlDoc.Load(File)

                            Dim xmlTypes As XmlNodeList = xmlDoc.SelectNodes("SoccerPlayerProfiles")
                            Dim N As Integer
                            For N = 0 To xmlTypes.Count - 1
                                MoveFile = Player(xmlTypes(N))
                            Next

                            If MoveFile > 1 Then
                                Exit For
                            End If

                            xmlDoc = Nothing
                        Catch ex As Exception
                            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                                MoveFile = 3
                            Else
                                MoveFile = 2
                                Call ErrorHandler(MatchId, ex.Message, "Error with the profiles")
                            End If
                        End Try
                        If MoveFile = 1 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Complete\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf MoveFile = 2 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf MoveFile = 0 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf MoveFile = -1 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        End If
                    ElseIf Fi.Name.IndexOf(".xml") >= 0 And Fi.Name.StartsWith("club_") And DbConnected = True Then
                        Dim MoveFile As Integer = 1
                        Try
                            Dim xmlDoc As New XmlDocument
                            xmlDoc.Load(File)

                            Dim xmlTypes As XmlNodeList = xmlDoc.SelectNodes("SoccerTeamProfiles")
                            Dim N As Integer
                            For N = 0 To xmlTypes.Count - 1
                                MoveFile = Team(xmlTypes(N))
                            Next

                            If MoveFile > 1 Then
                                Exit For
                            End If

                            xmlDoc = Nothing
                        Catch ex As Exception
                            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                                MoveFile = 3
                            Else
                                MoveFile = 2
                                Call ErrorHandler(MatchId, ex.Message, "Error with the profiles")
                            End If
                        End Try
                        If MoveFile = 1 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Complete\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf MoveFile = 2 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf MoveFile = 0 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        ElseIf MoveFile = -1 Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\ByPassed\" & Fi.Name & "", True)
                            Fi.Delete()
                        End If
                    End If
                Next

                updateListbox("Profiles complete", True)
            End If
        End If

    End Sub

    Private Function Team(ByVal xmlTeam As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim QueryText As String = ""

        Try

            Dim xmlTeams As XmlNodeList = xmlTeam.SelectNodes("Teams/Team")
            Dim I As Integer
            For I = 0 To xmlTeams.Count - 1
                Dim Id As String = ""
                Dim Name As String = ""
                Dim N As Integer
                Dim tmpName As String

                Dim xmlAttributes As XmlAttributeCollection = xmlTeams(I).Attributes
                For N = 0 To xmlAttributes.Count - 1
                    tmpName = xmlAttributes(N).Name
                    Select Case tmpName
                        Case "id"
                            Id = xmlAttributes(N).InnerText
                        Case "name"
                            Name = xmlAttributes(N).InnerText
                    End Select
                Next

                Dim xmlPlayers As XmlNodeList = xmlTeams(I).SelectNodes("Squad/Player")
                Dim M As Integer
                For M = 0 To xmlPlayers.Count - 1
                    Dim playerId As String = ""
                    Dim squadNumber As String = ""
                    Dim Current As Integer = 0
                    Dim FirstName As String = ""
                    Dim MiddleNames As String = ""
                    Dim LastName As String = ""
                    Dim FullName As String = ""

                    xmlAttributes = xmlPlayers(M).Attributes
                    Dim O As Integer
                    For O = 0 To xmlAttributes.Count - 1
                        tmpName = xmlAttributes(O).Name
                        Select Case tmpName
                            Case "id"
                                playerId = xmlAttributes(O).InnerText
                            Case "squadNumber"
                                squadNumber = xmlAttributes(O).InnerText
                            Case "current"
                                If xmlAttributes(O).InnerText = "Yes" Then
                                    Current = 1
                                End If
                        End Select
                    Next

                    Dim xmlNames As XmlNodeList = xmlPlayers(M).SelectNodes("Name")
                    For O = 0 To xmlNames.Count - 1
                        xmlAttributes = xmlNames(O).Attributes
                        Dim P As Integer
                        For P = 0 To xmlAttributes.Count - 1
                            tmpName = xmlAttributes(P).Name
                            Select Case tmpName
                                Case "firstName"
                                    FirstName = xmlAttributes(O).InnerText
                                Case "middleNames"
                                    MiddleNames = xmlAttributes(O).InnerText
                                Case "lastName"
                                    LastName = xmlAttributes(O).InnerText
                            End Select
                        Next
                        FullName = xmlNames(O).InnerText
                    Next

                    If QueryText <> "" Then
                        QueryText &= Environment.NewLine
                    End If

                    QueryText &= "Insert Into Soccer.dbo.pa_squads (teamId, teamName, Season, Id, squadNumber, isCurrent, firstName, middleName, lastName, fullName, created) Values (" & Id & ", '" & Name.Replace("'", "''") & "', " & paSeason & ", '" & playerId.Replace("'", "''") & "', '" & squadNumber.Replace("'", "''") & "', '" & Current & "', '" & FirstName.Replace("'", "''") & "', '" & MiddleNames.Replace("'", "''") & "', '" & LastName.Replace("'", "''") & "', '" & FullName.Replace("'", "''") & "', GetDate());"
                Next
                If QueryText <> "" Then
                    QueryText = Environment.NewLine & "Delete From Soccer.dbo.pa_squads Where (TeamId = " & Id & ") And (Season = " & paSeason & ");" & QueryText
                End If
            Next

            If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                SqlQuery = New SqlCommand(QueryText, DatabaseConn)
                SqlQuery.ExecuteNonQuery()
            Else
                Completed = 3
            End If

        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler("Team Profiles", ex.Message, "Error with the team")
            End If
        End Try

        Return Completed

    End Function

    Private Function Player(ByVal xmlTeam As System.Xml.XmlNode) As Integer

        Dim Completed As Integer = 1
        Dim QueryText As String = ""

        Try
            Dim xmlTeams As XmlNodeList = xmlTeam.SelectNodes("Players")
            Dim I As Integer
            For I = 0 To xmlTeams.Count - 1
                Dim xmlPlayers As XmlNodeList = xmlTeams(I).SelectNodes("Player")
                Dim N As Integer
                For N = 0 To xmlPlayers.Count - 1
                    Dim Id As String = ""
                    Dim FirstName As String = ""
                    Dim MiddleNames As String = ""
                    Dim LastName As String = ""
                    Dim FullName As String = ""
                    Dim Nationality As String = ""
                    Dim DateOfBirth As DateTime = New DateTime(1900, 1, 1, 0, 0, 0)
                    Dim Height As String = ""
                    Dim Weight As String = ""
                    Dim Teams As String = ""
                    Dim Position As String = ""
                    Dim xmlAttributes As XmlAttributeCollection
                    Dim xmlAttribute As XmlAttribute
                    Dim xmlNodes As XmlNodeList
                    Dim xmlNode As XmlNode
                    Dim M As Integer

                    xmlAttributes = xmlPlayers(N).Attributes
                    For M = 0 To xmlAttributes.Count - 1
                        Dim tmpName As String = xmlAttributes(M).Name
                        Select Case tmpName
                            Case "id"
                                Id = xmlAttributes(M).InnerText
                            Case "nationality"
                                Nationality = xmlAttributes(M).InnerText
                            Case "dateOfBirth"
                                Dim tmpDate As String = xmlAttributes(M).InnerText
                                If tmpDate <> "" Then
                                    DateOfBirth = New DateTime(tmpDate.Substring(0, 4), tmpDate.Substring(4, 2), tmpDate.Substring(6, 2), 0, 0, 0)
                                End If
                        End Select
                    Next

                    Dim teamList As Hashtable = New Hashtable()

                    xmlNodes = xmlPlayers(N).ChildNodes
                    For Each xmlNode In xmlNodes
                        Dim tmpName As String = xmlNode.Name
                        Select Case tmpName
                            Case "Name"
                                FullName = xmlNode.InnerText
                                xmlAttributes = xmlNode.Attributes
                                For Each xmlAttribute In xmlAttributes
                                    Select Case xmlAttribute.Name
                                        Case "firstName"
                                            FirstName = xmlAttribute.InnerText
                                        Case "middleNames"
                                            MiddleNames = xmlAttribute.InnerText
                                        Case "lastName"
                                            LastName = xmlAttribute.InnerText
                                    End Select
                                Next
                            Case "Height"
                                Height = xmlNode.InnerText
                            Case "Weight"
                                Weight = xmlNode.InnerText
                            Case "Career"
                                Dim Type As String = xmlNode.Attributes("type").InnerText
                                If Type = "Club" Then
                                    If xmlNode.HasChildNodes Then
                                        Dim tmpNode As XmlNode = xmlNode.FirstChild
                                        xmlAttributes = tmpNode.Attributes
                                        For Each xmlAttribute In xmlAttributes
                                            Select Case xmlAttribute.Name
                                                Case "position"
                                                    Position = xmlAttribute.InnerText
                                            End Select
                                        Next
                                    End If
                                End If
                        End Select
                    Next

                    If QueryText <> "" Then
                        QueryText &= Environment.NewLine
                    End If

                    QueryText &= ";" & Environment.NewLine & "Delete From Soccer.dbo.pa_profiles Where (Id = " & Id & ");" & Environment.NewLine & "Insert Into Soccer.dbo.pa_profiles (id, firstName, middleNames, lastName, fullName, nationality, dateOfBirth, height, weight, created, Position) Values (" & Id & ", '" & FirstName.Replace("'", "''") & "', '" & MiddleNames.Replace("'", "''") & "', '" & LastName.Replace("'", "''") & "', '" & FullName.Replace("'", "''") & "', '" & Nationality.Replace("'", "''") & "', '" & DateOfBirth & "', '" & Height.Replace("'", "''") & "', '" & Weight.Replace("'", "''") & "', GetDate(), '" & Position & "');"
                Next
            Next

            If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
                SqlQuery = New SqlCommand(QueryText, DatabaseConn)
                SqlQuery.ExecuteNonQuery()
            Else
                Completed = 3
            End If
        Catch ex As Exception
            If ex.Source = ".Net SqlClient Data Provider" And ex.Message.IndexOf("Incorrect syntax near") < 0 Then
                Completed = 3
            Else
                Completed = 2
                Call ErrorHandler("Profiles", ex.Message, "Error with the player")
            End If
        End Try

        Return Completed

    End Function

#End Region

#Region " Competitions "

    Private Sub Competitions()

        Dim File As String
        Dim Files() As String
        Dim Fi As FileInfo

        If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Competition\Temporary") Then
            Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Competition\Temporary")
            Dim Count As Integer = 0
            For Each File In Files
                Fi = New FileInfo(File)
                If Fi.Name.IndexOf(".xml") >= 0 And Fi.Name.IndexOf("soc_cmp") >= 0 Then
                    Count = Count + 1
                End If
            Next
            If Count > 0 Then
                updateListbox("Processing competitions", True)
                For Each File In Files
                    Fi = New FileInfo(File)
                    If Fi.Name.IndexOf(".xml") >= 0 And Fi.Name.IndexOf("soc_cmp") >= 0 Then
                        Dim MoveFile As Boolean = True
                        Try
                            Dim xmlDoc As New XmlDocument
                            xmlDoc.Load(File)

                            Dim xmlCompetitions As XmlNodeList = xmlDoc.SelectNodes("Competitions/Competition")
                            Dim I As Integer
                            For I = 0 To xmlCompetitions.Count - 1
                                Dim Competition_Id As Integer
                                Dim Competition_Sponsor As String = ""
                                Dim Competition_Name As String = ""
                                Dim Competition_StartDate As DateTime = New DateTime(1900, 1, 1)
                                Dim Competition_EndDate As DateTime = New DateTime(1900, 1, 1)

                                Dim tmpNodes As XmlAttributeCollection = xmlCompetitions(I).Attributes
                                Dim M As Integer
                                For M = 0 To tmpNodes.Count - 1
                                    Dim tmpName As String = tmpNodes(M).Name
                                    Select Case tmpName
                                        Case "id"
                                            Competition_Id = tmpNodes(M).InnerText
                                        Case "sponsor"
                                            Competition_Sponsor = tmpNodes(M).InnerText
                                        Case "name"
                                            Competition_Name = tmpNodes(M).InnerText
                                        Case "startDate"
                                            Dim tmpDate As String = tmpNodes(M).InnerText
                                            Dim tmpYear As String = tmpDate.Substring(0, 4)
                                            Dim tmpMonth As String = tmpDate.Substring(4, 2)
                                            Dim tmpDay As String = tmpDate.Substring(6, 2)
                                            If tmpMonth.StartsWith("0") Then
                                                tmpMonth = tmpMonth.Substring(1, 1)
                                            End If
                                            If tmpDay.StartsWith("0") Then
                                                tmpDay = tmpDay.Substring(1, 1)
                                            End If
                                            Competition_StartDate = New DateTime(tmpYear, tmpMonth, tmpDay, 0, 0, 0)
                                        Case "endDate"
                                            Dim tmpDate As String = tmpNodes(M).InnerText
                                            Dim tmpYear As String = tmpDate.Substring(0, 4)
                                            Dim tmpMonth As String = tmpDate.Substring(4, 2)
                                            Dim tmpDay As String = tmpDate.Substring(6, 2)
                                            If tmpMonth.StartsWith("0") Then
                                                tmpMonth = tmpMonth.Substring(1, 1)
                                            End If
                                            If tmpDay.StartsWith("0") Then
                                                tmpDay = tmpDay.Substring(1, 1)
                                            End If
                                            Competition_EndDate = New DateTime(tmpYear, tmpMonth, tmpDay, 0, 0, 0)
                                        Case Else

                                    End Select
                                Next

                                SqlQuery = New SqlCommand("Delete From Soccer.dbo.pa_Competitions Where Competition_Id = " & Competition_Id & " And (Competition_Season = 2)", DatabaseConn)
                                SqlQuery.ExecuteNonQuery()

                                Dim xmlStages As XmlNodeList = xmlCompetitions(I).SelectNodes("Stages/Stage")
                                For M = 0 To xmlStages.Count - 1
                                    Dim Stage_Number As Integer
                                    Dim Stage_Name As String = ""
                                    Dim Stage_StartDate As DateTime = New DateTime(1900, 1, 1)
                                    Dim Stage_EndDate As DateTime = New DateTime(1900, 1, 1)
                                    Dim Stage_Type As String

                                    tmpNodes = xmlStages(M).Attributes
                                    Dim N As Integer
                                    For N = 0 To tmpNodes.Count - 1
                                        Dim tmpName As String = tmpNodes(N).Name
                                        Select Case tmpName
                                            Case "number"
                                                Stage_Number = tmpNodes(N).InnerText
                                            Case "type"
                                                Stage_Type = tmpNodes(N).InnerText
                                            Case "name"
                                                Stage_Name = tmpNodes(N).InnerText
                                            Case "startDate"
                                                Dim tmpDate As String = tmpNodes(N).InnerText
                                                Dim tmpYear As String = tmpDate.Substring(0, 4)
                                                Dim tmpMonth As String = tmpDate.Substring(4, 2)
                                                Dim tmpDay As String = tmpDate.Substring(6, 2)
                                                If tmpMonth.StartsWith("0") Then
                                                    tmpMonth = tmpMonth.Substring(1, 1)
                                                End If
                                                If tmpDay.StartsWith("0") Then
                                                    tmpDay = tmpDay.Substring(1, 1)
                                                End If
                                                Stage_StartDate = New DateTime(tmpYear, tmpMonth, tmpDay, 0, 0, 0)
                                            Case "endDate"
                                                Dim tmpDate As String = tmpNodes(N).InnerText
                                                Dim tmpYear As String = tmpDate.Substring(0, 4)
                                                Dim tmpMonth As String = tmpDate.Substring(4, 2)
                                                Dim tmpDay As String = tmpDate.Substring(6, 2)
                                                If tmpMonth.StartsWith("0") Then
                                                    tmpMonth = tmpMonth.Substring(1, 1)
                                                End If
                                                If tmpDay.StartsWith("0") Then
                                                    tmpDay = tmpDay.Substring(1, 1)
                                                End If
                                                Stage_EndDate = New DateTime(tmpYear, tmpMonth, tmpDay, 0, 0, 0)
                                            Case Else

                                        End Select
                                    Next

                                    Dim xmlRounds As XmlNodeList = xmlStages(M).SelectNodes("Rounds/Round")
                                    For N = 0 To xmlRounds.Count - 1
                                        Dim Round_Number As Integer
                                        Dim Round_Name As String = ""
                                        Dim Round_StartDate As DateTime = New DateTime(1900, 1, 1)
                                        Dim Round_EndDate As DateTime = New DateTime(1900, 1, 1)
                                        Dim Round_Legs As Integer = 1
                                        Dim Round_Replay As Integer = 0

                                        tmpNodes = xmlRounds(N).Attributes
                                        Dim O As Integer
                                        For O = 0 To tmpNodes.Count - 1
                                            Dim tmpName As String = tmpNodes(O).Name
                                            Select Case tmpName
                                                Case "number"
                                                    Round_Number = tmpNodes(O).InnerText
                                                Case "name"
                                                    Round_Name = tmpNodes(O).InnerText
                                                Case "startDate"
                                                    Dim tmpDate As String = tmpNodes(O).InnerText
                                                    Dim tmpYear As String = tmpDate.Substring(0, 4)
                                                    Dim tmpMonth As String = tmpDate.Substring(4, 2)
                                                    Dim tmpDay As String = tmpDate.Substring(6, 2)
                                                    If tmpMonth.StartsWith("0") Then
                                                        tmpMonth = tmpMonth.Substring(1, 1)
                                                    End If
                                                    If tmpDay.StartsWith("0") Then
                                                        tmpDay = tmpDay.Substring(1, 1)
                                                    End If
                                                    Round_StartDate = New DateTime(tmpYear, tmpMonth, tmpDay, 0, 0, 0)
                                                Case "endDate"
                                                    Dim tmpDate As String = tmpNodes(O).InnerText
                                                    Dim tmpYear As String = tmpDate.Substring(0, 4)
                                                    Dim tmpMonth As String = tmpDate.Substring(4, 2)
                                                    Dim tmpDay As String = tmpDate.Substring(6, 2)
                                                    If tmpMonth.StartsWith("0") Then
                                                        tmpMonth = tmpMonth.Substring(1, 1)
                                                    End If
                                                    If tmpDay.StartsWith("0") Then
                                                        tmpDay = tmpDay.Substring(1, 1)
                                                    End If
                                                    Round_EndDate = New DateTime(tmpYear, tmpMonth, tmpDay, 0, 0, 0)
                                                Case "legs"
                                                    Round_Legs = tmpNodes(O).InnerText
                                                Case "replaysPossible"
                                                    If tmpNodes(O).InnerText = "Yes" Then
                                                        Round_Replay = 1
                                                    End If
                                                Case Else

                                            End Select
                                        Next

                                        Dim Competitions() As String = "100|500|501|510|511|620|625|635|645|650|794|879".Split("|")
                                        Dim P As Integer
                                        Dim Insert As Boolean = False
                                        For P = 0 To Competitions.GetUpperBound(0)
                                            If Competitions(P) = Competition_Id Then
                                                Insert = True
                                                Exit For
                                            End If
                                        Next

                                        If Insert = True Then
                                            SqlQuery = New SqlCommand("Insert Into Soccer.dbo.pa_Competitions (Competition_Id, Competition_Season, Competition_Name, Competition_Sponsor, Competition_Startdate, Competition_Enddate, Stage_Number, Stage_Name, Stage_Startdate, Stage_Enddate, Stage_Type, Round_Number, Round_Name, Round_Startdate, Round_Enddate, Round_Legs, Round_Replay, Created) Values (" & Competition_Id & ", 2, '" & Competition_Name.Replace("'", "''") & "', '" & Competition_Sponsor.Replace("'", "''") & "', '" & Competition_StartDate & "', '" & Competition_EndDate & "', " & Stage_Number & ", '" & Stage_Name.Replace("'", "''") & "', '" & Stage_StartDate & "', '" & Stage_EndDate & "', '" & Stage_Type.Replace("'", "''") & "', " & Round_Number & ", '" & Round_Name.Replace("'", "''") & "', '" & Round_StartDate & "', '" & Round_EndDate & "', " & Round_Legs & ", " & Round_Replay & ", '" & DateTime.Now.ToString & "')", DatabaseConn)
                                            SqlQuery.ExecuteNonQuery()
                                        End If
                                    Next
                                Next
                            Next
                            xmlCompetitions = Nothing

                            xmlDoc = Nothing
                        Catch ex As Exception
                            MoveFile = False
                            Call ErrorHandler("", ex.Message, "Error with the competitions")
                        End Try
                        If MoveFile = True Then
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Competition\Complete\" & Fi.Name & "", True)
                        Else
                            Fi.CopyTo("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Competition\ByPassed\" & Fi.Name & "", True)
                        End If
                        Fi.Delete()
                    End If
                Next
                If Files.Length > 0 Then
                    updateListbox("Competitions complete", True)
                End If
            End If
        End If

    End Sub

#End Region

#Region " Sms "

    Private Sub mtnFixtures()

        updateListbox("Processing MTN Fixtures", True)

        If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
            Dim Tournaments As String = ""
            Dim IEnum As IEnumerator = mtnTournaments.GetEnumerator

            Dim da As SqlDataAdapter = New SqlDataAdapter
            Dim ds As New DataSet
            While IEnum.MoveNext
                If Tournaments <> "" Then
                    Tournaments &= ","
                End If
                Tournaments &= IEnum.Current
            End While

            Try
                SqlQuery = New SqlCommand("Select a.Competition, a.Id, a.MatchDateTime, a.HomeTeamName, a.AwayTeamName, a.HomeTeamId, a.AwayTeamId, b.mtnId As HomeTeamMtn, c.mtnId As AwayTeamMtn From Soccer.dbo.pa_Matches a INNER JOIN Soccer.dbo.pa_TeamNames b ON b.TeamId = a.HomeTeamId INNER JOIN Soccer.dbo.pa_TeamNames c ON c.TeamId = a.AwayTeamId Where (a.Competition In (" & Tournaments & ")) And (a.StatusId = 0) And (GetDate() > DateAdd(minute, -15, a.MatchDateTime)) And (b.Season = a.Season And c.Season = a.Season) And (b.Competition = a.Competition And c.Competition = a.Competition) And (a.StatusId = 0) Order By a.MatchDateTime", DatabaseConn)
                da = New SqlDataAdapter
                da.SelectCommand = SqlQuery
                da.Fill(ds, "matches")

                If ds.Tables("matches").Rows.Count > 0 Then
                    SqlQuery = New SqlCommand("Select * From SSZGeneral.dbo.mtnSms Where (LeagueId In (" & Tournaments & ")) And (event = 'fixture') And (Created > DateAdd(day, -1, GetDate()))", DatabaseConn)
                    da = New SqlDataAdapter
                    da.SelectCommand = SqlQuery
                    da.Fill(ds, "recent")

                    SqlQuery = New SqlCommand("Select a.Id, a.Competition, a.MatchDateTime From Soccer.dbo.pa_Matches a Where (a.Competition In (" & Tournaments & ")) And (a.Season = " & paSeason & ") And (a.StatusId < 14) Order By a.MatchDateTime, a.Id", DatabaseConn)
                    da = New SqlDataAdapter
                    da.SelectCommand = SqlQuery
                    da.Fill(ds, "numbers")

                    SqlQuery = New SqlCommand("Select * From SSZGeneral.dbo.mtnSms Where (LeagueId In (" & Tournaments & ")) And (Created > DateAdd(day, -1, GetDate()))", DatabaseConn)
                    da = New SqlDataAdapter
                    da.SelectCommand = SqlQuery
                    da.Fill(ds, "sms")
                End If
            Catch ex As Exception
                Call ErrorHandler("MTN Sms fixtures", ex.Message, "Error retrieving match details from the db")
            End Try

            Dim tmpRows() As DataRow = ds.Tables("matches").Select("", "")
            Dim tmpRow As DataRow
            For Each tmpRow In tmpRows
                Dim tempRows() As DataRow = ds.Tables("recent").Select("(matchId = " & tmpRow.Item("Id") & ") And (event = 'fixture')", "")
                If tempRows.Length = 0 Then
                    Dim Title As String = ""
                    Dim FileName As String = ""
                    Dim ExternalReference As String = ""
                    Dim ShortCode As String = ""
                    Dim MatchDateTime As DateTime = tmpRow.Item("MatchDateTime")
                    Dim smsText As String
                    Dim MatchNumber As Integer = 1
                    Dim LeagueId As Integer = tmpRow.Item("Competition")
                    Dim FileFolder As String = ""
                    Dim SmsFolders As ArrayList = New ArrayList
                    Dim Total As Integer = 0

                    Dim tempRows1() As DataRow = ds.Tables("numbers").Select("Competition = " & LeagueId, "MatchDateTime, Id")
                    Dim tempRow1 As DataRow
                    For Each tempRow1 In tempRows1
                        If tempRow1.Item("Id") = tmpRow.Item("Id") Then
                            Exit For
                        End If
                        MatchNumber = MatchNumber + 1
                    Next

                    If LeagueId = 794 Then
                        MatchNumber = MatchNumber + 8
                    End If

                    smsText = MatchDateTime.ToString("dd MMMM") & ", MATCH ALERT: " & tmpRow.Item("HomeTeamName") & " v " & tmpRow.Item("AwayTeamName") & ". Kick-off " & MatchDateTime.ToString("HH':'mm 'CAT'")

                    Select Case LeagueId
                        Case 794
                            ShortCode = "PSL"
                            SmsFolders.Add("PSL " & tmpRow.Item("HomeTeamMtn") & " SMS")
                            SmsFolders.Add("PSL " & tmpRow.Item("AwayTeamMtn") & " SMS")
                        Case 100
                            ShortCode = "EPL"
                            SmsFolders.Add("EPL " & tmpRow.Item("HomeTeamMtn") & " SMS")
                            SmsFolders.Add("EPL " & tmpRow.Item("AwayTeamMtn") & " SMS")
                    End Select

                    Title = "Match Alert"
                    FileName = "metadata_" & ShortCode & "_game_" & MatchNumber & "_alert"
                    ExternalReference = "game_" & ShortCode & "_" & MatchNumber & "_alert_sms"
                    FileFolder = MatchNumber.ToString

                    tempRows1 = ds.Tables("sms").Select("(LeagueId = " & LeagueId & ") And (MatchId = '" & tmpRow.Item("Id") & "')", "")
                    If tempRows1.Length > 0 Then
                        Total = tempRows1.Length
                    End If

                    Total = (Total * 1) + 1

                    If Not System.IO.Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "") Then
                        System.IO.Directory.CreateDirectory("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "")
                    End If

                    Dim Files() As String = System.IO.Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "")
                    Dim N As Integer
                    For N = 0 To Files.Length - 1
                        If Files(N).IndexOf(FileName & ".xml") >= 0 Then
                            File.Delete(Files(N))
                        End If
                        If Files(N).IndexOf(FileName & ".zip") >= 0 Then
                            File.Delete(Files(N))
                        End If
                    Next

                    Dim ws As New XmlWriterSettings()
                    Dim utf8 As Encoding
                    utf8 = Encoding.UTF8
                    ws.CheckCharacters = True
                    ws.CloseOutput = True
                    ws.Encoding = utf8
                    ws.Indent = True
                    ws.ConformanceLevel = ConformanceLevel.Auto

                    'SmsFolders.Clear()
                    'SmsFolders.Add("Test Confederation Cup Notofications")

                    Using tmpWriter As XmlWriter = XmlWriter.Create("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "/" & FileName & ".xml", ws)
                        tmpWriter.WriteStartDocument()
                        tmpWriter.WriteStartElement("import", "http://www.volantis.com/xmlns/vcms/cmsimport")
                        tmpWriter.WriteAttributeString("xsi", "schemaLocation", "http://www.w3.org/2001/XMLSchema-instance", "http://www.volantis.com/xmlns/vcms/cmsimport../../xsd/cmsimport1-0.xsd")

                        smsText = FixText(smsText)

                        Dim I As Integer
                        For I = 0 To SmsFolders.Count - 1
                            tmpWriter.WriteStartElement("article")
                            tmpWriter.WriteElementString("charge-code", "0")
                            tmpWriter.WriteElementString("searchable", "false")
                            tmpWriter.WriteElementString("content-owner", "Subscription")
                            tmpWriter.WriteElementString("provider-code", "901000442")
                            tmpWriter.WriteElementString("service-type", "023")
                            tmpWriter.WriteElementString("is-promo", "false")
                            tmpWriter.WriteElementString("time-to-live", "-1")
                            tmpWriter.WriteElementString("name", ExternalReference & Total & "_l")
                            tmpWriter.WriteElementString("title", SmsFolders(I).Replace("0/", ""))
                            tmpWriter.WriteElementString("target-folder-id", "0/" & SmsFolders(I))
                            tmpWriter.WriteElementString("body-title", Title)
                            tmpWriter.WriteElementString("body-text", smsText)
                            tmpWriter.WriteElementString("link-text", "Not used")
                            tmpWriter.WriteElementString("include-in-index", "false")
                            tmpWriter.WriteElementString("external-reference", ExternalReference & Total & "_t")
                            tmpWriter.WriteEndElement()
                            Total = Total + 1
                        Next

                        tmpWriter.Flush()
                    End Using

                    Dim Fs As FileStream
                    Dim Sw As StreamWriter
                    Dim Sr As StreamReader
                    Dim String1 As String = ""

                    Fs = New FileStream("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "/" & FileName & ".xml", FileMode.Open, FileAccess.Read)
                    Sr = New StreamReader(Fs)
                    String1 = Sr.ReadToEnd
                    Sr.Close()

                    String1 = String1.Replace("﻿", "")
                    String1 = String1.Replace("ï»¿", "")

                    Fs = New FileStream("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "/" & FileName & ".xml", FileMode.Create, FileAccess.Write)
                    Sw = New StreamWriter(Fs, Encoding.GetEncoding("ISO-8859-1"))
                    Sw.Write(String1)
                    Sw.Close()

                    Xceed.Zip.Licenser.LicenseKey = "ZIN23-X0UZT-5AHMP-4A2A"

                    Dim Status As Integer = 1
                    Try
                        Dim tmpArchive As New Xceed.Zip.ZipArchive(New Xceed.FileSystem.DiskFile("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\sms\" & LeagueId & "\" & FileFolder & "\" & FileName & ".zip"))
                        Dim MetaFile As New Xceed.FileSystem.DiskFile("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\sms\" & LeagueId & "\" & FileFolder & "\" & FileName & ".xml")
                        MetaFile.CopyTo(tmpArchive, True)
                        tmpArchive = Nothing

                        Status = 2
                        Dim Server As String = "196.13.230.226"
                        Dim UserName As String = "ssport"
                        Dim Password As String = "123ssport321"

                        Dim Send As Boolean = True

                        If Send = True Then
                            Dim Ftp As Ftp = New Ftp
                            Ftp.Connect(Server)
                            Ftp.Login(UserName, Password)
                            Ftp.SetTransferType(FtpTransferType.Binary)
                            Ftp.KeepAlive()
                            Ftp.ChangeDirectory("content/production")
                            Status = 3
                            Ftp.PutFile("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\sms\" & LeagueId & "\" & FileFolder & "\" & FileName & ".zip", FileName & ".zip")
                            Status = 4
                            Ftp.Disconnect()

                            Ftp.Dispose()
                        Else
                            Status = 4
                        End If
                    Catch ex As Exception
                        Call ErrorHandler("MTN Sms fixtures", ex.Message, "Error uploading sms")
                    End Try

                    If Status > 2 Then
                        Try
                            SqlQuery = New SqlCommand("Insert Into SSZGeneral.dbo.mtnSms (leagueId, databaseId, matchId, matchNumber, event, textEn) Values (" & LeagueId & ", 2, " & tmpRow.Item("Id") & ", " & MatchNumber & ", 'fixture', '" & smsText.Replace("'", "''") & "')", DatabaseConn)
                            SqlQuery.ExecuteNonQuery()
                        Catch ex As Exception
                            Call ErrorHandler("MTN Sms fixtures", ex.Message, "Error inserting into database")
                        End Try
                    End If
                End If
            Next
            ds = Nothing
        End If

        updateListbox("MTN Fixtures complete", True)

    End Sub

    Private Sub mtnMatchSms(ByVal LeagueId As Integer, ByVal MatchId As String, ByVal Type As String, Optional ByVal eventId As String = "")

        If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
            Dim da As SqlDataAdapter = New SqlDataAdapter
            Dim ds As New DataSet
            Dim smsText As String = ""

            Try
                Dim Season As Integer = paSeason
                SqlQuery = New SqlCommand("Select a.Id, a.Competition, a.Season, a.MatchDateTime, a.HomeTeamId, a.HomeTeamName, a.HomeTeamScore, a.HalfTimeHomeTeamScore, b.mtnId As HomeTeamMtn, a.AwayTeamId, a.AwayTeamName, a.AwayTeamScore, a.HalfTimeAwayTeamScore, c.mtnId As AwayTeamMtn, a.HomeTeamPenalties, a.AwayTeamPenalties, a.Result From Soccer.dbo.pa_Matches a INNER JOIN Soccer.dbo.pa_TeamNames b ON b.TeamId = a.HomeTeamId INNER JOIN Soccer.dbo.pa_TeamNames c ON c.TeamId = a.AwayTeamId Where (a.Id = '" & MatchId & "') And (b.Competition = a.Competition And c.Competition = a.Competition) And (b.Season = a.Season And c.Season = a.Season)", DatabaseConn)
                da = New SqlDataAdapter
                da.SelectCommand = SqlQuery
                da.Fill(ds, "match")

                If ds.Tables("match").Rows.Count > 0 Then
                    Season = ds.Tables("match").Rows(0).Item("Season")
                End If

                SqlQuery = New SqlCommand("Select Id, TeamId, TeamName, Player1FullName As Player, Player1LastName As Surname, Minutes, Seconds, txtTime, EventId, HomeTeamScore, AwayTeamScore From Soccer.dbo.pa_Events Where (MatchId = '" & MatchId & "') And (EventId > 1 And EventId < 6)", DatabaseConn)
                da = New SqlDataAdapter
                da.SelectCommand = SqlQuery
                da.Fill(ds, "goals")

                SqlQuery = New SqlCommand("Select a.Id, a.Competition, a.MatchDateTime From Soccer.dbo.pa_Matches a Where (a.Competition = " & LeagueId & ") And (a.Season = " & Season & ") And (a.StatusId < 14) Order By a.MatchDateTime, a.Id", DatabaseConn)
                da = New SqlDataAdapter
                da.SelectCommand = SqlQuery
                da.Fill(ds, "numbers")

                SqlQuery = New SqlCommand("Select * From SSZGeneral.dbo.mtnSms Where (MatchId = '" & MatchId & "')", DatabaseConn)
                da = New SqlDataAdapter
                da.SelectCommand = SqlQuery
                da.Fill(ds, "sms")

                If eventId <> "" Then
                    SqlQuery = New SqlCommand("Select Id, TeamId, Player1LastName As Surname, Player1FullName As Player, eventId, Minutes, txtTime, HomeTeamScore, AwayTeamScore From Soccer.dbo.pa_Events Where (MatchId = '" & MatchId & "') And (Id = '" & eventId & "')", DatabaseConn)
                    da = New SqlDataAdapter
                    da.SelectCommand = SqlQuery
                    da.Fill(ds, "event")
                End If
            Catch ex As Exception
                ds.Tables("match").Rows.Clear()
                Call ErrorHandler("MTN Sms Event", ex.Message, "Error retrieving match details from the db (" & MatchId & ")")
            End Try

            Dim Proceed As Boolean = True
            If ds.Tables("match").Rows.Count > 0 Then
                If Type.ToLower = "half" Or Type.ToLower = "full" Then
                    Dim testRows() As DataRow = ds.Tables("sms").Select("event = '" & Type.ToLower & "'", "")
                    If testRows.Length > 0 Then
                        Proceed = False
                    End If
                    If Type.ToLower = "full" And Proceed = True Then
                        testRows = ds.Tables("match").Select("", "")
                        If DateTime.Now.AddHours(-4) < testRows(0).Item("MatchDateTime") Then
                            Proceed = True
                        Else
                            Proceed = False
                        End If
                    End If
                End If
                If Type.ToLower = "goal" Or Type.ToLower = "half" Then
                    If ds.Tables("match").Rows(0).Item("Result") = True Then
                        Proceed = False
                    End If
                End If
            Else
                Proceed = False
            End If

            If Proceed = True Then
                Dim tmpRow As DataRow = ds.Tables("match").Rows(0)
                Dim HomeTeamId As Integer = tmpRow.Item("HomeTeamId")
                Dim AwayTeamId As Integer = tmpRow.Item("AwayTeamId")
                Dim HomeTeamName As String = tmpRow.Item("HomeTeamName")
                Dim AwayTeamName As String = tmpRow.Item("AwayTeamName")
                Dim HomeTeamScore As String = tmpRow.Item("HomeTeamScore")
                Dim AwayTeamScore As String = tmpRow.Item("AwayTeamScore")
                Dim HomeTeamHalfTimeScore As Integer = tmpRow.Item("HalfTimeHomeTeamScore")
                Dim AwayTeamHalfTimeScore As Integer = tmpRow.Item("HalfTimeAwayTeamScore")
                Dim HomeTeamPenalties As Integer = tmpRow.Item("HomeTeamPenalties")
                Dim AwayTeamPenalties As Integer = tmpRow.Item("AwayTeamPenalties")
                Dim HomeTeamScorers As String = ""
                Dim AwayTeamScorers As String = ""
                Dim HomeTeamMTN As String = tmpRow.Item("HomeTeamMTN")
                Dim AwayTeamMTN As String = tmpRow.Item("AwayTeamMTN")
                Dim Total As Integer = 0
                Dim MatchNumber As Integer = 1

                If Type.ToLower = "half" Then
                    If HomeTeamHalfTimeScore > 0 Or AwayTeamHalfTimeScore > 0 Then
                        HomeTeamScore = HomeTeamHalfTimeScore
                        AwayTeamScore = AwayTeamHalfTimeScore
                    End If
                End If

                Dim tempRows1() As DataRow = ds.Tables("numbers").Select("Competition = " & LeagueId, "MatchDateTime, Id")
                Dim tempRow1 As DataRow
                For Each tempRow1 In tempRows1
                    If tempRow1.Item("Id") = tmpRow.Item("Id") Then
                        Exit For
                    End If
                    MatchNumber = MatchNumber + 1
                Next

                tempRows1 = ds.Tables("sms").Select("", "")
                If tempRows1.Length > 0 Then
                    Total = tempRows1.Length
                End If

                Total = (Total * 1) + 1

                If LeagueId = 794 Then
                    MatchNumber = MatchNumber + 8
                End If

                If HomeTeamPenalties > 0 Or AwayTeamPenalties > 0 Then
                    HomeTeamScore &= " (" & HomeTeamPenalties & ")"
                    AwayTeamScore &= " (" & AwayTeamPenalties & ")"
                End If

                Dim tempRows() As DataRow

                If Type.ToLower = "half" Then
                    tempRows = ds.Tables("goals").Select("TeamId = " & HomeTeamId & " And Minutes < 46", "Minutes, Seconds")
                Else
                    tempRows = ds.Tables("goals").Select("TeamId = " & HomeTeamId, "Minutes, Seconds")
                End If
                Dim tempRow As DataRow
                For Each tempRow In tempRows
                    If HomeTeamScorers <> "" Then
                        HomeTeamScorers &= ", "
                    End If
                    If tempRow.Item("EventId") = 3 Then
                        HomeTeamScorers &= tempRow.Item("Surname") & " og"
                    Else
                        HomeTeamScorers &= tempRow.Item("Surname")
                    End If
                Next

                If HomeTeamScorers <> "" Then
                    HomeTeamScorers = " (" & HomeTeamScorers & ")"
                End If

                If Type.ToLower = "half" Then
                    tempRows = ds.Tables("goals").Select("TeamId = " & AwayTeamId & " And Minutes < 46", "Minutes, Seconds")
                Else
                    tempRows = ds.Tables("goals").Select("TeamId = " & AwayTeamId, "Minutes, Seconds")
                End If
                For Each tempRow In tempRows
                    If AwayTeamScorers <> "" Then
                        AwayTeamScorers &= ", "
                    End If
                    If tempRow.Item("EventId") = 3 Then
                        AwayTeamScorers &= tempRow.Item("Surname") & " og"
                    Else
                        AwayTeamScorers &= tempRow.Item("Surname")
                    End If
                Next

                If AwayTeamScorers <> "" Then
                    AwayTeamScorers = " (" & AwayTeamScorers & ")"
                End If

                Dim FileFolder As String = ""
                Dim FileName As String = ""
                Dim ExternalReference As String = ""
                Dim ShortCode As String = ""
                Dim Title As String = ""
                Dim SmsFolders As ArrayList = New ArrayList

                FileFolder = MatchNumber.ToString
                Select Case LeagueId
                    Case 794
                        ShortCode = "PSL"
                        SmsFolders.Add("PSL " & HomeTeamMTN & " SMS")
                        SmsFolders.Add("PSL " & AwayTeamMTN & " SMS")
                    Case 100
                        ShortCode = "EPL"
                        SmsFolders.Add("EPL " & HomeTeamMTN & " SMS")
                        SmsFolders.Add("EPL " & AwayTeamMTN & " SMS")
                End Select

                Select Case Type
                    Case "full"
                        Title = "Full-time highlights"
                        FileName = "metadata_" & ShortCode & "_game_" & MatchNumber & "_full"
                        ExternalReference = "game_" & ShortCode & "_" & MatchNumber & "_full_sms"
                        smsText = "FULLTIME: " & HomeTeamName & " " & HomeTeamScore & HomeTeamScorers & ", " & AwayTeamName & " " & AwayTeamScore & AwayTeamScorers & "."
                    Case "half"
                        Title = "Half-time highlights"
                        FileName = "metadata_" & ShortCode & "_game_" & MatchNumber & "_half"
                        ExternalReference = "game_" & ShortCode & "_" & MatchNumber & "_half_sms"
                        smsText = "HALFTIME: " & HomeTeamName & " " & HomeTeamScore & HomeTeamScorers & ", " & AwayTeamName & " " & AwayTeamScore & AwayTeamScorers & "."
                    Case "goal"
                        tempRow = ds.Tables("event").Rows(0)
                        Dim goalTeam As Integer = tempRow.Item("TeamId")
                        Dim Player As String = tempRow.Item("Player")
                        Dim MatchTime As String = tempRow.Item("Minutes")
                        HomeTeamScore = tempRow.Item("HomeTeamScore")
                        AwayTeamScore = tempRow.Item("AwayTeamScore")

                        If tempRow.Item("EventId") = 3 Then
                            Player &= " og"
                        End If

                        Dim Count As Integer = 0
                        tempRows = ds.Tables("goals").Select("", "Minutes, Seconds")
                        For Each tempRow In tempRows
                            Count = Count + 1
                            If tempRow.Item("Id") = eventId Then
                                Exit For
                            End If
                        Next
                        Title = "Goal Alert"
                        FileName = "metadata_" & ShortCode & "_game_" & MatchNumber & "_goal" & Count
                        ExternalReference = "game_" & ShortCode & "_" & MatchNumber & "_goal" & Count & "_sms"
                        If goalTeam = AwayTeamId Then
                            smsText = "GOAL ALERT! " & HomeTeamName & " " & HomeTeamScore & ", " & AwayTeamName & " " & AwayTeamScore & " (" & Player & ", " & MatchTime & ")."
                        Else
                            smsText = "GOAL ALERT! " & HomeTeamName & " " & HomeTeamScore & " (" & Player & ", " & MatchTime & "), " & AwayTeamName & " " & AwayTeamScore & "."
                        End If
                End Select

                smsText = smsText.Trim
                If smsText.EndsWith(".") = False Then
                    smsText &= "."
                End If
                Select Case LeagueId
                    Case 794
                        smsText &= " http://mtnloaded.mtn.co.za/psl"
                    Case 100
                        smsText &= " http://mtnloaded.mtn.co.za/epl"
                End Select

                If Not System.IO.Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "") Then
                    System.IO.Directory.CreateDirectory("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "")
                End If

                Dim Files() As String = System.IO.Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "")
                Dim N As Integer
                For N = 0 To Files.Length - 1
                    If Files(N).IndexOf(FileName & ".xml") >= 0 Then
                        File.Delete(Files(N))
                    End If
                    If Files(N).IndexOf(FileName & ".zip") >= 0 Then
                        File.Delete(Files(N))
                    End If
                Next

                Dim ws As New XmlWriterSettings()
                Dim utf8 As Encoding
                utf8 = Encoding.UTF8
                ws.CheckCharacters = True
                ws.CloseOutput = True
                ws.Encoding = utf8
                ws.Indent = True
                ws.ConformanceLevel = ConformanceLevel.Auto

                'SmsFolders.Clear()
                'SmsFolders.Add("Test Confederation Cup Notofications")

                Using tmpWriter As XmlWriter = XmlWriter.Create("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "/" & FileName & ".xml", ws)
                    tmpWriter.WriteStartDocument()
                    tmpWriter.WriteStartElement("import", "http://www.volantis.com/xmlns/vcms/cmsimport")
                    tmpWriter.WriteAttributeString("xsi", "schemaLocation", "http://www.w3.org/2001/XMLSchema-instance", "http://www.volantis.com/xmlns/vcms/cmsimport../../xsd/cmsimport1-0.xsd")

                    smsText = FixText(smsText)

                    Dim I As Integer
                    For I = 0 To SmsFolders.Count - 1
                        tmpWriter.WriteStartElement("article")
                        tmpWriter.WriteElementString("charge-code", "0")
                        tmpWriter.WriteElementString("searchable", "false")
                        tmpWriter.WriteElementString("content-owner", "Subscription")
                        tmpWriter.WriteElementString("provider-code", "901000442")
                        tmpWriter.WriteElementString("service-type", "023")
                        tmpWriter.WriteElementString("is-promo", "false")
                        tmpWriter.WriteElementString("time-to-live", "-1")
                        tmpWriter.WriteElementString("name", ExternalReference & Total & "_l")
                        tmpWriter.WriteElementString("title", SmsFolders(I).Replace("0/", ""))
                        tmpWriter.WriteElementString("target-folder-id", "0/" & SmsFolders(I))
                        tmpWriter.WriteElementString("body-title", Title)
                        tmpWriter.WriteElementString("body-text", smsText)
                        tmpWriter.WriteElementString("link-text", "Not used")
                        tmpWriter.WriteElementString("include-in-index", "false")
                        tmpWriter.WriteElementString("external-reference", ExternalReference & Total & "_t")
                        tmpWriter.WriteEndElement()
                        Total = Total + 1
                    Next

                    tmpWriter.Flush()
                End Using

                Dim Fs As FileStream
                Dim Sw As StreamWriter
                Dim Sr As StreamReader
                Dim String1 As String = ""

                Fs = New FileStream("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "/" & FileName & ".xml", FileMode.Open, FileAccess.Read)
                Sr = New StreamReader(Fs)
                String1 = Sr.ReadToEnd
                Sr.Close()

                String1 = String1.Replace("﻿", "")
                String1 = String1.Replace("ï»¿", "")

                Fs = New FileStream("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "/" & FileName & ".xml", FileMode.Create, FileAccess.Write)
                Sw = New StreamWriter(Fs, Encoding.GetEncoding("ISO-8859-1"))
                Sw.Write(String1)
                Sw.Close()

                Xceed.Zip.Licenser.LicenseKey = "ZIN23-X0UZT-5AHMP-4A2A"

                Dim Status As Integer = 1
                Try
                    Dim tmpArchive As New Xceed.Zip.ZipArchive(New Xceed.FileSystem.DiskFile("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\sms\" & LeagueId & "\" & FileFolder & "\" & FileName & ".zip"))
                    Dim MetaFile As New Xceed.FileSystem.DiskFile("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\sms\" & LeagueId & "\" & FileFolder & "\" & FileName & ".xml")
                    MetaFile.CopyTo(tmpArchive, True)
                    tmpArchive = Nothing

                    Status = 2
                    Dim Server As String = "196.13.230.226"
                    Dim UserName As String = "ssport"
                    Dim Password As String = "123ssport321"

                    Dim Send As Boolean = True

                    If Send = True Then
                        Dim Ftp As Ftp = New Ftp
                        Ftp.Connect(Server)
                        Ftp.Login(UserName, Password)
                        Ftp.SetTransferType(FtpTransferType.Binary)
                        Ftp.KeepAlive()
                        Ftp.ChangeDirectory("content/production")
                        Status = 3
                        Ftp.PutFile("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\sms\" & LeagueId & "\" & FileFolder & "\" & FileName & ".zip", FileName & ".zip")
                        Status = 4
                        Ftp.Disconnect()

                        Ftp.Dispose()
                    Else
                        Status = 4
                    End If
                Catch ex As Exception
                    Call ErrorHandler("MTN Sms Event", ex.Message, "Error uploading sms (" & MatchId & ")")
                End Try

                If Status > 2 Then
                    Try
                        SqlQuery = New SqlCommand("Insert Into SSZGeneral.dbo.mtnSms (leagueId, databaseId, matchId, matchNumber, event, textEn) Values (" & LeagueId & ", 2, " & MatchId & ", " & MatchNumber & ", '" & Type.ToLower & "', '" & smsText.Replace("'", "''") & "')", DatabaseConn)
                        SqlQuery.ExecuteNonQuery()
                    Catch ex As Exception
                        Call ErrorHandler("MTN Sms Event", ex.Message, "Error inserting into database (" & MatchId & ")")
                    End Try
                End If
            End If
        End If

    End Sub

    Private Sub mtnDailySummary(ByVal LeagueId As Integer, ByVal MatchId As String)

        If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
            Dim da As SqlDataAdapter = New SqlDataAdapter
            Dim ds As New DataSet
            Dim MatchDateTime As DateTime
            Dim StartDate As DateTime
            Dim StartDates As Hashtable = New Hashtable
            Dim Proceed As Boolean = True

            StartDates.Add(100, New DateTime(2009, 8, 15))
            StartDates.Add(794, New DateTime(2009, 8, 7))

            StartDate = StartDates(LeagueId)

            Try
                SqlQuery = New SqlCommand("Select MatchDateTime From Soccer.dbo.pa_Matches Where (Id = '" & MatchId & "')", DatabaseConn)
                MatchDateTime = SqlQuery.ExecuteScalar
                Dim smsText As String = ""

                SqlQuery = New SqlCommand("Select Count(Id) From SSZGeneral.dbo.mtnSms Where (LeagueId = " & LeagueId & ") And (Event = 'summary') And (Created > DateAdd(hour, -8, GetDate()))", DatabaseConn)
                If SqlQuery.ExecuteScalar > 0 Then
                    Proceed = False
                End If

                If Proceed = True Then
                    SqlQuery = New SqlCommand("Select a.Id, a.MatchDateTime, b.mtnId As HomeTeamMtn, b.MtnSmsId As HomeTeamMtnSms, c.mtnId As AwayTeamMtn, c.MtnSmsId As AwayTeamMtnSms, a.HomeTeamScore, a.AwayTeamScore, a.Result, a.HomeTeamPenalties, a.AwayTeamPenalties From Soccer.dbo.pa_Matches a INNER JOIN Soccer.dbo.pa_TeamNames b ON b.TeamId = a.HomeTeamId INNER JOIN Soccer.dbo.pa_TeamNames c ON c.TeamId = a.AwayTeamId Where (a.Competition = " & LeagueId & " And b.Competition = " & LeagueId & " And c.Competition = " & LeagueId & ") And (b.Season = a.Season And c.Season = a.Season) And (a.MatchDateTime Between '" & MatchDateTime.ToString("yyyy'-'MM'-'dd") & " 00:00:00' And '" & MatchDateTime.ToString("yyyy'-'MM'-'dd") & " 23:59:59') And (a.StatusId < 14) Order By a.MatchDateTime", DatabaseConn)
                    da = New SqlDataAdapter
                    da.SelectCommand = SqlQuery
                    da.Fill(ds, "matches")
                End If
            Catch ex As Exception
                ds.Tables("matches").Rows.Clear()
                Call ErrorHandler("MTN Sms Summary", ex.Message, "Error retrieving match details from the db")
            End Try

            If Proceed = True Then
                If ds.Tables("matches").Rows.Count > 0 Then
                    Dim smsText As String = ""
                    Dim tmpId As Integer = 100000
                    Dim tmpRows() As DataRow = ds.Tables("matches").Select("Result = 0", "")
                    If tmpRows.Length = 0 Then
                        tmpRows = ds.Tables("matches").Select("", "MatchDateTime")
                        Dim tmpRow As DataRow
                        For Each tmpRow In tmpRows
                            If smsText.Length > 6 Then
                                smsText &= "."
                            End If
                            If tmpRow.Item("HomeTeamPenalties") > 0 Or tmpRow.Item("AwayTeamPenalties") > 0 Then
                                smsText &= " " & tmpRow.Item("HomeTeamMtnSms") & " " & tmpRow.Item("HomeTeamScore") & " (" & tmpRow.Item("HomeTeamPenalties") & ") " & tmpRow.Item("AwayTeamMtnSms") & " " & tmpRow.Item("AwayTeamScore") & " (" & tmpRow.Item("AwayTeamPenalties") & ")"
                            Else
                                smsText &= " " & tmpRow.Item("HomeTeamMtnSms") & " " & tmpRow.Item("HomeTeamScore") & " " & tmpRow.Item("AwayTeamMtnSms") & " " & tmpRow.Item("AwayTeamScore")
                            End If
                        Next

                        smsText = MatchDateTime.ToString("dd MMMM") & ", ROUND-UP:" & smsText

                        Dim DayNumber As Integer
                        Dim FileFolder As String = ""
                        Dim FileName As String = ""
                        Dim ExternalReference As String = ""
                        Dim ShortCode As String = ""
                        Dim Title As String = ""
                        Dim SmsFolders As ArrayList = New ArrayList

                        Dim tmpSpan As TimeSpan
                        tmpSpan = MatchDateTime.Subtract(StartDate)
                        DayNumber = tmpSpan.Days + 1

                        FileFolder = "day" & DayNumber
                        Select Case LeagueId
                            Case 794
                                ShortCode = "PSL"
                                SmsFolders.Add("PSL Highlights SMS")
                                SmsFolders.Add("PSL SMS Subscriptions Ussd")
                            Case 100
                                ShortCode = "EPL"
                                SmsFolders.Add("EPL Highlights SMS")
                                SmsFolders.Add("EPL SMS Subscriptions Ussd")
                        End Select

                        SqlQuery = New SqlCommand("Select Count(Id) From SSZGeneral.dbo.mtnSms Where (MatchId = " & MatchId & ") And (leagueId = " & LeagueId & ")", DatabaseConn)
                        Dim Total As Integer = SqlQuery.ExecuteScalar
                        Total = (Total * 1) + 1

                        Title = "Daily Summary"
                        FileName = "metadata_" & ShortCode & "_highlight_" & DayNumber & "_summary"
                        ExternalReference = "highlight_" & ShortCode & "_" & DayNumber & "_summary_sms"

                        If Not System.IO.Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "") Then
                            System.IO.Directory.CreateDirectory("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "")
                        End If

                        Dim Files() As String = System.IO.Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "")
                        Dim N As Integer
                        For N = 0 To Files.Length - 1
                            If Files(N).IndexOf(FileName & ".xml") >= 0 Then
                                File.Delete(Files(N))
                            End If
                            If Files(N).IndexOf(FileName & ".zip") >= 0 Then
                                File.Delete(Files(N))
                            End If
                        Next

                        Dim ws As New XmlWriterSettings()
                        Dim utf8 As Encoding
                        utf8 = Encoding.UTF8
                        ws.CheckCharacters = True
                        ws.CloseOutput = True
                        ws.Encoding = utf8
                        ws.Indent = True
                        ws.ConformanceLevel = ConformanceLevel.Auto

                        'SmsFolders.Clear()
                        'SmsFolders.Add("Test Confederation Cup Notofications")

                        Using tmpWriter As XmlWriter = XmlWriter.Create("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "/" & FileName & ".xml", ws)
                            tmpWriter.WriteStartDocument()
                            tmpWriter.WriteStartElement("import", "http://www.volantis.com/xmlns/vcms/cmsimport")
                            tmpWriter.WriteAttributeString("xsi", "schemaLocation", "http://www.w3.org/2001/XMLSchema-instance", "http://www.volantis.com/xmlns/vcms/cmsimport../../xsd/cmsimport1-0.xsd")

                            smsText = FixText(smsText)

                            Dim I As Integer
                            For I = 0 To SmsFolders.Count - 1
                                tmpWriter.WriteStartElement("article")
                                tmpWriter.WriteElementString("charge-code", "0")
                                tmpWriter.WriteElementString("searchable", "false")
                                tmpWriter.WriteElementString("content-owner", "Subscription")
                                tmpWriter.WriteElementString("provider-code", "901000442")
                                tmpWriter.WriteElementString("service-type", "023")
                                tmpWriter.WriteElementString("is-promo", "false")
                                tmpWriter.WriteElementString("time-to-live", "-1")
                                tmpWriter.WriteElementString("name", ExternalReference & Total & "_l")
                                tmpWriter.WriteElementString("title", SmsFolders(I).Replace("0/", ""))
                                tmpWriter.WriteElementString("target-folder-id", "0/" & SmsFolders(I))
                                tmpWriter.WriteElementString("body-title", Title)
                                tmpWriter.WriteElementString("body-text", smsText)
                                tmpWriter.WriteElementString("link-text", "Not used")
                                tmpWriter.WriteElementString("include-in-index", "false")
                                tmpWriter.WriteElementString("external-reference", ExternalReference & Total & "_t")
                                tmpWriter.WriteEndElement()
                                Total = Total + 1
                            Next

                            tmpWriter.Flush()
                        End Using

                        Dim Fs As FileStream
                        Dim Sw As StreamWriter
                        Dim Sr As StreamReader
                        Dim String1 As String = ""

                        Fs = New FileStream("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "/" & FileName & ".xml", FileMode.Open, FileAccess.Read)
                        Sr = New StreamReader(Fs)
                        String1 = Sr.ReadToEnd
                        Sr.Close()

                        String1 = String1.Replace("﻿", "")
                        String1 = String1.Replace("ï»¿", "")

                        Fs = New FileStream("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files/sms/" & LeagueId & "/" & FileFolder & "/" & FileName & ".xml", FileMode.Create, FileAccess.Write)
                        Sw = New StreamWriter(Fs, Encoding.GetEncoding("ISO-8859-1"))
                        Sw.Write(String1)
                        Sw.Close()

                        Xceed.Zip.Licenser.LicenseKey = "ZIN23-X0UZT-5AHMP-4A2A"

                        Dim Status As Integer = 1
                        Try
                            Dim tmpArchive As New Xceed.Zip.ZipArchive(New Xceed.FileSystem.DiskFile("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\sms\" & LeagueId & "\" & FileFolder & "\" & FileName & ".zip"))
                            Dim MetaFile As New Xceed.FileSystem.DiskFile("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\sms\" & LeagueId & "\" & FileFolder & "\" & FileName & ".xml")
                            MetaFile.CopyTo(tmpArchive, True)
                            tmpArchive = Nothing

                            Status = 2
                            Dim Server As String = "196.13.230.226"
                            Dim UserName As String = "ssport"
                            Dim Password As String = "123ssport321"

                            Dim Send As Boolean = True

                            If Send = True Then
                                Dim Ftp As Ftp = New Ftp
                                Ftp.Connect(Server)
                                Ftp.Login(UserName, Password)
                                Ftp.SetTransferType(FtpTransferType.Binary)
                                Ftp.KeepAlive()
                                Ftp.ChangeDirectory("content/production")
                                Status = 3
                                Ftp.PutFile("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\sms\" & LeagueId & "\" & FileFolder & "\" & FileName & ".zip", FileName & ".zip")
                                Status = 4
                                Ftp.Disconnect()

                                Ftp.Dispose()
                            Else
                                Status = 4
                            End If
                        Catch ex As Exception
                            Call ErrorHandler("MTN Sms Summary", ex.Message, "Error uploading Summary")
                        End Try

                        If Status > 2 Then
                            Try
                                SqlQuery = New SqlCommand("Insert Into SSZGeneral.dbo.mtnSms (leagueId, databaseId, matchId, matchNumber, event, textEn) Values (" & LeagueId & ", 2, " & MatchId & ", 1, 'summary', '" & smsText.Replace("'", "''") & "')", DatabaseConn)
                                SqlQuery.ExecuteNonQuery()
                            Catch ex As Exception
                                Call ErrorHandler("MTN Sms Summary", ex.Message, "Error inserting Summary into database")
                            End Try
                        End If
                    End If
                End If
            End If
        End If

    End Sub

    Private Sub SmsFixtures(ByVal TournamentId As Integer)

        Dim Count As Integer
        Dim fixtureDate As DateTime = DateTime.Now
        Dim SmsTournament As Integer = 1
        Dim SmsTournamentName As String = ""
        Dim RsRec As SqlDataReader

        Select Case TournamentId
            Case 100
                SmsTournament = 39
                SmsTournamentName = "EPL"
            Case 794
                SmsTournament = 61
                SmsTournamentName = "PSL"
        End Select

        updateListbox("Processing " & SmsTournamentName & " Fixtures", True)

        If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
            Dim Matches As String = ""
            Try
                SqlQuery = New SqlCommand("Select Id, HomeTeamName, AwayTeamName, MatchDateTime From Soccer.dbo.pa_Matches Where (Competition = " & TournamentId & ") And (Result = 0) And (MatchDateTime >= '" & fixtureDate.ToString("yyyy'-'MM'-'dd '00:00:00'") & "' And MatchDateTime <= '" & fixtureDate.ToString("yyyy'-'MM'-'dd '23:59:59'") & "') And (StatusId < 14)", DatabaseConn)
                RsRec = SqlQuery.ExecuteReader
                Count = 1
                While RsRec.Read
                    Dim Id As String = RsRec("Id")
                    Dim HomeTeamName As String = RsRec("HomeTeamName")
                    Dim AwayTeamName As String = RsRec("AwayTeamName")
                    Dim MatchDateTime As DateTime = RsRec("MatchDateTime")
                    Dim tmpMatchDateTime As String = MatchDateTime.ToString("dd MMM HH':'mm")
                    If Count > 1 Then
                        Matches &= "|"
                    End If
                    Matches &= Id
                    Matches &= "," & HomeTeamName
                    Matches &= "," & AwayTeamName
                    Matches &= "," & tmpMatchDateTime
                    Count = Count + 1
                End While
                RsRec.Close()
            Catch ex As Exception
                Call ErrorHandler("Sms fixtures", ex.Message, "Error retrieving matches from db")
            End Try

            If Matches <> "" Then
                Dim tmpArray() As String = Matches.Split("|")
                Dim I As Integer
                For I = 0 To tmpArray.GetUpperBound(0)
                    Dim tmpArray1() As String = tmpArray(I).Split(",")
                    Dim Tournaments As String = ""
                    Dim Id As String = tmpArray1(0)
                    Dim HomeTeamName As String = tmpArray1(1)
                    Dim AwayTeamName As String = tmpArray1(2)
                    Dim tmpMatchDateTime As String = tmpArray1(3)

                    Try
                        SqlQuery = New SqlCommand("Select Count(*) As Total From SSZGeneral.dbo.bulksmsapplist Where (Tournament = " & SmsTournament & ") And (MatchId = " & Id & ") And (Type = 'fixture')", DatabaseConn)
                        Dim Total As Integer = SqlQuery.ExecuteScalar

                        If Total = 0 Then
                            SqlQuery = New SqlCommand("Select Id From SuperSportZone.dbo.supersms_products Where (ParentId = " & SmsTournament & ") And (Active = 1) And (Name = '" & HomeTeamName & "' Or name = '" & AwayTeamName & "')", DatabaseConn)
                            RsRec = SqlQuery.ExecuteReader
                            While RsRec.Read
                                Dim Tournament As String = RsRec("Id")
                                If Tournaments = "" Then
                                    Tournaments = Tournament
                                Else
                                    Tournaments &= "|" & Tournament
                                End If
                            End While
                            RsRec.Close()

                            If Tournaments <> "" Then
                                Dim ClientReference As String = "4_" & DateTime.Now.ToString("yyyyMMddhhmmssffff")
                                Dim SmsText As String = "SS Mobile - Team Fixture"
                                SmsText &= Environment.NewLine & tmpMatchDateTime
                                SmsText &= Environment.NewLine & HomeTeamName & " v " & AwayTeamName
                                SqlQuery = New SqlCommand("Insert Into SSZGeneral.dbo.BulkSms (Reference, Tournament, Text, Complete, Created, Modified, smsApp) Values ('" & ClientReference & "', '" & Tournaments & "', '" & SmsText.Replace("'", "''") & "', 0, '" & DateTime.Now & "', '" & DateTime.Now & "', 1)", DatabaseConn)
                                SqlQuery.ExecuteNonQuery()
                                SqlQuery = New SqlCommand("Insert Into SSZGeneral.dbo.BulkSmsAppList (Tournament, MatchId, Type, Created) Values (" & SmsTournament & ", '" & Id & "','fixture', '" & DateTime.Now & "')", DatabaseConn)
                                SqlQuery.ExecuteNonQuery()
                            End If
                        End If
                    Catch ex As Exception
                        Call ErrorHandler("Sms fixtures", ex.Message, "Error inserting sms in db")
                    End Try
                Next
            End If
        End If

        updateListbox("" & SmsTournamentName & " Fixtures complete", True)

    End Sub

    Private Sub SmsResult(ByVal TournamentId As Integer, ByVal MatchId As String)

        Dim Count As Integer
        Dim SmsTournament As Integer = 1
        Dim SmsTournamentName As String = ""
        Dim RsRec As SqlDataReader

        Select Case TournamentId
            Case 100
                SmsTournament = 39
                SmsTournamentName = "EPL"
            Case 100
                SmsTournament = 61
                SmsTournamentName = "PSL"
        End Select

        If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
            Try
                SqlQuery = New SqlCommand("Select Count(*) As Total From SSZGeneral.dbo.bulksmsapplist Where (Tournament = " & SmsTournament & ") And (MatchId = '" & MatchId & "') And (Type = 'result')", DatabaseConn)
                Dim Total As Integer = SqlQuery.ExecuteScalar

                If Total = 0 Then
                    Dim Tournaments As String = ""
                    Dim HomeTeamName As String = ""
                    Dim AwayTeamName As String = ""
                    Dim HomeTeamScore As Integer = 0
                    Dim AwayTeamScore As Integer = 0
                    Dim MatchDateTime As DateTime = DateTime.Now
                    Dim tmpMatchDateTime As String = ""

                    SqlQuery = New SqlCommand("Select HomeTeamName, AwayTeamName, HomeTeamScore, AwayTeamScore, MatchDateTime From Soccer.dbo.Pa_Matches Where (Id = '" & MatchId & "')", DatabaseConn)
                    RsRec = SqlQuery.ExecuteReader
                    While RsRec.Read
                        HomeTeamName = RsRec("HomeTeamName")
                        AwayTeamName = RsRec("AwayTeamName")
                        HomeTeamScore = RsRec("HomeTeamScore")
                        AwayTeamScore = RsRec("AwayTeamScore")
                        MatchDateTime = RsRec("MatchDateTime")
                        tmpMatchDateTime = MatchDateTime.ToString("dd MMM")
                    End While
                    RsRec.Close()

                    SqlQuery = New SqlCommand("Select Id From SuperSportZone.dbo.supersms_products Where (ParentId = " & SmsTournament & ") And (Active = 1) And (Name = '" & HomeTeamName & "' Or Name = '" & AwayTeamName & "')", DatabaseConn)
                    RsRec = SqlQuery.ExecuteReader
                    While RsRec.Read
                        Dim Tournament As String = RsRec("Id")
                        If Tournaments = "" Then
                            Tournaments = Tournament
                        Else
                            Tournaments &= "|" & Tournament
                        End If
                    End While
                    RsRec.Close()

                    If Tournaments <> "" Then
                        Dim ClientReference As String = "4_" & DateTime.Now.ToString("yyyyMMddhhmmssffff")
                        Dim SmsText As String = "SS Mobile - Team Result"
                        SmsText &= Environment.NewLine & tmpMatchDateTime
                        SmsText &= Environment.NewLine & HomeTeamName & " " & HomeTeamScore & " " & AwayTeamName & " " & AwayTeamScore
                        SqlQuery = New SqlCommand("Insert Into SSZGeneral.dbo.BulkSms (Reference, Tournament, Text, Complete, Created, Modified, smsApp) Values ('" & ClientReference & "', '" & Tournaments & "', '" & SmsText.Replace("'", "''") & "', 0, '" & DateTime.Now & "', '" & DateTime.Now & "', 1)", DatabaseConn)
                        SqlQuery.ExecuteNonQuery()
                        SqlQuery = New SqlCommand("Insert Into SSZGeneral.dbo.BulkSmsAppList (Tournament, MatchId, Type, Created) Values (" & SmsTournament & ", '" & MatchId & "','result', '" & DateTime.Now & "')", DatabaseConn)
                        SqlQuery.ExecuteNonQuery()
                    End If
                End If
            Catch ex As Exception
                Call ErrorHandler("Sms results", ex.Message, "Error inserting sms in db")
            End Try
        End If

    End Sub

    Private Sub SmsLog(ByVal TournamentId As Integer)

        Dim Count As Integer
        Dim SmsTournament As Integer = 1
        Dim SmsTournamentName As String = ""
        Dim RsRec As SqlDataReader
        Dim logDate As DateTime = DateTime.Now

        Select Case TournamentId
            Case 100
                SmsTournament = 39
                SmsTournamentName = "EPL"
            Case 794
                SmsTournament = 61
                SmsTournamentName = "PSL"
        End Select

        If DatabaseConn.State = ConnectionState.Open And DatabaseConn.State <> ConnectionState.Broken Then
            Try
                SqlQuery = New SqlCommand("Select Count(*) As Total From SSZGeneral.dbo.bulksmsapplist Where (Tournament = " & SmsTournament & ") And (Type = 'log') And (Created >= '" & DateTime.Now.ToString("yyyy'-'MM'-'dd '00:00:00'") & "' And Created <= '" & DateTime.Now.ToString("yyyy'-'MM'-'dd '23:59:59'") & "')", DatabaseConn)
                Dim Total As Integer = SqlQuery.ExecuteScalar

                If Total = 0 Then
                    SqlQuery = New SqlCommand("Select Count(*) As Total From Soccer.dbo.pa_Matches Where (Competition = " & TournamentId & ") And (MatchDateTime >= '" & logDate.ToString("yyyy'-'MM'-'dd '00:00:00'") & "' And MatchDateTime <= '" & logDate.ToString("yyyy'-'MM'-'dd '23:59:59'") & "') And (Result = 0) And (StatusId < 14 Or StatusId = 15 Or StatusId = 16)", DatabaseConn)
                    Total = SqlQuery.ExecuteScalar
                End If

                If Total = 0 Then
                    Dim Tournaments As String = ""
                    Dim tmpQuery As String = "Select Id From SuperSportZone.dbo.supersms_products Where (ParentId = " & SmsTournament & ") And (Active = 1) And (Name = 'sdfsdfsdf'"
                    SqlQuery = New SqlCommand("Select HomeTeamName, AwayTeamName From Soccer.dbo.pa_Matches Where (Competition = " & TournamentId & ") And (MatchDateTime >= '" & logDate.ToString("yyyy'-'MM'-'dd '00:00:00'") & "' And MatchDateTime <= '" & logDate.ToString("yyyy'-'MM'-'dd '23:59:59'") & "') And (StatusId < 14)", DatabaseConn)
                    RsRec = SqlQuery.ExecuteReader
                    While RsRec.Read
                        tmpQuery &= " Or Name = '" & RsRec("HomeTeamName") & "' Or Name = '" & RsRec("AwayTeamName") & "'"
                    End While
                    RsRec.Close()
                    tmpQuery &= ")"

                    SqlQuery = New SqlCommand(tmpQuery, DatabaseConn)
                    RsRec = SqlQuery.ExecuteReader
                    While RsRec.Read
                        Dim Tournament As String = RsRec("Id")
                        If Tournaments = "" Then
                            Tournaments = Tournament
                        Else
                            Tournaments &= "|" & Tournament
                        End If
                    End While
                    RsRec.Close()

                    If Tournaments <> "" Then
                        Dim ClientReference As String = "4_" & DateTime.Now.ToString("yyyyMMddhhmmssffff")
                        Dim SmsText As String = "SS Mobile - Log"
                        SmsText &= Environment.NewLine & "POS, TEAM, PLD, PTS"
                        SqlQuery = New SqlCommand("Execute Soccer.dbo.pa_returnLogs " & TournamentId & ", 0", DatabaseConn)
                        RsRec = SqlQuery.ExecuteReader
                        Count = 1
                        While RsRec.Read
                            SmsText &= Environment.NewLine & Count & " " & RsRec("TeamSmsName") & " " & RsRec("Played") & " " & RsRec("LogPoints")
                            Count = Count + 1
                        End While
                        RsRec.Close()
                        SqlQuery = New SqlCommand("Insert Into SSZGeneral.dbo.BulkSms (Reference, Tournament, Text, Complete, Created, Modified, smsApp) Values ('" & ClientReference & "', '" & Tournaments & "', '" & SmsText.Replace("'", "''") & "', 0, '" & DateTime.Now & "', '" & DateTime.Now & "', 1)", DatabaseConn)
                        SqlQuery.ExecuteNonQuery()
                        SqlQuery = New SqlCommand("Insert Into SSZGeneral.dbo.BulkSmsAppList (Tournament, MatchId, Type, Created) Values (" & SmsTournament & ", '','log', '" & DateTime.Now & "')", DatabaseConn)
                        SqlQuery.ExecuteNonQuery()
                    End If
                End If
            Catch ex As Exception
                Call ErrorHandler("Sms log", ex.Message, "Error inserting sms in db")
            End Try
        End If

    End Sub

    Private Function FixText(ByVal tmpText As String) As String

        Dim retString As String = tmpText

        retString = retString.Replace("à", "a")
        retString = retString.Replace("â", "a")
        retString = retString.Replace("ä", "a")

        retString = retString.Replace("ç", "c")

        retString = retString.Replace("è", "e")
        retString = retString.Replace("é", "e")
        retString = retString.Replace("ê", "e")
        retString = retString.Replace("ë", "e")

        retString = retString.Replace("î", "i")
        retString = retString.Replace("ï", "i")

        retString = retString.Replace("ô", "o")

        retString = retString.Replace("ù", "u")
        retString = retString.Replace("û", "u")
        retString = retString.Replace("ü", "u")

        retString = retString.Replace("ÿ", "y")

        Return retString

    End Function

#End Region

#Region " Admin "

    Private Sub ErrorHandler(ByVal MatchId As String, ByVal LogError As String)

        Try
            Dim Fs As New FileStream("C:\Program Files\SuperSport Zone\SuperSport Soccer\log.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim Sw As New StreamWriter(Fs)
            Sw.BaseStream.Seek(0, SeekOrigin.End)
            Sw.WriteLine("" & DateTime.Now & " - " & MatchId & " - " & LogError & "")
            Sw.Close()
        Catch Ex As Exception

        End Try

    End Sub

    Private Sub ErrorHandler(ByVal MatchId As String, ByVal LogError As String, ByVal Message As String)

        Try
            Dim Fs As New FileStream("C:\Program Files\SuperSport Zone\SuperSport Soccer\log.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim Sw As New StreamWriter(Fs)
            Sw.BaseStream.Seek(0, SeekOrigin.End)
            Sw.WriteLine("" & DateTime.Now & " - " & MatchId & " - " & LogError & "")
            Sw.Close()

        Catch Ex As Exception

        End Try

        Try
            Dim tmpErrors As New Errors.Service
            If Proxy = True Then
                tmpErrors.Proxy = myProxy
            End If
            tmpErrors.Errors("Soccer", MatchId, LogError, 4, Message, True, Message)
        Catch Ex As Exception
            Dim Fs As New FileStream("C:\Program Files\SuperSport Zone\SuperSport Soccer\log.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim Sw As New StreamWriter(Fs)
            Sw.BaseStream.Seek(0, SeekOrigin.End)
            Sw.WriteLine(Ex.Message)
            Sw.Close()
        End Try

    End Sub

    Public Function CheckForExistingInstance() As Boolean

        Dim Multiple As Boolean = False

        If Process.GetProcessesByName(Process.GetCurrentProcess.ProcessName).Length > 1 Then
            Multiple = True
        End If

        Return Multiple

    End Function

    Public Function CheckForUpdate() As Boolean

        Dim Update As Boolean = False

        Try
            'DatabaseConn.Open()
            'SqlQuery = New SqlCommand("Select Version From SSZGeneral.dbo.ssprograms Where (Id = 3)", DatabaseConn)
            'Dim tmpVersion As String = SqlQuery.ExecuteScalar
            'If Version <> tmpVersion Then
            '    Shell("C:\Program Files\SuperSport Zone\SuperSport Updater\SuperSport Updater.exe soccer", AppWinStyle.NormalFocus)
            '    Me.WindowState = FormWindowState.Minimized
            '    Update = True
            'Else
            '    Update = False
            'End If
            'DatabaseConn.Close()
        Catch Ex As Exception
            Update = False
        End Try

        Return Update

    End Function

    Private Function CheckForProxy() As Boolean

        If File.Exists("C:\Program Files\SuperSport Zone\SuperSport Active\proxyexists.txt") Then
            Dim h As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName)
            h.AddressList.GetValue(0)
            'If h.AddressList.GetValue(0).ToString.IndexOf("10.16.") >= 0 Then
            Dim Fs As New FileStream("C:\Program Files\SuperSport Zone\SuperSport Active\proxy.txt", FileMode.OpenOrCreate, FileAccess.Read)
            Dim Sw As New StreamReader(Fs)
            Sw.BaseStream.Seek(0, SeekOrigin.Begin)
            Dim proxyUsername As String = Sw.ReadLine
            Dim proxyPassword As String = Sw.ReadLine
            Dim proxyDomain As String = Sw.ReadLine
            Dim proxyAddress As String = Sw.ReadLine
            Dim proxyPort As Integer = Sw.ReadLine
            Dim ProxyCredentials As New System.Net.NetworkCredential(proxyUsername, proxyPassword, proxyDomain)
            myProxy = New System.Net.WebProxy(proxyAddress, proxyPort)
            myProxy.Credentials = ProxyCredentials
            Proxy = True
            Sw.Close()
            'End If
        End If

    End Function

    Private Sub ZipFiles()

        If Not File.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\History\" & DateTime.Now.ToString("yyyyMMdd") & ".zip") Then
            Try
                updateListbox("Archiving files", True)
                Dim FixtureFolder As New DiskFolder("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\Complete")
                Dim MatchFolder As New DiskFolder("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Match\Complete")
                Dim CompetitionFolder As New DiskFolder("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Competition\Complete")
                Dim LiveFolder As New DiskFolder("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Complete")
                Dim ProfileFolder As New DiskFolder("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Complete")
                Dim tmpArchive As ZipArchive = New ZipArchive(New DiskFile("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\History\" & DateTime.Now.ToString("yyyyMMdd") & ".zip"))
                FixtureFolder.CopyFilesTo(tmpArchive, True, True)
                MatchFolder.CopyFilesTo(tmpArchive, True, True)
                CompetitionFolder.CopyFilesTo(tmpArchive, True, True)
                LiveFolder.CopyFilesTo(tmpArchive, True, True)
                ProfileFolder.CopyFilesTo(tmpArchive, True, True)

                Dim File As String
                Dim Files() As String
                Dim Fi As FileInfo

                If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Match\Complete") Then
                    Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Match\Complete")
                    For Each File In Files
                        Fi = New FileInfo(File)
                        If Fi.Name.IndexOf(".xml") >= 0 Then
                            Try
                                Fi.Delete()
                            Catch ex As Exception

                            End Try
                        End If
                    Next
                End If

                If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\Complete") Then
                    Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Fixture\Complete")
                    For Each File In Files
                        Fi = New FileInfo(File)
                        If Fi.Name.IndexOf(".xml") >= 0 Then
                            Try
                                Fi.Delete()
                            Catch ex As Exception

                            End Try
                        End If
                    Next
                End If

                If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Competition\Complete") Then
                    Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Competition\Complete")
                    For Each File In Files
                        Fi = New FileInfo(File)
                        If Fi.Name.IndexOf(".xml") >= 0 Then
                            Try
                                Fi.Delete()
                            Catch ex As Exception

                            End Try
                        End If
                    Next
                End If

                If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Complete") Then
                    Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Live\Complete")
                    For Each File In Files
                        Fi = New FileInfo(File)
                        If Fi.Name.IndexOf(".xml") >= 0 Then
                            Try
                                Fi.Delete()
                            Catch ex As Exception

                            End Try
                        End If
                    Next
                End If

                If Directory.Exists("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Complete") Then
                    Files = Directory.GetFiles("C:\Program Files\SuperSport Zone\SuperSport Soccer\Files\Profiles\Complete")
                    For Each File In Files
                        Fi = New FileInfo(File)
                        If Fi.Name.IndexOf(".xml") >= 0 Then
                            Try
                                Fi.Delete()
                            Catch ex As Exception

                            End Try
                        End If
                    Next
                End If

                updateListbox("Files archived", True)
            Catch ex As Exception
                updateListbox("Error archiving files", True)
            End Try
        End If

    End Sub

#End Region

    Private Function FixTeamName(ByVal TeamId As String, ByVal Name As String) As String

        Dim Teams As Hashtable = New Hashtable
        Teams.Add("494", "Ireland")
        Teams.Add("631", "Netherlands")

        If Teams.ContainsKey(TeamId) Then
            Name = Teams(TeamId)
        End If

        Return Name

    End Function

    Private Delegate Sub dlgUpdate(ByVal tmpString As String, ByVal tmpBoolean As Boolean)

    Sub updateListbox(ByVal tmpString As String, ByVal tmpBoolean As Boolean)

        If ListBox1.InvokeRequired = True Then
            Dim d As New dlgUpdate(AddressOf updateListbox)
            ListBox1.Invoke(d, tmpString, tmpBoolean)
        Else
            If tmpString = "" Then
                ListBox1.Items.Clear()
            ElseIf tmpString = "select" Then
                ListBox1.SelectedIndex = ListBox1.Items.Count - 1
            ElseIf tmpBoolean = True Then
                ListBox1.Items.Add(tmpString)
            Else
                ListBox1.Items.Remove(tmpString)
            End If
            ListBox1.Refresh()
        End If

    End Sub

    Sub updateButton(ByVal tmpString As String, ByVal tmpBoolean As Boolean)

        If Button1.InvokeRequired = True Then
            Dim d As New dlgUpdate(AddressOf updateButton)
            Button1.Invoke(d, tmpString, tmpBoolean)
        Else
            If tmpBoolean = True Then
                Button1.Enabled = True
                RunToolStripMenuItem.Enabled = True
            Else
                Button1.Enabled = False
                RunToolStripMenuItem.Enabled = False
            End If
        End If

    End Sub

End Class
